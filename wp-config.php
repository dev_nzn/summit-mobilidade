<?php

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'summit-mobilidade');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'summit-mobilidade');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'summit-mobilidade');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tZ. $*QS2k-E=#]-SPL6{EKhV*mUNf&vLx7]~3)3Ez?aoN!p%Z[fH`!T?;z9yP1:');
define('SECURE_AUTH_KEY',  'p$`[UG)w?dC#ue]N.kIRmJ4JEBapmAgUG@gf+lW!gvs}/X/iPpQ~nBu;j7[iA/m=');
define('LOGGED_IN_KEY',    'gsVb6hu8l2@K>$+]^O(/d<HyXE!zKE@~jm`2IIs&sd6rhDjqw ]hY}](&+uw$[?{');
define('NONCE_KEY',        'B1m:3JWMzVt#Wmk,B,W%gI@8TqNc`UkZD|:z:ALMVk/!6Bk=YV]G[KO]^ 5O=r/2');
define('AUTH_SALT',        '?*WxOpDQewifXBhCiGZ-|>>u4?V}c`IqrMoE(~fHe wuS<IPdWZs-A8dSkBa,JB~');
define('SECURE_AUTH_SALT', 's~Dgbmke2dB05<dcsxKT&{VB(PZH)eWN^juJ=0/X2Hqdy;3~dUH(B@W@$2}8!mg-');
define('LOGGED_IN_SALT',   'I`n+_e*,A~+F2pBMzT40P4&no|Pb!30`TOI];yGsr#e4_`b$J<ZaBg&NUY~Q5RvY');
define('NONCE_SALT',       'FKxGxuW_oL3RQsBq, a&!0+k80!0f.onekhov2oR2eewNU<|)))`n>4xlFR!Z*9?');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';