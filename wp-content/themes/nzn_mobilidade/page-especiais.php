<?php
/**
 * Template Name: Especiais Page Templete 
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

 get_header();
 $blog_page_image = wp_get_attachment_url( get_post_thumbnail_id(get_option( 'page_for_posts' )) );

 ?>
    <main id="site-content" class="custom-template-page page_especial page_especiais_temp" role="main">
        <div class="container banner-top">
            <div class="row d-flex justify-content-center">
                <div class="col-12">
                    <?php
                    get_template_part( 'template-parts/home-super_banner' );
                ?>
                </div>
            </div>
        </div>
        <header class="header-especial" style="background:<?php the_field('cor_primaria_do_patrocinador'); ?>">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-6">
                        <h3 class="titleFixHeader">
                            <?php the_title(); ?>
                        </h3>
                    </div>
                    <div class="col-6">
                        <div id="detailsHeader">
                            <span><?php the_field('texto1'); ?></span>
                            <a href="<?php the_field('link_patrocinador'); ?>">
                                <img src="<?php the_field('logo_patrocinador'); ?>"  />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- .archive-header -->
        <div class="container">

            <div class="row post-list">
                <?php 
                if (has_term('', 'especiaistax')) {

	$meta_query = [];
	foreach (get_the_terms($post->ID, 'especiaistax') as $item) {
		$meta_query[] = array(
			'key' => 'categoria_patrocinador',
            'value' => $item->term_id,
            'compare' => 'IN'
        );
    }
    
	$query = array(
		'post_type' => 'page',
		'meta_query' => $meta_query
	);
    $posts = get_posts($query);


    $termId = get_field('categoria_patrocinador');
    
    if( $termId ): 
        $args = array(
            'tax_query' => array(
                array(
                    'taxonomy' => 'especiaistax',
                    'terms' => $termId
                )
            )
        );

        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
            $i = 0;
            while ( $query->have_posts() ) {
                $i++;
                $query->the_post();
                get_template_part( 'template-parts/content-archive', get_post_type() );
                if($i == 6){
                    get_template_part( 'template-parts/home-arroba_banner' );
                }

            }
        }
        endif;

    }

        wp_reset_postdata();
        ?>
            </div>
        </div>

        <?php get_template_part( 'template-parts/pagination' ); ?>

    </main>
    <!-- #site-content -->
    <?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
    <?php get_footer(); ?>