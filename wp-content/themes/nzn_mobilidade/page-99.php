<?php
/**
 * Template Name: Page Templete 99
 * Template Post Type: page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

 get_header();

 ?>
    <main id="site-content" class="custom-template-page taxonomy_post" role="main">

        <?php
 if ( ! is_page_template() ) {
   $archive_title    = get_the_archive_title();
   $archive_subtitle = get_the_archive_description();
}


if ( have_posts() ) {

   $i = 0;

   while ( have_posts() ) {
      $i++;
      if ( $i > 1 ) {
         echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
      }
      the_post();

      get_template_part( 'template-parts/entry-header', get_post_type() );

   }
} 
   ?>






            <div class="container post-list" style="margin-top: 0;">
                <div class="row">
                    <?php include get_template_directory().'/template-parts/feed-agencia99.php'; ?>

                    <div class="col-4 ">
                        <div id="arroba-banner">
                            <?php
            if( have_rows('arroba_banner_gerais', 'option') ):

               while( have_rows('arroba_banner_gerais', 'option') ): the_row();
       
               $banner = get_sub_field('banner');
               $link = get_sub_field('link');
       
                   
       
           if( $link ){
               
                   $link_url = $link['url'];
                   $link_title = $link['title'];
                   $link_target = $link['target'] ? $link['target'] : '_self';
                   
                   ?>
                                <div class="arroba_banner">
                                    <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                   <img src="<?php echo esc_url( $banner ); ?>" alt="<?php echo esc_html( $link_title ); ?>" />
               </a>
                                </div>
                                <?php
           }
       endwhile;
       endif;
         ?>
                        </div>
                    </div>

                </div>
            </div>


            <?php get_template_part( 'template-parts/pagination' ); ?>

    </main>
    <!-- #site-content -->
    <?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
    <?php get_footer(); ?>