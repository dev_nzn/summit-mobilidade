<?php

/**
 * The template for displaying single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */
get_header();
setPostViews(get_the_ID());

the_post();
$cover_thumb = wp_get_attachment_url(get_post_thumbnail_id($post->ID, 'full-destaque'));

if (has_term('', 'especiaistax')) {

    $taxonomy = get_the_terms($post->ID, 'especiaistax');
    foreach ($taxonomy as $tax) {

        $post_type = get_post_type(get_the_ID());
    }
?>
    <div class="fix-header-especial">
        <div class="container">
            <div class="row d-sm-flex align-items-center">
                <div class="col-sm-6">
                    <h3 class="titleFixHeader">
                        <?php echo $tax->name; ?>
                    </h3>
                </div>
                <div class="col-sm-6">
                    <div id="detailsHeader">
                        <span><?php the_field('texto1', $tax); ?></span>
                        <a href="<?php the_field('link_patrocinador', $tax); ?>">
                            <img src="<?php the_field('logo_patrocinador', $tax); ?>" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <main id="site-content" role="main" class="single_post_content">
        <div class="container banner-top">
            <div class="row d-flex justify-content-center">
                <div class="col-12">
                    <?php
                get_template_part('template-parts/home-super_banner');
                ?>
                </div>
            </div>
        </div>
        <?php
    if (has_term('', 'especiaistax')) {
    ?>
            <header class="header-especial" style="background:<?php the_field('cor_primaria_do_patrocinador', $tax); ?>">
                <div class="container">
                    <div class="row d-sm-flex align-items-center">
                        <div class="col-sm-6">
                            <h3 class="titleFixHeader">
                                <?php echo $tax->name; ?>
                            </h3>
                        </div>
                        <div class="col-sm-6">
                            <div id="detailsHeader">
                                <span><?php the_field('texto1', $tax); ?></span>
                                <a href="<?php the_field('link_patrocinador', $tax); ?>">
                                <img src="<?php the_field('logo_patrocinador', $tax); ?>" />
                            </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <?php } ?>

            <article <?php post_class(); ?> id="post-
                <?php the_ID(); ?>">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <?php
                if (empty($cover_thumb)) {
                ?>

                            <?php
                } else {
                ?>
                                <div class="col-lg-10 col-md-11 col-12">
                                    <div class="bg-post-single">
                                        <div class="bg-post" style="background: url(<?php echo $cover_thumb; ?>);">
                                        </div>
                                    </div>
                                </div>
                                <?php
                }
                ?>

                                    <div class="row d-flex justify-content-center">
                                        <div class="col-lg-8 col-md-9 col-11">
                                            <div class="content-blog">
                                                <div class="header-blog">
                                                    <?php
                                if (function_exists('yoast_breadcrumb')) {
                                    yoast_breadcrumb('<div id="breadcrumbs" class="links-breadcrumb">', '</div>');
                                }
                                the_title('<h1>', '</h1>');
                                ?>
                                                        <div class="date-time-card">
                                                            <span class="data-time">
                                        <?php the_time('j \d\e F \d\e Y') ?>
                                    </span>
                                                            <i class="data-time">&bull;</i>
                                                            <span class="data-time"><?php echo reading_time();  ?></span>
                                                        </div>
                                                        <strong class="excerpt_post"><?php echo strip_tags(the_excerpt()); ?></strong>
                                                </div>
                                                <div class="body-blog">
                                                    <?php the_content(); ?>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="banner-news">
                                            <span class="close-bnn">x</span>
                                            <img class="down-free-intern" src="<?php echo get_template_directory_uri(); ?>/assets/images/icone-engrenage.svg" alt="download-gratuito">
                                            <a href="#newsletter"><span>Baixe gratuitamente</span>
                            <h6>Infográfico Mobilidade Urbana e Covid-19</h6>
                        </a>
                                            <small>Assine a newsletter para concluir o download</small>
                                            <a class="bta" href="#newsletter">EU QUERO</a>
                                            <!-- <span class="arrow-bnn">v</span> -->
                                        </div>
                                        <!-- <div class="banner-news">
                        <span class="close-bnn">x</span>
                        <a href="javascript:void(0)"><span>100% online e gratuito</span>
                            <h6>SUMMIT MOBILIDADE URBANA 2021</h6>
                        </a>
                        <small>17 a 21 de maio</small>
                        <a class="bta" id="event_cta" target="_blank" href="http://bit.ly/3rUvPfl">INSCREVA-SE</a>
                    </div> -->
                                    </div>
                    </div>

                </div>
            </article>
            <?php get_template_part('template-parts/news-component'); ?>

            <?php get_template_part('template-parts/related-posts'); ?>

    </main>
    <!-- #site-content -->

    <?php get_template_part('template-parts/footer-menus-widgets'); ?>

    <?php get_footer(); ?>