<?php
/**
 * Customizer Separator Control settings for this theme.
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

if ( class_exists( 'WP_Customize_Control' ) ) {

	if ( ! class_exists( 'nznmobilidade_Separator_Control' ) ) {
		/**
		 * Separator Control.
		 */
		class nznmobilidade_Separator_Control extends WP_Customize_Control {
			/**
			 * Render the hr.
			 */
			public function render_content() {
				echo '<hr/>';
			}

		}
	}
}
