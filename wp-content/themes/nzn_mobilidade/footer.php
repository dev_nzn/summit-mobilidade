<?php

/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

$has_footer_menu = has_nav_menu('footer');
$has_social_menu = has_nav_menu('social');

$has_sidebar_3 = is_active_sidebar('sidebar-4');
?>
    <footer id="site-footer" role="contentinfo" class="footer-group">

        <div class="section-inner">

            <div class="footer-credits">

                <p class="footer-copyright">&copy;
                    <?php
                echo date_i18n(
                    /* translators: Copyright date format, see https://www.php.net/date */
                    _x('Y', 'copyright date format', 'nznmobilidade')
                );
                ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <?php bloginfo('name'); ?>
                        </a>
                </p>
                <!-- .footer-copyright -->

                <!--<p class="powered-by-wordpress">
							<a href="<?php echo esc_url(__('https://wordpress.org/', 'nznmobilidade')); ?>">
								<?php _e('Powered by WordPress', 'nznmobilidade'); ?>
							</a>
						</p> .powered-by-wordpress -->

            </div>
            <!-- .footer-credits -->

            <a class="to-the-top" href="#site-header">
                <span class="to-the-top-long">
                <?php
                /* translators: %s: HTML character for up arrow. */
                printf(__('To the top %s', 'nznmobilidade'), '<span class="arrow" aria-hidden="true">&uarr;</span>'); ?>
                </span>
                <!-- .to-the-top-long -->
                <span class="to-the-top-short">
                <?php
                /* translators: %s: HTML character for up arrow. */
                printf(__('Up %s', 'nznmobilidade'), '<span class="arrow" aria-hidden="true">&uarr;</span>'); ?>
                </span>
                <!-- .to-the-top-short -->
            </a>
            <!-- .to-the-top -->

        </div>
        <!-- .section-inner -->

    </footer>
    <!-- #site-footer -->
    <?php
if ($has_footer_menu || $has_social_menu || $has_sidebar_3) {
?>

        <aside id="site-footer-last" class="footer-widget-outer-wrapper footer-last" role="complementary">

            <?php if ($has_sidebar_3) { ?>
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="footer-widgets col-12 grid-item">
                        <?php dynamic_sidebar('sidebar-4'); ?>
                    </div>
                </div>
            </div>
            <?php } ?>


        </aside>
        <!-- .footer-widgets-outer-wrapper -->

        <?php } ?>

        <!-- Modal -->
        <div class="modal fade" id="modalInscrevase" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <button type="button" class="close close-btn" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
                    <div class="modal-body">
                        <?php
                $url = get_field('link', 'option');
                if (get_field('titulo', 'option')) { ?>
                            <h2 class="titleDestaqueSmall">
                                <?php the_field('titulo', 'option'); ?>
                            </h2>
                            <p>
                                <strong class="subtitleText"><?php the_field('subtitulo', 'option'); ?></strong>
                                <?php the_field('descricao', 'option'); ?>
                            </p>
                            <?php } ?>

                    </div>
                </div>
            </div>

        </div>
        </div>
        </div>
        <script>
            (function($) {

                $(window).scroll(function() {
                    var header = $(document).scrollTop();
                    var headerHeight = $(".header-prime, .fix-header-especial").outerHeight();
                    if (header > headerHeight) {
                        $(".header-prime, .fix-header-especial").addClass("fixed");
                    } else {
                        $(".header-prime, .fix-header-especial").removeClass("fixed");
                    }
                });

                $('.mini_bio').toggle(function() {
                    $('.mini_bio i').text('-');
                    $('.mini_bio-sub').slideToggle();

                }, function() {
                    $('.mini_bio i').text('+');
                    $('.mini_bio-sub').slideToggle();
                });

                $(".close-bnn, .arrow-bnn").click(function() {
                    $(".banner-news").hide();
                });
            })(jQuery);
        </script>


        <!-- Load font awesome -->
        <script>
            window.addEventListener('load', loadFontAwesome, false);

            function loadFontAwesome() {
                var head = document.getElementsByTagName('head')[0];
                var link = document.createElement('link');
                link.rel = 'stylesheet';
                link.type = 'text/css';
                link.href = 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css';
                link.media = 'all';
                head.appendChild(link);
            }
        </script>

        <?php wp_footer(); ?>

        </body>

        </html>