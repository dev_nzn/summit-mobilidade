
<?php if( get_field('video_embed') ): ?>
<div class="section-page-first">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="embed-container">
                    <?php the_field('video_embed'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
