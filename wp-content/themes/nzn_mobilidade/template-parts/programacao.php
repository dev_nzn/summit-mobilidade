<div id="programacao" class="container">
    <div class="row">
        <div class="col-lg-9 col-md-12">
        <?php if( have_rows('escala_programacao') ): ?>
            <?php while( have_rows('escala_programacao') ): the_row(); 

                $titulo = get_sub_field('titulo');
                $data = get_sub_field('data');
                $botao = get_sub_field('botao');
                ?>
                <div id="escala_programacao">
                <?php if( $titulo ): ?>
                    <h2 class="titleDestaque"><?php echo $titulo; ?></h2>
                <?php endif; ?>
                <?php if( $data ): ?>
                    <?php echo $data; ?>
                <?php endif; ?>
                        <?php 
                            if( have_rows( 'sessoes' ) ):
                            while ( have_rows( 'sessoes' ) ) : the_row();
                            $horario = get_sub_field('horario');
                            $titulo = get_sub_field('titulo');
                            $descricao = get_sub_field('descricao');
                        ?>
                    <div class="box-content">
                        <div class="data-content">
                        <?php if( $horario ): ?>
                            <strong class="detail-hour"><?php echo $horario; ?></strong>
                        <?php endif; ?>
                        </div>
                        <div class="data-content-sessoes">
                        <?php if( $titulo ): ?>
                            <h3 class="nome-editoria-home-h2"><?php echo $titulo; ?></h3>
                        <?php endif; ?>
                        <?php if( $descricao ): ?>
                            <p class="descricao-txt"><?php echo $descricao; ?></p>
                        <?php endif; ?>
                            <?php 
                                if( have_rows( 'paineis' ) ):
                            ?>
                                <div class="data-content-paineis">
                            <?php
                                while ( have_rows( 'paineis' ) ) : the_row();
                                $destaque = get_sub_field('destaque');
                                $titulo = get_sub_field('titulo');
                                $descricao = get_sub_field('descricao');
                                $mini_bio_titulo = get_sub_field('mini_bio_titulo');
                                $mini_bio_descricao = get_sub_field('mini_bio_descricao');

                            ?>
                                <div class="content-paineis">
                                <?php if( $destaque ): ?>
                                    <strong class="subtitle"><?php echo $destaque; ?></strong>
                                <?php endif; ?>
                                <?php if( $titulo ): ?>
                                    <h3 class="subtitle-under"><?php echo $titulo; ?></h3>
                                <?php endif; ?>
                                <?php if( $descricao ): ?>
                                    <p class="descricao-sub"><?php echo $descricao; ?></p>
                                <?php endif; ?>
                                <?php if( $mini_bio_titulo ): ?>
                                    <a class="mini_bio"><?php echo $mini_bio_titulo; ?><i>+</i></a>
                                <?php endif; ?>
                                <?php if( $mini_bio_descricao ): ?>
                                    <p class="mini_bio-sub"><?php echo $mini_bio_descricao; ?></p>
                                <?php endif; ?>
                                </div>
                            <?php
                                endwhile;
                            ?>
                            </div>
                            <?php
                            endif; 
                            wp_reset_postdata();
                            ?>
                        </div>
                    </div>
                        <?php
                            endwhile;
                        endif; 
                        wp_reset_postdata();
                        ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <?php if( $botao ): 
                            $link_url = $botao['url'];
                            $link_title = $botao['title'];
                            $link_target = $botao['target'] ? $botao['target'] : '_self';
                        ?>
                            <p class="entry-btn "><a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a></p>
                        <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; 
        wp_reset_postdata();?>
        </div>
    </div>
</div>