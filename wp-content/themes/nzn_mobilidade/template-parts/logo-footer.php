<?php 
    if( have_rows('logo_footer', 'option') ):
        while( have_rows('logo_footer', 'option') ): the_row();

        $logo = get_sub_field('logo');
        $link_logo = get_sub_field('link_logo');

        $link_url = $link_logo['url'];
        $link_title = $link_logo['title'];
        $link_target = $link_logo['target'] ? $link_logo['target'] : '_self';

?>
    <a rel="noreferrer" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
        <img src="<?php echo $logo; ?>" alt="<?php echo $link_title; ?>" />
    </a>
<?php 
        endwhile;
    endif;
    wp_reset_postdata();
?>