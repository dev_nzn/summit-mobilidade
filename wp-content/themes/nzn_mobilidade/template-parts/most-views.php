<h2>Mais lidas</h2>
<div class="row d-flex align-items-stretch most-posts">
  <?php
  $popular_posts_args = array(
    'posts_per_page' => 5,
    'meta_key' => 'my_post_viewed',
    'orderby' => 'meta_value_num',
    'order'=> 'DESC',
    'post__not_in' => get_option( 'sticky_posts' ) 

  );


   
  $popular_posts_loop = new WP_Query( $popular_posts_args );
  if ($popular_posts_loop->have_posts()) {
    
  while( $popular_posts_loop->have_posts() ):
    $popular_posts_loop->the_post();
  ?>
    <div class="col-12 col-lg-12 col-md-6">
    <!-- Post Title -->
      <a href="<?php the_permalink(); ?>" class="link-content">  
      <!-- Post Image  -->
      <?php if (has_post_thumbnail()): ?>
        <div class="content-image">
          <div class="img-moda-168-104">
               <?php the_post_thumbnail('thumb-mostviews'); ?>
            </div>
        </div>
      <?php endif; ?>
        <div class="content-text">
        <?php 
          $categories = get_the_category();
          if ( !empty( $categories ) ) {
            echo '<span class="simple-category" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</span>';
          }                                        
        ?>
          <h3 class="model-title-most-posts"><?php the_title(); ?></h3>
        </div>
      </a> 
        </div>
  <?php
 
endwhile;
  }
wp_reset_query();
  ?>
</div>