<?php
/**
 * Displays the search icon and modal
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

?>
<div class="search-modal cover-modal header-group" data-modal-target-string=".search-modal">

	<div class="search-modal-inner modal-inner">
	<button class="toggle search-untoggle close-search-toggle fill-children-current-color" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
				<span class="screen-reader-text"><?php _e( 'Close search', 'nznmobilidade' ); ?></span>
				<?php nznmobilidade_the_theme_svg( 'cross' ); ?>
			</button><!-- .search-toggle -->

		<div class="section-inner">

			<?php
			get_search_form(
				array(
					'label' => __( 'Digite o que procura', 'nznmobilidade' ),
				)
			);
			?>

			

		</div><!-- .section-inner -->
		<div class="categories-modal-search">
		<ul>
			<?php wp_list_categories( array(
				'title_li'   => '<h3>Ou navegue</h3>',
				'orderby'    => 'name',
				'show_count' => false,
				'use_desc_for_title' => false
			) ); ?>
		</ul>
		</div>
	</div><!-- .search-modal-inner -->
</div><!-- .menu-modal -->
