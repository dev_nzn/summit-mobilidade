<?php if (have_rows('patrocinadores_list', 'option')) : ?>
    <?php
    $countCols = 0;
    while (have_rows('patrocinadores_list', 'option')) : the_row();
        $titulo = get_sub_field('titulo');
    ?>
        <?php if ($titulo !== 'Apoio') : ?>
            <div style="padding-left:6%!important;padding-top:1% !important;padding-bottom:1% !important;" class="col-auto col-num-<?php echo $countCols;  ?>">
                <div class="align-items-center d-flex flex-column">
                    <?php if ($titulo) : ?>
                        <div class="row">
                            <h4 class="title text-left">
                                <?php echo $titulo; ?>
                            </h4>
                        </div>
                    <?php endif; ?>
                    <?php if (have_rows('marcas')) :
                        $countM = 0;
                    ?>
                        <div class="row" style="flex-wrap: nowrap !important;">
                            <?php while (have_rows('marcas')) : the_row();
                                $imagem = get_sub_field('imagem_da_marca');
                                $link = get_sub_field('link');
                                $tamanhoW = get_sub_field('tamanhow');
                            ?>

                                <?php if ($link) :
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
                                    <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                        <?php if (esc_url($link_url == "https://www.grupoccr.com.br/")) : ?>
                                            <?php $imgClass = "patrocinadores__img--big"; ?>
                                        <?php else : ?>
                                            <?php $imgClass = "patrocinadores__img"; ?>
                                        <?php endif; ?>
                                        <img class="<?php echo $imgClass;  ?> img-<?php echo $countM;  ?>" src="<?php echo esc_url($imagem); ?>" alt="<?php echo esc_attr($link_title); ?>" width="<?php echo $tamanhoW; ?>" style="max-width: <?php echo $tamanhoW; ?>px" />
                                    </a>
                                <?php endif; ?>

                            <?php
                                $countM++;
                            endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php $countCols++;  ?>
        <?php endif; ?>
    <?php
    endwhile;
    ?>

<?php endif;
wp_reset_postdata(); ?>