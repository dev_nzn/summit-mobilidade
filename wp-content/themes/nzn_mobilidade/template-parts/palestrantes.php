

    <?php if( have_rows('adicionar_paletrantes') ): ?>
                <div class="row d-flex justify-content-strach">
                    <?php 
                    while( have_rows('adicionar_paletrantes') ): the_row(); 
                        $foto = get_sub_field('foto');
                        $nome = get_sub_field('nome');
                        $profissao = get_sub_field('profissao');
                        $descricao = get_sub_field('descricao');
                    ?>
                    <div class="col-lg-4">
                       <div class="card">
                            <figure>
                                <?php
                                    if( !empty( $foto ) ): ?>
                                        <img src="<?php echo esc_url($foto['url']); ?>" alt="<?php echo esc_attr($foto['alt']); ?>" />
                                <?php   
                                    endif;
                                ?>
                            </figure>
                            <div class="card-content">
                                <h3><?php echo $nome; ?></h3>
                                <h4><?php echo $profissao; ?></h4>
                                <p><?php echo $descricao; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php 
                        endwhile; 
                    ?>
                </div>
       
    <?php endif; 
    wp_reset_postdata();?>
