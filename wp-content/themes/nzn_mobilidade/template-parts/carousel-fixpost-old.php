<?php

$sticky_posts_args = array(
    'posts_per_page' => 3,
    'post__in'  => get_option( 'sticky_posts' ),
    'order' => 'DESC'
);

$sticky_posts_args = new WP_Query($sticky_posts_args);
     
?>
    <div id="carousel-posts" class="container">
        <div class="row align-self-stretch">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">    
                          

<?php
    $i = 0;
    while ($sticky_posts_args->have_posts()):
    $sticky_posts_args->the_post();
    if($i == 0) {
        $activeClass = 'active';
      }
      else {
        $activeClass = '';
      }
?>
                            <div class="carousel-item <?php echo $activeClass; ?>">
                                <div class="row align-self-stretch">
                                    <?php if(has_post_thumbnail()): ?>
                                    <div class="col-lg-6">
                                    <a href="<?php the_permalink(); ?>">  
                                        <?php the_post_thumbnail('thumb-carousel'); ?>
                                    </a>
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-lg-4">
                                        <?php 
                                        $categories = get_the_category();
                                        if ( ! empty( $categories ) ) {
                                            echo '<a class="categoryTitle" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                        }                                        
                                        ?>
                                        <div class="date-time-card">
                                            <span class="data-time">
                                            <?php the_time('j \d\e F \d\e Y') ?>
                                            </span>
                                            <i class="data-time">&bull;</i>
                                            <span class="data-time"><?php echo reading_time();  ?></span>
                                        </div>
                                        <h2 class="titulo-materia-card-destaque-carrosel">
                                        <a href="<?php the_permalink(); ?>">  
                                            <?php the_title(); ?>
                                    </a>
                                        </h2>

                                        <p class="textinho-pos-titulo-ultimas-noticiaspoppins-—-14pt">
                                        <a href="<?php the_permalink(); ?>">  

                                           <?php 
                                            if ( !has_excerpt() ) {
                                                echo get_the_excerpt();
                                            } else { 
                                                echo get_the_excerpt();
                                            }
                                            ?>
                                                                                    </a>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
    $i++;
    endwhile;
    wp_reset_query();
?>


                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                    </div>
                </div>
            </div>
        </div>
    </div>