<?php
if (is_page('sobre-o-evento')) {
    $posicao = 'p-ver';
    $rowFlex = 'justify-content-center';
    $col = 'col-lg-12 col-md-12';
    $forV = 'd-flex justify-content-around';
} else {
    $posicao = 'p-ver';
    $rowFlex = 'justify-content-center';
    $col = 'col-lg-6 col-md-12';
    $forV = 'd-flex justify-content-around';
}
?>

<?php if (have_rows('patrocinadores_list', 'option')) : ?>
    <div class="<?php echo $col; ?> <?php echo $rowFlex; ?>">
        <?php
        while (have_rows('patrocinadores_list', 'option')) : the_row();
            $titulo = get_sub_field('titulo');
        ?>
            <div class="patrocinadores">
                <?php if ($titulo) : ?>
                    <h4 class="title"><?php echo $titulo; ?></h4>
                <?php endif; ?>
                <div class="align-items-center patrocinadores-box  <?php echo $posicao; ?> <?php echo $forV; ?>">
                    <?php if (have_rows('marcas')) :
                    ?>
                        <?php while (have_rows('marcas')) : the_row();
                            $imagem = get_sub_field('imagem_da_marca');
                            $link = get_sub_field('link');

                        ?>
                            <ul>
                                <li>
                                    <?php if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                            <?php if (esc_url($link_url == "https://www.grupoccr.com.br/")) : ?>
                                                <?php $imgClass = "patrocinadores__img--big"; ?>
                                            <?php else : ?>
                                                <?php $imgClass = "patrocinadores__img"; ?>
                                            <?php endif; ?>
                                            <img class="<?php echo $imgClass; ?>" src="<?php echo esc_url($imagem); ?>" alt="<?php echo esc_attr($link_title); ?>" />
                                        </a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        endwhile;
        ?>
    </div>

<?php endif;
wp_reset_postdata(); ?>