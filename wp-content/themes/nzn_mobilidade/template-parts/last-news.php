<h2>Últimas notícias</h2>
<div class="row d-flex align-items-stretch last-posts">
   <?php
   $recent_posts = array(
      'posts_per_page' => 3,
      'order' => 'DESC',
      'post__not_in' => get_option( 'sticky_posts' ) 
    );
   
   $recent_posts = new WP_Query($recent_posts);
   if ($recent_posts->have_posts()) {
   while ($recent_posts->have_posts()):
      $recent_posts->the_post();
   ?>
   <div class="col-12">
   <!-- Post Title -->
      <a href="<?php the_permalink(); ?>" class="link-content">  
      <!-- Post Image  -->
      <?php if (has_post_thumbnail()){ ?>
         <div class="content-image">
         <?php 
            $categories = get_the_category();
            if ( ! empty( $categories ) ) {
               echo '<span class="categoryTitle" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</span>';
            }                                        
         ?>
            <div class="img-moda-270-168">
               <?php the_post_thumbnail('thumb-lastnews'); ?>
            </div>
         </div>
         <?php } else { 
            $categories = get_the_category();
            if ( ! empty( $categories ) ) {
               echo '<span class="categoryTitle" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</span>';
            }    
          } ?>

         <div class="content-text">
         <div class="date-time-card">
            <span class="data-time"><?php the_time('j \d\e F \d\e Y') ?></span>
            <i class="data-time">&bull;</i>
            <span class="data-time"><?php echo reading_time();  ?></span>
         </div>
         <h3 class="model-title-last-posts"><?php the_title(); ?></h3>
         <p class="model-excerpt-last-posts">
         <?php 
            if ( !has_excerpt() ) {
               echo get_the_excerpt();
            } else { 
               echo get_the_excerpt();
            }
         ?>
         </p>
         </div>
      </a> 
      </div>
   <?php
   endwhile;
}
   wp_reset_query();
   ?>
</div>