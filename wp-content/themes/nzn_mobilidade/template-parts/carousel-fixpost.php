<?php

$sticky_posts_args = array(
    'posts_per_page' => 3,
    'post__in'  => get_option( 'sticky_posts' ),
    'order' => 'DESC',
);

$postslist = get_posts( array('post_type' => 'especiaistype',
    'order' => 'DESC',
    'posts_per_page' => 3,
    'meta_query' => array(
        array(
            'key'   => 'fixar_post',
            'value' => '1',
        )
    ) 
));

$terms = get_terms(
    array(
        'taxonomy'   => 'especiaistax',
        'hide_empty' => false
    )
 );

 $top = '';
 if(get_field('bloco_duas_colunas') && get_field('bloco_duas_colunas')){
    $top = 'mt-5';
 }

$sticky_posts_args = new WP_Query($sticky_posts_args);
     
?>
    <div id="carousel-posts" class="container">
        <div class="row align-self-stretch <?php echo $top; ?>">
            <div class="col-lg-12">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                    <?php 
                    $j = 0;
                    if( $postslist ) {
                        foreach( $postslist as $post ) {
                    if($j < 3 ) { ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $j; ?>" class="<?php if ($j == 0 ) { echo "active"; } ?>"></li>
                    <?php } 
                    $j++;
                        }
                    }

                    $i = 3; 
                    while ($sticky_posts_args->have_posts()):
                        $sticky_posts_args->the_post();
                    if( $i < 6) { ?>
                        <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i; ?>" class="<?php if ( $i == get_the_ID() ) { echo "active"; } ?>"></li>
                    <?php } 
                    $i++;
                    endwhile;
                    
                    ?>                      
                    </ol>
                    <div class="carousel-inner">    
                          

<?php

$j = 0;
    if( $postslist ) {
        foreach( $postslist as $post ) {
            if($j == 0) {
                $activeClass = 'active';
              }
              else {
                $activeClass = '';
              }
              if ($j < 3) {
   
?>
                            <div class="carousel-item <?php echo $activeClass; ?>">
                                <div class="row align-self-stretch">
                                    <?php if(has_post_thumbnail()): ?>
                                    <div class="col-lg-6">
                                    <a href="<?php the_permalink(); ?>">  
                                        <?php the_post_thumbnail('thumb-carousel'); ?>
                                    </a>
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-lg-4">
                                    <?php
                                    if ( ! empty( $terms ) && is_array( $terms ) ) {
   foreach ( $terms as $term ) { ?>

                                    <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
         <h3 style="background:<?php the_field('cor_primaria_do_patrocinador', $term); ?>" class="categoryTitle"><?php echo $term->name; ?></h3>

   </a><?php }} ?>
                                        <?php 

                                        $categories = get_the_category();
                                        if ( ! empty( $categories ) ) {
                                            echo '<a class="categoryTitle" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                        }                                        
                                        ?>
                                        <div class="date-time-card">
                                            <span class="data-time">
                                            <?php the_time('j \d\e F \d\e Y') ?>
                                            </span>
                                            <i class="data-time">&bull;</i>
                                            <span class="data-time"><?php echo reading_time();  ?></span>
                                        </div>
                                        <h2 class="titulo-materia-card-destaque-carrosel">
                                        <a href="<?php the_permalink(); ?>">  
                                            <?php the_title(); ?>
                                    </a>
                                        </h2>

                                        <p class="textinho-pos-titulo-ultimas-noticiaspoppins-—-14pt">
                                        <a href="<?php the_permalink(); ?>">  

                                           <?php 
                                            if ( !has_excerpt() ) {
                                                echo get_the_excerpt();
                                            } else { 
                                                echo get_the_excerpt();
                                            }
                                            ?>
                                                                                    </a>

                                        </p>
                                    </div>
                                </div>
                            </div>
                            <?php
                         }
                         $j++;
                     }
                 }   
                 wp_reset_query();

                 if ($j != 0) {
                    $i = 3;

                 } else {
                    $i = 0;

                 }

    while ($sticky_posts_args->have_posts()):
    $sticky_posts_args->the_post();
    if($i == 0) {
        $activeClass = 'active';
      }
      else {
        $activeClass = '';
      }
     if ($i < 6) {
            ?>
<div class="carousel-item <?php echo $activeClass; ?>">
                                <div class="row align-self-stretch">
                                    <?php if(has_post_thumbnail()): ?>
                                    <div class="col-lg-6">
                                    <a href="<?php the_permalink(); ?>">  
                                        <?php the_post_thumbnail('thumb-carousel'); ?>
                                    </a>
                                    </div>
                                    <?php endif; ?>
                                    <div class="col-lg-4">
                                        <?php 
                                        $categories = get_the_category();
                                        if ( ! empty( $categories ) ) {
                                            echo '<a class="categoryTitle" href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>';
                                        }                                        
                                        ?>
                                        <div class="date-time-card">
                                            <span class="data-time">
                                            <?php the_time('j \d\e F \d\e Y') ?>
                                            </span>
                                            <i class="data-time">&bull;</i>
                                            <span class="data-time"><?php echo reading_time();  ?></span>
                                        </div>
                                        <h2 class="titulo-materia-card-destaque-carrosel">
                                        <a href="<?php the_permalink(); ?>">  
                                            <?php the_title(); ?>
                                    </a>
                                        </h2>

                                        <p class="textinho-pos-titulo-ultimas-noticiaspoppins-—-14pt">
                                        <a href="<?php the_permalink(); ?>">  

                                           <?php 
                                            if ( !has_excerpt() ) {
                                                echo get_the_excerpt();
                                            } else { 
                                                echo get_the_excerpt();
                                            }
                                            ?>
                                                                                    </a>

                                        </p>
                                    </div>
                                </div>
                            </div>
            <?php
             }
             $i++;
             endwhile;
             wp_reset_query();
             
?>


                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
