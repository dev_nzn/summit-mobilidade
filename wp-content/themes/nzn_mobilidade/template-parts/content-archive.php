
<?php
/**
 * The default template for displaying content
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

?>
<div class="col-lg-4 col-md-6 col-12">
	<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
	
	<a href="<?php the_permalink(); ?>">
	<?php
        if ( ! is_search() ) {
            if ( has_post_thumbnail()) {
                $fotoComSem = '';
            } else {
                $fotoComSem = 'noimage';
            }
    ?>
        <div class="image-content-post">
			<?php
			if (has_category()) {
			?>

            	<h3 class="categoryTitle <?php echo $fotoComSem; ?>"><?php
					$category = get_the_category();
					echo $category[0]->cat_name; ?>
				</h3>

			<?php
			} elseif(has_term('', 'especiaistax')) {
				
				$taxonomy = get_the_terms( $post->ID, 'especiaistax' );     
				foreach ( $taxonomy as $tax){
				  ?>
<h3 style="background:<?php the_field('cor_primaria_do_patrocinador', $tax); ?>" class="categoryTitle <?php echo $fotoComSem; ?>">
			<?php  echo $tax->name; ?>
				</h3>
				  <?php
				}
			?>
			
			<?php
				} else {
			?>
				<h3 class="categoryTitle <?php echo $fotoComSem; ?>"><?php the_title( ' ' ); ?></h3>
			<?php
			}
            	get_template_part( 'template-parts/featured-image' );
            ?>
        </div>
    <?php
        }
	?>
	<div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">
        <div class="date-time-card">
            <span class="data-time"><?php the_time('j \d\e F \d\e Y') ?></span>
            <i class="data-time">&bull;</i>
            <span class="data-time"><?php echo reading_time();  ?></span>
        </div>
		<div class="entry-content">
            <?php
            the_title( '<h2 class="titulo-materia-home"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );

			$intro_text_width = '';

            if ( is_singular() || is_page_template() ) {
                $intro_text_width = ' small';
            } else {
                $intro_text_width = ' thin';
            }
    
            if ( has_excerpt() && is_singular() || has_excerpt() && is_page_template() ) {
            ?>
            <!-- <div class="intro-text section-inner max-percentage<?php echo $intro_text_width; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>">
                <?php the_excerpt(); ?>
            </div> -->
            <?php
            }
			?>
		</div><!-- .entry-content -->
	</div><!-- .post-inner -->
	<div class="section-inner">
		<?php
		wp_link_pages(
			array(
				'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'nznmobilidade' ) . '"><span class="label">' . __( 'Pages:', 'nznmobilidade' ) . '</span>',
				'after'       => '</nav>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			)
		);

		edit_post_link();

		// // Single bottom post meta.
		// nznmobilidade_the_post_meta( get_the_ID(), 'single-bottom' );

		// if ( post_type_supports( get_post_type( get_the_ID() ), 'author' ) && is_single() ) {

		// 	get_template_part( 'template-parts/entry-author-bio' );

		// }
		?>

	</div><!-- .section-inner -->

	<?php

	// if ( is_single() ) {

	// 	get_template_part( 'template-parts/navigation' );

	// }

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */

	?>
</a>
    </article><!-- .post -->
</div>    