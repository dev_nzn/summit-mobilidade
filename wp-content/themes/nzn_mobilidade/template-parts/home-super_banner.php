<?php
        if (has_term('', 'especiaistax')) {

        $taxonomy = get_the_terms( $post->ID, 'especiaistax' );     
        foreach ( $taxonomy as $tax){
        if( have_rows('super_banner', $tax) ):

            while( have_rows('super_banner', $tax) ): the_row();

            $banner = get_sub_field('banner', $tax);
            $link = get_sub_field('link', $tax);

                

        if( $link ){
            
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                
                ?>
            <div class="supper_banner">
            <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                <img src="<?php echo esc_url( $banner ); ?>" alt="<?php echo esc_html( $link_title ); ?>" />
            </a>
            </div>
<?php
        }
    endwhile;

    endif;
    }
} else  {
    if( have_rows('super_banner_gerais', 'option') ):

        while( have_rows('super_banner_gerais', 'option') ): the_row();

        $banner = get_sub_field('banner');
        $link = get_sub_field('link');

            

    if( $link ){
        
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            
            ?>
        <div class="supper_banner">
        <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <img src="<?php echo esc_url( $banner ); ?>" alt="<?php echo esc_html( $link_title ); ?>" />
        </a>
        </div>
<?php
    }
endwhile;
endif;

}

?>
