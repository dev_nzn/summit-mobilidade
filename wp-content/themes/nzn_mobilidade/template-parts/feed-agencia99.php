<div class="row d-flex align-items-stretch">
    <?php

   //$link = "http://patrocinados.estadao.com.br/medialab/rss2SummitMobilidade.php?tkn=AtpvqPBGkl5Hkk99Glkp0sdBBzlkndsr";
	$link = "https://patrocinados.estadao.com.br/medialab/rss2SummitMobilidade.php?tkn=AtpvqPBGkl5Hkk99Glkp0sdBBzlkndsr";
   $context  = stream_context_create(array('http' => array('header' => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36')));
   $xml = file_get_contents($link, false, $context);
   $xmlFeed = simplexml_load_string($xml);

if ($xmlFeed === false) {
echo "Failed loading XML\n";
foreach(libxml_get_errors() as $error) {
    echo "\t", $error->message;
}
}

   $items = $xmlFeed -> channel;

   $limit = 10;
   $i = 0;
   foreach($items -> item as $item){
      if($i < $limit){ 
      ?>
        <div class="col-lg-3 col-md-6 col-12 pb-5">
            <a target="_blank" href="//<?php echo $item -> link; ?>" class="link-content">
                <div class="image-content-post">
                    <?php 
               
                    $categoryLimit = 1;
                    $c = 0;
                    foreach($item -> category as $category) {
                       if($c < $categoryLimit){
                          echo '<h3 class="categoryTitle "> ' . esc_html( $category ) . ' </h3> '; 
                       }
                       $c++;
                    }                                        
                 ?>
                    <div class="img-moda-255-168">
                        <?php 
                  $description = $item -> description;
                  $imageUrl = $item->enclosure['url'];
                  $imgTemp = $imageUrl; //'http://patrocinados.estadao.com.br/medialab/wp-content/themes/sladmag/images/logo-media-lab2.png';
                  echo  '<img src="//'.$imageUrl.'">'; 
                  ?>
                    </div>
                </div>
                <div class="content-text">

                    <h3 class="titulo-materia-home">
                        <?php echo $item -> title; ?>
                    </h3>
                </div>
            </a>
        </div>
        <?php
      }
      $i++;
   }
   ?>
</div>