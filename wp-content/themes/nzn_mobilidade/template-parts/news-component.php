<?php
$has_sidebar_2 = is_active_sidebar('sidebar-2');

?>
    <div id="newsletter" class="second-section">

        <div class="newsletter">
            <div class="container">

                <div class=" d-lg-flex  d-block align-items-stretch ">
                    <div class="col-md-12 col-lg-6">
                        <span class="title">Cadastre-se na newsletter</span>
                        <small>e receba conteúdos semanais sobre o mobilidade</small>
                        <img class="down-free" src="<?php echo get_template_directory_uri(); ?>/assets/images/download-gratuito.svg" alt="download-gratuito">
                    </div>
                    <?php if ($has_sidebar_2) { ?>
                    <?php dynamic_sidebar('sidebar-2'); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>