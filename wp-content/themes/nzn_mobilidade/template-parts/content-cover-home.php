<?php
/**
 * The default template for displaying content
 *
 * Used for template home .
 * *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */
$entry_header_classes = '';

$cover_thumb = wp_get_attachment_url(get_post_thumbnail_id($post->ID) ); 

?>
    <article class="overlay-img" style="background: url(<?php echo $cover_thumb; ?>);" <?php post_class(); ?> id="post-
		<?php the_ID(); ?>">
		<?php
			if( get_field('exibir_scroll_down')){
		?>
			<a href="<?php the_field('hash_id_da_sessao'); ?>"  class="icon-scroll">
				<img src="<?php echo get_template_directory_uri() ?>/assets/images/icones/icone_down.svg" class="icon-scroll" />
			</a>
		<?php
			}
		?>
        <header class="entry-header has-text-align-center<?php echo esc_attr( $entry_header_classes ); ?>">
            <div class="content-home-header ">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-7 col-md-9 col-12">
                            <div class="entry-header-inner section-inner medium">

                                <?php
    $show_categories = apply_filters( 'nznmobilidade_show_categories_in_entry_header', true );

	if ( true === $show_categories && has_category() ) {
		?>

                                    <div class="entry-categories">
                                        <span class="screen-reader-text"><?php _e( 'Categories', 'nznmobilidade' ); ?></span>
                                        <div class="entry-categories-inner">
                                            <?php the_category( ' ' ); ?>
                                        </div>
                                        <!-- .entry-categories-inner -->
                                    </div>
                                    <!-- .entry-categories -->

                                    <?php
	}

	if ( is_singular() ) {
		the_title( '<h1 class="entry-title">', '</h1>' );
	} else {
		the_title( '<h2 class="entry-title heading-size-1"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
	}

	$intro_text_width = '';

	if ( is_singular() ) {
		$intro_text_width = ' small';
	} else {
		$intro_text_width = ' thin';
	}

	if ( has_excerpt() && is_singular() ) {
    ?>

                                        <!-- <div class="intro-text section-inner max-percentage<?php echo $intro_text_width; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>">
                                            <?php the_excerpt(); ?>
                                        </div> -->

                                        <?php
	}

	// Default to displaying the post meta.
	nznmobilidade_the_post_meta( get_the_ID(), 'single-top' );
	?>
                            </div>
                            <div class="post-inner <?php echo is_page_template( 'templates/template-full-width.php' ) ? '' : 'thin'; ?> ">

                                <div class="entry-content">

                                    <?php
			if ( is_search() || ! is_singular() && 'summary' === get_theme_mod( 'blog_content', 'full' ) ) {
				the_excerpt();
			} else {
				the_content( __( 'Continue reading', 'nznmobilidade' ) );
			}
			?>

                                </div>
                                <!-- .entry-content -->

                            </div>
                            <!-- .post-inner -->

                            <div class="section-inner">
                                <?php
		wp_link_pages(
			array(
				'before'      => '<nav class="post-nav-links bg-light-background" aria-label="' . esc_attr__( 'Page', 'nznmobilidade' ) . '"><span class="label">' . __( 'Pages:', 'nznmobilidade' ) . '</span>',
				'after'       => '</nav>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			)
		);

		edit_post_link();

		// Single bottom post meta.
		nznmobilidade_the_post_meta( get_the_ID(), 'single-bottom' );

		if ( post_type_supports( get_post_type( get_the_ID() ), 'author' ) && is_single() ) {

			get_template_part( 'template-parts/entry-author-bio' );

		}
		?>

                            </div>
                            <!-- .section-inner -->

                            <?php

	if ( is_single() ) {

		get_template_part( 'template-parts/navigation' );

	}

	/**
	 *  Output comments wrapper if it's a post, or if comments are open,
	 * or if there's a comment number – and check for password.
	 * */
	if ( ( is_single() || is_page() ) && ( comments_open() || get_comments_number() ) && ! post_password_required() ) {
		?>

                                <div class="comments-wrapper section-inner">

                                    <?php comments_template(); ?>

                                </div>
                                <!-- .comments-wrapper -->

                                <?php
	}
	?>
                        </div>
                    </div>
                </div>
            </div>
        </header>

    </article>
    <!-- .post -->