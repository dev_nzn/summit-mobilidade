
<h2 class="titleImage">Mobilidade</h2>
<div class="row d-flex align-items-stretch" >
   <?php
   $link = "https://mobilidade.estadao.com.br/feed";
   $context  = stream_context_create(array('http' => array('header' => 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36')));
   $xml = file_get_contents($link, false, $context);
   $xmlFeed = simplexml_load_string($xml);
   $items = $xmlFeed -> channel;
   $limit = 4;
   $i = 0;
   foreach($items -> item as $item){
      if($i < $limit){ 
      ?>
      <div class="col-lg-3 col-md-6 col-12">
         <a target="_blank" href="<?php echo $item -> link; ?>" class="link-content">  
            <div class="content-image">
               <div class="img-moda-255-168">
                  <?php 
                  $description = $item -> description;
                  $imageUrl = explode('"', $description)[1];
                  echo  '<img src="'.$imageUrl.'">'; 
                  ?> 
               </div>
            </div>
            <div class="content-text">
            <?php 
               
               $categoryLimit = 1;
               $c = 0;
               foreach($item -> category as $category) {
                  if($c < $categoryLimit){
                     echo '<span class="simple-category"> ' . esc_html( $category ) . ' </span> '; 
                  }
                  $c++;
               }                                        
            ?>
            <h3 class="model-title-last-posts"><?php echo $item -> title; ?></h3>
            </div>
         </a> 
      </div>
      <?php
      }
      $i++;
   }
   ?>
</div>