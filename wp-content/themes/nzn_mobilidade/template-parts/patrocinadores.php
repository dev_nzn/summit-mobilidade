<?php
if (is_page('sobre-o-evento') || is_page('regulamento-premio-vozes-da-mobilidade')) {
    $displayfb = 'block';
    $colBlock = '';
    $posicao = 'p-ver';
    $rowFlex = 'justify-content-center';
    $col = 'col-lg-12 col-md-12';
    $forV = 'd-lg-flex d-md-flex justify-content-around';
    $posText = 'text-center';
    $imgP = 'margin-right: 0';
} else if (is_front_page()) {
    $displayfb = 'flex';
    $colBlock = 'col-md-4 col-12';
    $posicao = 'p-ver-home';
    $rowFlex = 'justify-content-lg-between justify-content-md-between justify-content-between';
    $col = 'row';
    $forV = 'd-lg-flex d-md-flex justify-content-start';
    $posText = 'text-center';
    $imgP = 'margin-right: 20px';
} else {
    $displayfb = 'block';
    $colBlock = '';
    $posicao = 'p-ver';
    $rowFlex = 'justify-content-center';
    $col = 'col-lg-6 col-md-12';
    $forV = 'd-lg-flex d-md-flex justify-content-around';
    $posText = 'text-center';
    $imgP = 'margin-right: 0';
}
?>

<?php if (have_rows('patrocinadores_list', 'option')) : ?>
    <div class="<?php echo $displayfb; ?> <?php echo $col; ?> <?php echo $rowFlex; ?>">
        <?php
        $countCols = 0;
        while (have_rows('patrocinadores_list', 'option')) : the_row();
            $titulo = get_sub_field('titulo');
        ?>
            <div class="patrocinadores   <?php if (is_front_page() && $countCols >= 3) {
                                                echo 'col-md-12 col-12';
                                            } else {
                                                echo $colBlock;
                                            } ?>">
                <?php if ($titulo) : ?>
                    <h4 style="<?php if (is_front_page()) {
                                    echo "margin-bottom:5%";
                                }; ?>" class="patocinadores__title title <?php echo $posText; ?>">
                        <?php echo $titulo; ?>
                    </h4>
                <?php endif; ?>
                <div class="align-items-center patrocinadores-box  <?php echo $posicao; ?> <?php echo $forV; ?>">
                    <?php if (have_rows('marcas')) :
                        $countM = 0;
                    ?>
                        <ul style="<?php if ($countCols >= 3) {
                                        echo "justify-content: space-between;";
                                    } else {
                                        echo "justify-content: center;";
                                    } ?>" style="<?php if ($countCols >= 2) {
                                                        echo $imgP;
                                                    } ?>">

                            <?php while (have_rows('marcas')) : the_row();
                                $imagem = get_sub_field('imagem_da_marca');
                                $link = get_sub_field('link');
                                $tamanhoW = get_sub_field('tamanhow');
                            ?>
                                <li>

                                    <?php if ($link) :
                                        $link_url = $link['url'];
                                        $link_title = $link['title'];
                                        $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                        <a href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
                                            <?php if (esc_url($link_url == "https://www.grupoccr.com.br/")) : ?>
                                                <?php $imgClass = "patrocinadores__img--big"; ?>
                                            <?php else : ?>
                                                <?php $imgClass = "patrocinadores__img"; ?>
                                            <?php endif; ?>
                                            <img class="<?php echo $imgClass;  ?> <?php echo $countM;  ?>" src="<?php echo esc_url($imagem); ?>" alt="<?php echo esc_attr($link_title); ?>" width="<?php echo $tamanhoW; ?>" style="max-width: <?php echo $tamanhoW; ?>px" />
                                        </a>
                                    <?php endif; ?>

                                    <?php if ($link_url == "https://linktr.ee/cidadeesperanca") : ?>
                                        <br>
                                        <a style="color:#4f3f99 !important;" href="https://linktr.ee/cidadeesperanca" target="_blank">
                                            <span>Programa - Trânsito na Cidade</span>
                                        </a>
                                    <?php endif; ?>
                                </li>
                            <?php
                                $countM++;
                            endwhile; ?>
                        </ul>

                    <?php endif; ?>
                </div>
            </div>
        <?php
            $countCols++;
        endwhile;
        ?>
    </div>

<?php endif;
wp_reset_postdata(); ?>