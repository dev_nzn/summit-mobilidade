
<h2 class="footer-title"><?php the_field('titulo', 'option'); ?></h2>
<?php ?>
<ul class="social-list">
<?php 
    if( have_rows('redes_sociais', 'option') ):
        while( have_rows('redes_sociais', 'option') ): the_row(); 
            $icone_class = get_sub_field('icone_class');
            $link = get_sub_field('link');
            $nome = get_sub_field('nome');
?>
    <li>
        <?php
            if( $icone_class ): 
        ?>
        <a href="<?php echo esc_url( $link ); ?>" class="<?php echo  $icone_class; ?>" rel="noreferrer" target="_blank">
            <span class="namefield"><?php echo  $nome; ?></span>
        </a>
        <?php endif; 
        ?>
    </li>
<?php   endwhile;  
    endif;
    wp_reset_postdata();
?>  
</ul>