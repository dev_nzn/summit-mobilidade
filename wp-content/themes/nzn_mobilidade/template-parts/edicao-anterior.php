<?php if( have_rows('edicoes_anteriores') ): ?>

<div class="gallery">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">

                <?php 
                $edicao = 0;
                while( have_rows('edicoes_anteriores') ): the_row(); 
                    $titulo = get_sub_field('titulo');
                    $imagens = get_sub_field('galeria');
                ?>
                <div class="gallery-content">

                    <?php if( $titulo ): ?>
                        <h2 class="titleDestaque"><?php echo $titulo; ?></h2>
                    <?php endif; ?>
                        <?php 
                        if( $imagens ): ?>
                            <ul>
                                <?php 
                                $modal = 0;
                                foreach( $imagens as $image ): ?>
                                    <li>
                                        <button type="button" class="btn-image-modal" data-toggle="modal" data-target="#modalimage-<?php echo $edicao; ?>-<?php echo $modal; ?>">
                                                <img src="<?php echo $image; ?>" alt="<?php echo $edicao; ?>" />
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="modalimage-<?php echo $edicao; ?>-<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="modal-body">
                                                    <img src="<?php echo $image; ?>" alt="<?php echo $edicao; ?>" />
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php 
                            $modal++;
                            endforeach; ?>
                            </ul>
                        <?php endif; ?>
                </div>

                <?php 
                    $edicao++;
                    endwhile; 
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; 
                wp_reset_postdata();?>