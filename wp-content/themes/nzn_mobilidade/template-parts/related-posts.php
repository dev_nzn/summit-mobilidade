<?php global $wpdb;
?>
<div class="related-content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="titlePostList">Você também pode gostar:</h3>

                    <div class="row post-list">
                            <?php
                            
                            $related = new WP_Query(
                                array(
                                    'post_type' => 'post',

                            'category__in'   => wp_get_post_categories( $post->ID ),
                            'posts_per_page' => 3,
                            'post__not_in'   => array( $post->ID )
                                )
                            );
                            if( $related->have_posts() ) { 
                                    $limit = 3;
                                    $i = 0;
                                    while( $related->have_posts() ): 
                                        $related->the_post();
                                        if($i < $limit){ 
                                            get_template_part( 'template-parts/content-archive', get_post_type() );
                                        }
                                    $i++;
                                    endwhile;
                                wp_reset_postdata();
                            }
                            ?>
                        </div>
            </div>
        </div>
    </div>
</div>