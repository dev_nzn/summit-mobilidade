<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

$entry_header_classes = '';
$blog_page_image = wp_get_attachment_url( get_post_thumbnail_id(get_option( 'page_for_posts' )) );

if ( is_singular() ) {
	$entry_header_classes .= ' header-group';
}

?>

    <header class="entry-header tax has-text-align-left<?php echo esc_attr( $entry_header_classes ); ?>">

        <div class="entry-header-inner section-inner ">

            <?php
			/**
			 * Allow child themes and plugins to filter the display of the categories in the entry header.
			 *
			 * @since NZN Mobilidade 1.0
			 *
			 * @param bool   Whether to show the categories in header, Default true.
			 */
		$show_categories = apply_filters( 'nznmobilidade_show_categories_in_entry_header', true );
		if ( is_singular() || is_page_template() || is_home() ) {
				
				?>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<div id="breadcrumbs" class="links-breadcrumb">','</div>' );
						}
					the_title( '<h1 class="entry-title nome-categoria-paginas-internas">', '</h1>' );
					?>
                        </div>
                    </div>
                </div>

                <?php
			the_post_thumbnail ('size', array (
				'class' => 'feature-image-title'
			));
			} elseif ( true === $show_categories && has_category() ) {
				?>
                    <img src="<?php 
					echo $blog_page_image; ?>" class="feature-image-title wp-post-image" alt="noticias" height="125.042" width="803.327">
                    <?php
			} else {
			the_title( '<h2 class="entry-title heading-size-1"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
		}

		$intro_text_width = '';

		if ( is_singular() || is_page_template() ) {
			$intro_text_width = ' small';
		} else {
			$intro_text_width = ' thin';
		}

		if ( has_excerpt() && is_singular() || has_excerpt() && is_page_template() ) {
			?>

                        <!-- <div class="intro-text section-inner max-percentage<?php echo $intro_text_width; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- static output ?>">
                            <?php the_excerpt(); ?>
                        </div> -->

                        <?php
		}

		// Default to displaying the post meta.
		// nznmobilidade_the_post_meta( get_the_ID(), 'single-top' );
		?>


        </div>
        <!-- .entry-header-inner -->


    </header>
    <!-- .entry-header -->

    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="descricao-page-posts">

					<?php 
					if (is_page( array('especiais', 'especiaistype') ) ){

						the_content();
					} else {
						echo get_the_post_type_description('');
					}
					?>
                </div>
            </div>
        </div>
    </div>

	<!-- .entry-header - descripition -->
