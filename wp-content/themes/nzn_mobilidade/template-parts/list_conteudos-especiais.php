<?php
$page_id = 'especiais';  
$page = get_page_by_title( $page_id );
if ($page->post_status == 'publish') {
?>
    <h2><a href="<?php echo get_site_url(); ?>/especiais">Especiais</a></h2>

    <div class="row d-flex align-items-stretch">

        <?php 
$terms = get_terms(
   array(
       'taxonomy'   => 'especiaistax',
       'hide_empty' => false
   )
);

if ( ! empty( $terms ) && is_array( $terms ) ) {
   $i = 0;
   foreach ( $terms as $term ) { 
      $i++;
      if($i <= 2){

         ?>
        <div class="col-lg-4 col-md-6 col-12 pb-3">
            <div class="content-image">
                <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
         <h3 style="background:<?php the_field('cor_primaria_do_patrocinador', $term); ?>" class="categoryTitle"><?php echo $term->name; ?></h3>

      </a>
                <div class="img-moda-350-230">
                    <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="link-content">  
            <img src="<?php the_field('logo_patrocinador', $term) ?>"  /></a>
                </div>
            </div>

            <div class="content-text">
                <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="link-content">

                    <div class="date-time-card">
                        <span class="data-time"><?php the_time('j \d\e F \d\e Y') ?></span>

                    </div>
                    <h3 class="model-title-last-posts">
                        <?php echo $term->description; ?>
                    </h3>
                </a>
            </div>


        </div>
        <?php

      }

?>

            <?php
   }
?>
                <div class="col-4">
                    <div id="arroba-banner">
                        <?php
            if( have_rows('arroba_banner_gerais', 'option') ):

               while( have_rows('arroba_banner_gerais', 'option') ): the_row();
       
               $banner = get_sub_field('banner');
               $link = get_sub_field('link');
       
                   
       
           if( $link ){
               
                   $link_url = $link['url'];
                   $link_title = $link['title'];
                   $link_target = $link['target'] ? $link['target'] : '_self';
                   
                   ?>
                            <div class="arroba_banner">
                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                   <img src="<?php echo esc_url( $banner ); ?>" alt="<?php echo esc_html( $link_title ); ?>" />
               </a>
                            </div>
                            <?php
           }
       endwhile;
       endif;
         ?>
                    </div>
                </div>
                <?php
} 
?>


    </div>
    <?php
}
?>