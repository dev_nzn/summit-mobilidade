<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

$entry_header_classes = '';

if ( is_singular() ) {
	$entry_header_classes .= ' header-group';
}

?>

    <header class="entry-header has-text-align-left<?php echo esc_attr( $entry_header_classes ); ?>">
       
                    <div class="entry-header-inner section-inner ">

                        <?php
			/**
			 * Allow child themes and plugins to filter the display of the categories in the entry header.
			 *
			 * @since NZN Mobilidade 1.0
			 *
			 * @param bool   Whether to show the categories in header, Default true.
			 */
		$show_categories = apply_filters( 'nznmobilidade_show_categories_in_entry_header', true );

		if ( true === $show_categories && has_category() ) {
			?>

                            <div class="entry-categories">
                                <span class="screen-reader-text"><?php _e( 'Categories', 'nznmobilidade' ); ?></span>
                                <div class="entry-categories-inner">
                                    <?php the_category( ' ' ); ?>
                                </div>
                                <!-- .entry-categories-inner -->
                            </div>
                            <!-- .entry-categories -->

                            <?php
		}

		if ( is_singular() || is_page_template() || is_home() ) {
				
				?>
				<?php
					the_title( '<h1 class="entry-title  nome-categoria-paginas-internas">', '</h1>' );
					?>
			<?php
			the_post_thumbnail ('size', array (
				'class' => 'feature-image-title'
			));
			} else {
				
			the_title( '<h2 class="entry-title heading-size-1"><a href="' . esc_url( get_permalink() ) . '">', '</a></h2>' );
		}


	

		// Default to displaying the post meta.
		nznmobilidade_the_post_meta( get_the_ID(), 'single-top' );
		?>

                    </div>
                    <!-- .entry-header-inner -->
    

    </header>
    <!-- .entry-header -->