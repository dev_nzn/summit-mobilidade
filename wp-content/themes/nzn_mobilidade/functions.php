<?php

/**
 * NZN Mobilidade functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

/**
 * Table of Contents:
 * Theme Support
 * Required Files
 * Register Styles
 * Register Scripts
 * Register Menus
 * Custom Logo
 * WP Body Open
 * Register Sidebars
 * Enqueue Block Editor Assets
 * Enqueue Classic Editor Styles
 * Block Editor Settings
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function nznmobilidade_theme_support()
{

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	// Custom background color.
	add_theme_support(
		'custom-background',
		array(
			'default-color' => 'f5efe0',
		)
	);

	// Set content-width.
	global $content_width;
	if (!isset($content_width)) {
		$content_width = 580;
	}

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */


	add_theme_support('post-thumbnails');

	// Set post thumbnail size.
	set_post_thumbnail_size(1200, 9999);

	// Add custom image size used in Cover Template.
	add_image_size('nznmobilidade-fullscreen', 1980, 9999);
	add_image_size('full-destaque', 970, 460, true); // Unlimited Height Mode
	add_image_size('thumb-carousel', 560, 320, true); // Hard Crop Mode
	add_image_size('thumb-lastnews', 270, 168, true); // Soft Crop Mode
	add_image_size('thumb-mostviews', 168, 104, true); // Unlimited Height Mode
	add_image_size('thumb-geral', 290, 230, true); // Unlimited Height Mode


	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if (get_theme_mod('retina_logo', false)) {
		$logo_width  = floor($logo_width * 2);
		$logo_height = floor($logo_height * 2);
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag');

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on NZN Mobilidade, use a find and replace
	 * to change 'nznmobilidade' to the name of your theme in all the template files.
	 */
	load_theme_textdomain('nznmobilidade');

	// Add support for full and wide align images.
	add_theme_support('align-wide');

	// Add support for responsive embeds.
	add_theme_support('responsive-embeds');

	// acf configurações de tema
	if (function_exists('acf_add_options_page')) {

		acf_add_options_page(array(
			'page_title' 	=> 'Configurações de tema',
			'menu_title'	=> 'Configurações de tema',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Tema header configuração',
			'menu_title'	=> 'Header',
			'parent_slug'	=> 'theme-general-settings',
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> 'Tema footer configuração',
			'menu_title'	=> 'Footer',
			'parent_slug'	=> 'theme-general-settings',
		));
	}

	/*
	 * Adds starter content to highlight the theme on fresh sites.
	 * This is done conditionally to avoid loading the starter content on every
	 * page load, as it is a one-off operation only needed once in the customizer.
	 */
	if (is_customize_preview()) {
		require get_template_directory() . '/inc/starter-content.php';
		add_theme_support('starter-content', nznmobilidade_get_starter_content());
	}

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	/*
	 * Adds `async` and `defer` support for scripts registered or enqueued
	 * by the theme.
	 */
	$loader = new nznmobilidade_Script_Loader();
	add_filter('script_loader_tag', array($loader, 'filter_script_loader_tag'), 10, 2);
}

add_action('after_setup_theme', 'nznmobilidade_theme_support');

/**
 * REQUIRED FILES
 * Include required files.
 */
require get_template_directory() . '/inc/template-tags.php';

// Handle SVG icons.
require get_template_directory() . '/classes/class-nznmobilidade-svg-icons.php';
require get_template_directory() . '/inc/svg-icons.php';

// Handle Customizer settings.
require get_template_directory() . '/classes/class-nznmobilidade-customize.php';

// Require Separator Control class.
require get_template_directory() . '/classes/class-nznmobilidade-separator-control.php';

// Custom comment walker.
require get_template_directory() . '/classes/class-nznmobilidade-walker-comment.php';

// Custom page walker.
require get_template_directory() . '/classes/class-nznmobilidade-walker-page.php';

// Custom script loader class.
require get_template_directory() . '/classes/class-nznmobilidade-script-loader.php';

// Non-latin language handling.
require get_template_directory() . '/classes/class-nznmobilidade-non-latin-languages.php';

// Custom CSS.
require get_template_directory() . '/inc/custom-css.php';


/**
 * Register and Enqueue Styles.
 */
function nznmobilidade_register_styles()
{

	$theme_version = wp_get_theme()->get('Version');

	wp_enqueue_style('nznmobilidade-style', get_stylesheet_uri(), array(), $theme_version);
	wp_style_add_data('nznmobilidade-style', 'rtl', 'replace');

	// Add output of Customizer settings as inline style.
	wp_add_inline_style('nznmobilidade-style', nznmobilidade_get_customizer_css('front-end'));

	// Add print CSS.
	wp_enqueue_style('nznmobilidade-print-style', get_template_directory_uri() . '/print.css', null, $theme_version, 'print');

	// bootstrap CSS.
	wp_enqueue_style('nznmobilidade-boostrap-style', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.min.css', null, $theme_version);

	// carousel CSS.
	wp_enqueue_style('nznagro-carousel-style', get_template_directory_uri() . '/assets/sass/carousel.css', null, $theme_version);

	// bootstrap SCSS.
	wp_enqueue_style('nznmobilidade-scss-style', get_template_directory_uri() . '/assets/sass/theme_style.css', null, $theme_version);
}

add_action('wp_enqueue_scripts', 'nznmobilidade_register_styles');

/**
 * Register and Enqueue Scripts.
 */
function nznmobilidade_register_scripts()
{

	$theme_version = wp_get_theme()->get('Version');

	if ((!is_admin()) && is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}

	wp_enqueue_script('nznmobilidade-js', get_template_directory_uri() . '/assets/js/index.js', array(), $theme_version, false);
	wp_script_add_data('nznmobilidade-js', 'async', true);

	wp_enqueue_script('carousel', get_template_directory_uri() . '/assets/js/carousel.js', array('jquery'), $theme_version, false);
	wp_script_add_data('carousel', 'async', true);

	wp_enqueue_script('tabs', get_template_directory_uri() . '/assets/js/tabs.js', array('jquery'), $theme_version, false);
	wp_script_add_data('tabs', 'async', true);
}

add_action('wp_enqueue_scripts', 'nznmobilidade_register_scripts');

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function nznmobilidade_skip_link_focus_fix()
{
	// The following is minified via `terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
?>
	<script>
		/(trident|msie)/i.test(navigator.userAgent) && document.getElementById && window.addEventListener && window.addEventListener("hashchange", function() {
			var t, e = location.hash.substring(1);
			/^[A-z0-9_-]+$/.test(e) && (t = document.getElementById(e)) && (/^(?:a|select|input|button|textarea)$/i.test(t.tagName) || (t.tabIndex = -1), t.focus())
		}, !1);
	</script>
<?php
}
add_action('wp_print_footer_scripts', 'nznmobilidade_skip_link_focus_fix');

/** Enqueue non-latin language styles
 *
 * @since NZN Mobilidade 1.0
 *
 * @return void
 */
function nznmobilidade_non_latin_languages()
{
	$custom_css = nznmobilidade_Non_Latin_Languages::get_non_latin_css('front-end');

	if ($custom_css) {
		wp_add_inline_style('nznmobilidade-style', $custom_css);
	}
}

add_action('wp_enqueue_scripts', 'nznmobilidade_non_latin_languages');

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function nznmobilidade_menus()
{

	$locations = array(
		'primary'  => __('Desktop Horizontal Menu', 'nznmobilidade'),
		'expanded' => __('Desktop Expanded Menu', 'nznmobilidade'),
		'mobile'   => __('Mobile Menu', 'nznmobilidade'),
		'footer'   => __('Footer Menu', 'nznmobilidade'),
		'social'   => __('Social Menu', 'nznmobilidade'),
	);

	register_nav_menus($locations);
}

add_action('init', 'nznmobilidade_menus');

/**
 * Get the information about the logo.
 *
 * @param string $html The HTML output from get_custom_logo (core function).
 *
 * @return string $html
 */
function nznmobilidade_get_custom_logo($html)
{

	$logo_id = get_theme_mod('custom_logo');

	if (!$logo_id) {
		return $html;
	}

	$logo = wp_get_attachment_image_src($logo_id, 'full');

	if ($logo) {
		// For clarity.
		$logo_width  = esc_attr($logo[1]);
		$logo_height = esc_attr($logo[2]);

		// If the retina logo setting is active, reduce the width/height by half.
		if (get_theme_mod('retina_logo', false)) {
			$logo_width  = floor($logo_width / 2);
			$logo_height = floor($logo_height / 2);

			$search = array(
				'/width=\"\d+\"/iU',
				'/height=\"\d+\"/iU',
			);

			$replace = array(
				"width=\"{$logo_width}\"",
				"height=\"{$logo_height}\"",
			);

			// Add a style attribute with the height, or append the height to the style attribute if the style attribute already exists.
			if (strpos($html, ' style=') === false) {
				$search[]  = '/(src=)/';
				$replace[] = "style=\"height: {$logo_height}px;\" src=";
			} else {
				$search[]  = '/(style="[^"]*)/';
				$replace[] = "$1 height: {$logo_height}px;";
			}

			$html = preg_replace($search, $replace, $html);
		}
	}

	return $html;
}

add_filter('get_custom_logo', 'nznmobilidade_get_custom_logo');

if (!function_exists('wp_body_open')) {

	/**
	 * Shim for wp_body_open, ensuring backward compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open()
	{
		do_action('wp_body_open');
	}
}

/**
 * Include a skip to content link at the top of the page so that users can bypass the menu.
 */
function nznmobilidade_skip_link()
{
	echo '<a class="skip-link screen-reader-text" href="#site-content">' . __('Skip to the content', 'nznmobilidade') . '</a>';
}

add_action('wp_body_open', 'nznmobilidade_skip_link', 5);

/**
 * Register widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function nznmobilidade_sidebar_registration()
{

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
	);

	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer #1', 'nznmobilidade'),
				'id'          => 'sidebar-1',
				'description' => __('Widgets in this area will be displayed in the first column in the footer.', 'nznmobilidade'),
				'before_widget' => '<div class="widget col-3 %2$s"><div class="widget-content">',
				'after_widget'  => '</div></div>',
			)
		)
	);

	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer #2', 'nznmobilidade'),
				'id'          => 'sidebar-2',
				'description' => __('Widgets in this area will be displayed in the second column in the footer.', 'nznmobilidade'),
				'before_widget' => '<div class="widget col-md-12 col-lg-6 %2$s"><div class="widget-content">',
				'after_widget'  => '</div></div>',
			)
		)
	);

	// Footer #3.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer #3', 'nznmobilidade'),
				'id'          => 'sidebar-3',
				'description' => __('Widgets in this area will be displayed in the second column in the footer.', 'nznmobilidade'),
				'before_widget' => '<div class="widget col-md-6 col-lg-4 %2$s"><div class="widget-content footer-widget">',
				'after_widget'  => '</div></div>',
			)
		)
	);

	// Footer #4.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer #4', 'nznmobilidade'),
				'id'          => 'sidebar-4',
				'description' => __('Widgets in this area will be displayed in the second column in the footer.', 'nznmobilidade'),
				'before_widget' => '<div class="widget col-12 %2$s"><div class="widget-content">',
				'after_widget'  => '</div></div>',
			)
		)
	);
}

add_action('widgets_init', 'nznmobilidade_sidebar_registration');

//
//  Adding more Widget Areas to the Sidebar
//
register_sidebar(array(
	'name' => __('Home', 'nznmobilidade_theme_support'),
	'id' => 'home_mobilidade',
	'description' => __('Home', 'nznmobilidade_theme_support'),
	'before_widget' => '<div class="widget_home col-lg-6"> <div class="widget_home_content">',
	'after_widget' => '</div></div>'
));

//
//  Adding more Widget Areas to the Sidebar
//
register_sidebar(array(
	'name' => __('Sobre o evento', 'nznmobilidade_theme_support'),
	'id' => 'sobre_o_evento_mobilidade',
	'description' => __('Sobre o evento', 'nznmobilidade_theme_support'),
	'before_widget' => '<div class="widget_home col-lg-6"> <div class="widget_home_content">',
	'after_widget' => '</div></div>'
));

//estimated reading time
function reading_time()
{
	global $post;
	$content = get_post_field('post_content', $post->ID);
	$word_count = str_word_count(strip_tags($content));
	$readingtime = ceil($word_count / 200);
	if ($readingtime == 1) {
		$timer = " min. de leitura";
	} else {
		$timer = " mins. de leitura";
	}
	$totalreadingtime = $readingtime . $timer;
	return $totalreadingtime;
}

/**
 * most popular posts
 */
/* Set post views count*/
function count_post_visits()
{
	if (is_single() && 'post' == get_post_type()) {
		global $post;
		$views = get_post_meta($post->ID, 'my_post_viewed', true);
		if ($views == '') {
			update_post_meta($post->ID, 'my_post_viewed', '1');
		} else {
			$views_no = intval($views);
			update_post_meta($post->ID, 'my_post_viewed', ++$views_no);
		}
	}
}
add_action('wp_head', 'count_post_visits');


// function to display number of posts.
function getPostViews($postID)
{
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if ($count == '') {
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	return $count . ' Views';
}

// function to count views.
function setPostViews($postID)
{
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if ($count == '') {
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	} else {
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}


// Add it to a column in WP-Admin
add_filter('manage_posts_columns', 'posts_column_views');
add_action('manage_posts_custom_column', 'posts_custom_column_views', 5, 2);
function posts_column_views($defaults)
{
	$defaults['post_views'] = __('Views');
	return $defaults;
}
function posts_custom_column_views($column_name, $id)
{
	if ($column_name === 'post_views') {
		echo getPostViews(get_the_ID());
	}
}
/**
 * Enqueue supplemental block editor styles.
 */
function nznmobilidade_block_editor_styles()
{

	$css_dependencies = array();

	// Enqueue the editor styles.
	wp_enqueue_style('nznmobilidade-block-editor-styles', get_theme_file_uri('/assets/css/editor-style-block.css'), $css_dependencies, wp_get_theme()->get('Version'), 'all');
	wp_style_add_data('nznmobilidade-block-editor-styles', 'rtl', 'replace');

	// Add inline style from the Customizer.
	wp_add_inline_style('nznmobilidade-block-editor-styles', nznmobilidade_get_customizer_css('block-editor'));

	// Add inline style for non-latin fonts.
	wp_add_inline_style('nznmobilidade-block-editor-styles', nznmobilidade_Non_Latin_Languages::get_non_latin_css('block-editor'));

	// Enqueue the editor script.
	wp_enqueue_script('nznmobilidade-block-editor-script', get_theme_file_uri('/assets/js/editor-script-block.js'), array('wp-blocks', 'wp-dom'), wp_get_theme()->get('Version'), true);
}

add_action('enqueue_block_editor_assets', 'nznmobilidade_block_editor_styles', 1, 1);

/**
 * Enqueue classic editor styles.
 */
function nznmobilidade_classic_editor_styles()
{

	$classic_editor_styles = array(
		'/assets/css/editor-style-classic.css',
	);

	add_editor_style($classic_editor_styles);
}

add_action('init', 'nznmobilidade_classic_editor_styles');

/**
 * Output Customizer settings in the classic editor.
 * Adds styles to the head of the TinyMCE iframe. Kudos to @Otto42 for the original solution.
 *
 * @param array $mce_init TinyMCE styles.
 *
 * @return array $mce_init TinyMCE styles.
 */
function nznmobilidade_add_classic_editor_customizer_styles($mce_init)
{

	$styles = nznmobilidade_get_customizer_css('classic-editor');

	if (!isset($mce_init['content_style'])) {
		$mce_init['content_style'] = $styles . ' ';
	} else {
		$mce_init['content_style'] .= ' ' . $styles . ' ';
	}

	return $mce_init;
}

add_filter('tiny_mce_before_init', 'nznmobilidade_add_classic_editor_customizer_styles');

/**
 * Output non-latin font styles in the classic editor.
 * Adds styles to the head of the TinyMCE iframe. Kudos to @Otto42 for the original solution.
 *
 * @param array $mce_init TinyMCE styles.
 *
 * @return array $mce_init TinyMCE styles.
 */
function nznmobilidade_add_classic_editor_non_latin_styles($mce_init)
{

	$styles = nznmobilidade_Non_Latin_Languages::get_non_latin_css('classic-editor');

	// Return if there are no styles to add.
	if (!$styles) {
		return $mce_init;
	}

	if (!isset($mce_init['content_style'])) {
		$mce_init['content_style'] = $styles . ' ';
	} else {
		$mce_init['content_style'] .= ' ' . $styles . ' ';
	}

	return $mce_init;
}

add_filter('tiny_mce_before_init', 'nznmobilidade_add_classic_editor_non_latin_styles');

/**
 * Block Editor Settings.
 * Add custom colors and font sizes to the block editor.
 */
function nznmobilidade_block_editor_settings()
{

	// Block Editor Palette.
	$editor_color_palette = array(
		array(
			'name'  => __('Accent Color', 'nznmobilidade'),
			'slug'  => 'accent',
			'color' => nznmobilidade_get_color_for_area('content', 'accent'),
		),
		array(
			'name'  => __('Primary', 'nznmobilidade'),
			'slug'  => 'primary',
			'color' => nznmobilidade_get_color_for_area('content', 'text'),
		),
		array(
			'name'  => __('Secondary', 'nznmobilidade'),
			'slug'  => 'secondary',
			'color' => nznmobilidade_get_color_for_area('content', 'secondary'),
		),
		array(
			'name'  => __('Subtle Background', 'nznmobilidade'),
			'slug'  => 'subtle-background',
			'color' => nznmobilidade_get_color_for_area('content', 'borders'),
		),
	);

	// Add the background option.
	$background_color = get_theme_mod('background_color');
	if (!$background_color) {
		$background_color_arr = get_theme_support('custom-background');
		$background_color     = $background_color_arr[0]['default-color'];
	}
	$editor_color_palette[] = array(
		'name'  => __('Background Color', 'nznmobilidade'),
		'slug'  => 'background',
		'color' => '#' . $background_color,
	);

	// If we have accent colors, add them to the block editor palette.
	if ($editor_color_palette) {
		add_theme_support('editor-color-palette', $editor_color_palette);
	}

	// Block Editor Font Sizes.
	add_theme_support(
		'editor-font-sizes',
		array(
			array(
				'name'      => _x('Small', 'Name of the small font size in the block editor', 'nznmobilidade'),
				'shortName' => _x('S', 'Short name of the small font size in the block editor.', 'nznmobilidade'),
				'size'      => 18,
				'slug'      => 'small',
			),
			array(
				'name'      => _x('Regular', 'Name of the regular font size in the block editor', 'nznmobilidade'),
				'shortName' => _x('M', 'Short name of the regular font size in the block editor.', 'nznmobilidade'),
				'size'      => 21,
				'slug'      => 'normal',
			),
			array(
				'name'      => _x('Large', 'Name of the large font size in the block editor', 'nznmobilidade'),
				'shortName' => _x('L', 'Short name of the large font size in the block editor.', 'nznmobilidade'),
				'size'      => 26.25,
				'slug'      => 'large',
			),
			array(
				'name'      => _x('Larger', 'Name of the larger font size in the block editor', 'nznmobilidade'),
				'shortName' => _x('XL', 'Short name of the larger font size in the block editor.', 'nznmobilidade'),
				'size'      => 32,
				'slug'      => 'larger',
			),
		)
	);

	// If we have a dark background color then add support for dark editor style.
	// We can determine if the background color is dark by checking if the text-color is white.
	if ('#ffffff' === strtolower(nznmobilidade_get_color_for_area('content', 'text'))) {
		add_theme_support('dark-editor-style');
	}
}

add_action('after_setup_theme', 'nznmobilidade_block_editor_settings');

/**
 * Overwrite default more tag with styling and screen reader markup.
 *
 * @param string $html The default output HTML for the more tag.
 *
 * @return string $html
 */
function nznmobilidade_read_more_tag($html)
{
	return preg_replace('/<a(.*)>(.*)<\/a>/iU', sprintf('<div class="read-more-button-wrap"><a$1><span class="faux-button">$2</span> <span class="screen-reader-text">"%1$s"</span></a></div>', get_the_title(get_the_ID())), $html);
}

add_filter('the_content_more_link', 'nznmobilidade_read_more_tag');

/**
 * Enqueues scripts for customizer controls & settings.
 *
 * @since NZN Mobilidade 1.0
 *
 * @return void
 */
function nznmobilidade_customize_controls_enqueue_scripts()
{
	$theme_version = wp_get_theme()->get('Version');

	// Add main customizer js file.
	wp_enqueue_script('nznmobilidade-customize', get_template_directory_uri() . '/assets/js/customize.js', array('jquery'), $theme_version, false);

	// Add script for color calculations.
	wp_enqueue_script('nznmobilidade-color-calculations', get_template_directory_uri() . '/assets/js/color-calculations.js', array('wp-color-picker'), $theme_version, false);

	// Add script for controls.
	wp_enqueue_script('nznmobilidade-customize-controls', get_template_directory_uri() . '/assets/js/customize-controls.js', array('nznmobilidade-color-calculations', 'customize-controls', 'underscore', 'jquery'), $theme_version, false);
	wp_localize_script('nznmobilidade-customize-controls', 'nznmobilidadeBgColors', nznmobilidade_get_customizer_color_vars());
}

add_action('customize_controls_enqueue_scripts', 'nznmobilidade_customize_controls_enqueue_scripts');


/**
 * Enqueues scripts add.
 *
 * @since NZN Mobilidade 1.0
 *
 * @return void
 */
function nznmobilidade_bootstrap_enqueue_scripts()
{
	global $wp_scripts;

	wp_register_script('html5_shiv', 'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', '', '', false);
	wp_register_script('respond_js', 'https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', '', '', false);
	$wp_scripts->add_data('html5_shiv', 'conditional', 'lt IE 9');
	$wp_scripts->add_data('respond_js', 'conditional', 'lt IE 9');	//bootstrap
	wp_register_script('nznmobilidade-bootstrap', get_template_directory_uri() . '/assets/bootstrap/js/bootstrap.min.js', array('jquery'), '1.0.0', false);
	wp_enqueue_script('nznmobilidade-bootstrap');

	wp_dequeue_style('wp-block-library');
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('wp_print_styles', 'print_emoji_styles');
}

add_action('wp_enqueue_scripts', 'nznmobilidade_bootstrap_enqueue_scripts');

/**
 * Enqueue scripts for the customizer preview.
 *
 * @since NZN Mobilidade 1.0
 *
 * @return void
 */
function nznmobilidade_customize_preview_init()
{
	$theme_version = wp_get_theme()->get('Version');

	wp_enqueue_script('nznmobilidade-customize-preview', get_theme_file_uri('/assets/js/customize-preview.js'), array('customize-preview', 'customize-selective-refresh', 'jquery'), $theme_version, true);
	wp_localize_script('nznmobilidade-customize-preview', 'nznmobilidadeBgColors', nznmobilidade_get_customizer_color_vars());
	wp_localize_script('nznmobilidade-customize-preview', 'nznmobilidadePreviewEls', nznmobilidade_get_elements_array());

	wp_add_inline_script(
		'nznmobilidade-customize-preview',
		sprintf(
			'wp.customize.selectiveRefresh.partialConstructor[ %1$s ].prototype.attrs = %2$s;',
			wp_json_encode('cover_opacity'),
			wp_json_encode(nznmobilidade_customize_opacity_range())
		)
	);
}

add_action('customize_preview_init', 'nznmobilidade_customize_preview_init');

/**
 * Get accessible color for an area.
 *
 * @since NZN Mobilidade 1.0
 *
 * @param string $area The area we want to get the colors for.
 * @param string $context Can be 'text' or 'accent'.
 * @return string Returns a HEX color.
 */
function nznmobilidade_get_color_for_area($area = 'content', $context = 'text')
{

	// Get the value from the theme-mod.
	$settings = get_theme_mod(
		'accent_accessible_colors',
		array(
			'content'       => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
			'header' => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
			'footer' => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
			'aside' => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
		)
	);

	// If we have a value return it.
	if (isset($settings[$area]) && isset($settings[$area][$context])) {
		return $settings[$area][$context];
	}

	// Return false if the option doesn't exist.
	return false;
}

/**
 * Returns an array of variables for the customizer preview.
 *
 * @since NZN Mobilidade 1.0
 *
 * @return array
 */
function nznmobilidade_get_customizer_color_vars()
{
	$colors = array(
		'content'       => array(
			'setting' => 'background_color',
		),
		'header' => array(
			'setting' => 'header_background_color',
		),
		'footer' => array(
			'setting' => 'footer_background_color',
		),
		'aside' => array(
			'setting' => 'footerlast_background_color',
		),
	);
	return $colors;
}

/* taxonomia para especiais */
function wporg_register_taxonomy_especiais()
{
	$labels = [
		'name'              => _x('Especiais categoria', 'especiaiscategoria'),
		'singular_name'     => _x('Especial categoria', 'especialcategoria'),
		'search_items'      => __('Buscar Especiais'),
		'all_items'         => __('Todas'),
		'parent_item'       => __('Ascendente Especial'),
		'parent_item_colon' => __('Ascendente Especial:'),
		'edit_item'         => __('Editar'),
		'update_item'       => __('Atualizar'),
		'add_new_item'      => __('Adicionar nova'),
		'new_item_name'     => __('Nome'),
		'menu_name'         => __('Especiais categoria'),
	];
	$args = [
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_in_rest'        	   => true,
		'show_tagcloud'              => true,
	];
	register_taxonomy('especiaistax', array('especiaistype'), $args);
}
add_action('init', 'wporg_register_taxonomy_especiais');


/*
* Post type para especiais
*/

function custom_post_type()
{

	$labels = array(
		'name'                => _x('Especiais', 'Post Type General Name', 'nznmobilidade'),
		'singular_name'       => _x('Especial', 'Post Type Singular Name', 'nznmobilidade'),
		'menu_name'           => __('Especiais', 'nznmobilidade'),
		'parent_item_colon'   => __('Ascendente Especial', 'nznmobilidade'),
		'all_items'           => __('Todas', 'nznmobilidade'),
		'view_item'           => __('Ver Especial', 'nznmobilidade'),
		'add_new_item'        => __('Adicionar nova especial', 'nznmobilidade'),
		'add_new'             => __('Adicionar nova', 'nznmobilidade'),
		'edit_item'           => __('Editar', 'nznmobilidade'),
		'update_item'         => __('Atualizar', 'nznmobilidade'),
		'search_items'        => __('Buscar', 'nznmobilidade'),
		'not_found'           => __('Não encontrada', 'nznmobilidade'),
		'not_found_in_trash'  => __('Não encontrada na lixeira', 'nznmobilidade'),
	);


	$args = array(
		'label'               => __('especiais', 'nznmobilidade'),
		'description'         => __('Para patrocinadores', 'nznmobilidade'),
		'labels'              => $labels,
		'supports'            => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
		'taxonomies'          => array('especiaistax', 'post_tag'),

		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest' => true,
	);

	register_post_type('especiaistype', $args);
}

/* Hook into the 'init' action so that the function
	* Containing our post type registration is not 
	* unnecessarily executed. 
	*/

add_action('init', 'custom_post_type', 0);


/**
 * Get an array of elements.
 *
 * @since NZN Mobilidade 1.0
 *
 * @return array
 */
function nznmobilidade_get_elements_array()
{

	// The array is formatted like this:
	// [key-in-saved-setting][sub-key-in-setting][css-property] = [elements].
	$elements = array(
		'content'       => array(
			'accent'     => array(
				'color'            => array('.color-accent', '.color-accent-hover:hover', '.color-accent-hover:focus', ':root .has-accent-color', '.has-drop-cap:not(:focus):first-letter', '.wp-block-button.is-style-outline', 'a'),
				'border-color'     => array('blockquote', '.border-color-accent', '.border-color-accent-hover:hover', '.border-color-accent-hover:focus'),
				'background-color' => array('button:not(.toggle)', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file .wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.bg-accent', '.bg-accent-hover:hover', '.bg-accent-hover:focus', ':root .has-accent-background-color', '.comment-reply-link'),
				'fill'             => array('.fill-children-accent', '.fill-children-accent *'),
			),
			'background' => array(
				'color'            => array(':root .has-background-color', 'button', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.wp-block-button', '.comment-reply-link', '.has-background.has-primary-background-color:not(.has-text-color)', '.has-background.has-primary-background-color *:not(.has-text-color)', '.has-background.has-accent-background-color:not(.has-text-color)', '.has-background.has-accent-background-color *:not(.has-text-color)'),
				'background-color' => array(':root .has-background-background-color'),
			),
			'text'       => array(
				'color'            => array('body', '.entry-title a', ':root .has-primary-color'),
				'background-color' => array(':root .has-primary-background-color'),
			),
			'secondary'  => array(
				'color'            => array('cite', 'figcaption', '.wp-caption-text', '.post-meta', '.entry-content .wp-block-archives li', '.entry-content .wp-block-categories li', '.entry-content .wp-block-latest-posts li', '.wp-block-latest-comments__comment-date', '.wp-block-latest-posts__post-date', '.wp-block-embed figcaption', '.wp-block-image figcaption', '.wp-block-pullquote cite', '.comment-metadata', '.comment-respond .comment-notes', '.comment-respond .logged-in-as', '.pagination .dots', '.entry-content hr:not(.has-background)', 'hr.styled-separator', ':root .has-secondary-color'),
				'background-color' => array(':root .has-secondary-background-color'),
			),
			'borders'    => array(
				'border-color'        => array('pre', 'fieldset', 'input', 'textarea', 'table', 'table *', 'hr'),
				'background-color'    => array('caption', 'code', 'code', 'kbd', 'samp', '.wp-block-table.is-style-stripes tbody tr:nth-child(odd)', ':root .has-subtle-background-background-color'),
				'border-bottom-color' => array('.wp-block-table.is-style-stripes'),
				'border-top-color'    => array('.wp-block-latest-posts.is-grid li'),
				'color'               => array(':root .has-subtle-background-color'),
			),
		),
		'header' => array(
			'accent'     => array(
				'color'            => array('body:not(.overlay-header) .primary-menu > li > a', 'body:not(.overlay-header) .primary-menu > li > .icon', '.modal-menu a', '.wp-block-pullquote:before', '.singular:not(.overlay-header) .entry-header a', '.archive-header a', '.header-group .color-accent', '.header-group .color-accent-hover:hover'),
				'background-color' => array('.social-icons a'),
			),
			'background' => array(
				'color'            => array('.social-icons a', 'body:not(.overlay-header) .primary-menu ul', '.header-group button', '.header-group .button', '.header-group .faux-button', '.header-group .wp-block-button:not(.is-style-outline) .wp-block-button__link', '.header-group .wp-block-file__button', '.header-group input[type="button"]', '.header-group input[type="reset"]', '.header-group input[type="submit"]'),
				'background-color' => array('#site-header', '.menu-modal', '.menu-modal-inner', '.search-modal-inner', '.archive-header', '.singular .entry-header', '.singular .featured-media:before', '.wp-block-pullquote:before'),
			),
			'text'       => array(
				'color'               => array('.header-group', 'body:not(.overlay-header) #site-header .toggle', '.menu-modal .toggle'),
				'background-color'    => array('body:not(.overlay-header) .primary-menu ul'),
				'border-bottom-color' => array('body:not(.overlay-header) .primary-menu > li > ul:after'),
				'border-left-color'   => array('body:not(.overlay-header) .primary-menu ul ul:after'),
			),
			'secondary'  => array(
				'color' => array('.site-description', 'body:not(.overlay-header) .toggle-inner .toggle-text', '.widget .post-date', '.widget .rss-date', '.widget_archive li', '.widget_categories li', '.widget cite', '.widget_pages li', '.widget_meta li', '.widget_nav_menu li', '.powered-by-wordpress', '.to-the-top', '.singular .entry-header .post-meta', '.singular:not(.overlay-header) .entry-header .post-meta a'),
			),
			'borders'    => array(
				'border-color'     => array('.header-group pre', '.header-group fieldset', '.header-group input', '.header-group textarea', '.header-group table', '.header-group table *', '.menu-modal nav *'),
				'background-color' => array('.header-group table caption', 'body:not(.overlay-header) .header-inner .toggle-wrapper::before'),
			),
		),
		'footer' => array(
			'accent'     => array(
				'color'            => array('.footer-menu a, .footer-widgets a', '#site-footer .wp-block-button.is-style-outline', '.wp-block-pullquote:before', '.archive-header a', '.footer-group .color-accent', '.footer-group .color-accent-hover:hover'),
				'background-color' => array('#site-footer button:not(.toggle)', '#site-footer .button', '#site-footer .faux-button', '#site-footer .wp-block-button__link', '#site-footer .wp-block-file__button', '#site-footer input[type="button"]', '#site-footer input[type="reset"]', '#site-footer input[type="submit"]'),
			),
			'background' => array(
				'color'            => array('.social-icons a', '.footer-group button', '.footer-group .button', '.footer-group .faux-button', '.footer-group .wp-block-button:not(.is-style-outline) .wp-block-button__link', '.footer-group .wp-block-file__button', '.footer-group input[type="button"]', '.footer-group input[type="reset"]', '.footer-group input[type="submit"]'),
				'background-color' => array('.footer-nav-widgets-wrapper', '#site-footer', '.wp-block-pullquote:before'),
			),
			'text'       => array(
				'color'               => array('.footer-group', '.menu-modal .toggle'),
				'background-color'    => array('body:not(.overlay-header) .primary-menu ul'),
				'border-bottom-color' => array('body:not(.overlay-header) .primary-menu > li > ul:after'),
				'border-left-color'   => array('body:not(.overlay-header) .primary-menu ul ul:after'),
			),
			'secondary'  => array(
				'color' => array('.site-description', 'body:not(.overlay-header) .toggle-inner .toggle-text', '.widget .post-date', '.widget .rss-date', '.widget_archive li', '.widget_categories li', '.widget cite', '.widget_pages li', '.widget_meta li', '.widget_nav_menu li', '.powered-by-wordpress', '.to-the-top'),
			),
			'borders'    => array(
				'border-color'     => array('.footer-group pre', '.footer-group fieldset', '.footer-group input', '.footer-group textarea', '.footer-group table', '.footer-group table *', '.footer-nav-widgets-wrapper', '.menu-modal nav *', '.footer-widgets-outer-wrapper', '.footer-top'),
				'background-color' => array('.footer-group table caption', 'body:not(.overlay-header) .header-inner .toggle-wrapper::before'),
			),
		),
		'aside' => array(
			'background' => array(
				'color'            => array('.footer-last .wp-block-file__button', '.footer-last input[type="button"]', '.footer-last input[type="reset"]', '.footer-last input[type="submit"]'),
				'background-color' => array('#site-footer-last', '.wp-block-pullquote:before'),
			),
			'borders'    => array(
				'border-color'     => array('.footer-last pre', '.footer-last fieldset', '.footer-last input', '.footer-last textarea', '.footer-last table', '.footer-last table *', '.menu-modal nav *'),
				'background-color' => array('.footer-last table caption', 'body:not(.overlay-header) .header-inner .toggle-wrapper::before'),
			),
		),
	);

	/**
	 * Filters NZN Mobilidade theme elements
	 *
	 * @since NZN Mobilidade 1.0
	 *
	 * @param array Array of elements
	 */
	return apply_filters('nznmobilidade_get_elements_array', $elements);
}



function especiaistax_query($args, $field, $post_id)
{

	foreach (get_post_types(array('show_in_rest' => true), 'objects') as $post_type) {
		add_filter('rest_' . $post_type->name . '_query', 'especiaistax_query', 10, 2);
	}
}

function custom_header_setup()
{
	$defaults = array(
		// Default Header Image to display
		'default-image'         => get_template_directory_uri() . '/images/headers/default.jpg',
		// Display the header text along with the image
		'header-text'           => false,
		// Header text color default
		'default-text-color'        => '000',
		// Header image width (in pixels)
		'width'             => 1000,
		// Header image height (in pixels)
		'height'            => 198,
		// Header image random rotation default
		'random-default'        => false,
		// Enable upload of image file in admin 
		'uploads'       => false,
		// function to be called in theme head section
		'wp-head-callback'      => 'wphead_cb',
		//  function to be called in preview page head section
		'admin-head-callback'       => 'adminhead_cb',
		// function to produce preview markup in the admin screen
		'admin-preview-callback'    => 'adminpreview_cb',
	);
}
add_action('after_setup_theme', 'custom_header_setup');


function wpbanner_after($content)
{
	if (is_single()) {
		if ('post' !== get_post_type()) {
			return $content;
		}
		if ((has_term('', 'especiaistax'))) {
			$taxonomy = get_the_terms($post->ID, 'especiaistax');
			foreach ($taxonomy as $tax) {

				if (have_rows('arroba_banner', $tax)) {

					while (have_rows('arroba_banner', $tax)) : the_row();
						$banner = get_sub_field('banner', $tax);
						$link = get_sub_field('link', $tax);

						if ($link) {
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
							$content .=
								'	
				<div class="arroba_banner">
						<a href="' . esc_url($link_url) . '" target="' . esc_attr($link_target) . '">
							<img src="' .  esc_url($banner) . '" alt="' .  esc_html($link_title) . '" />
						</a>
						</div>
				';
						};
					endwhile;
				}
			}
		} else {
			if (have_rows('arroba_banner_gerais', 'option')) {

				while (have_rows('arroba_banner_gerais', 'option')) : the_row();
					$banner = get_sub_field('banner');
					$link = get_sub_field('link');

					if ($link) {
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
						$content .=
							'	
			<div class="arroba_banner">
					<a href="' . esc_url($link_url) . '" target="' . esc_attr($link_target) . '">
						<img src="' .  esc_url($banner) . '" alt="' .  esc_html($link_title) . '" />
					</a>
					</div>
			';
					};
				endwhile;
			}
		}
	}
	return $content;
}
add_filter('the_content', 'wpbanner_after');
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');

// set cannonical
add_filter('wpseo_canonical', function ($canonical) {

	if ('//summitmobilidade.estadao.com.br/' ==  get_option('home')) {
		return $canonical;
	}

	$canonical = preg_replace('#//[^/]*/#U', '//summitmobilidade.estadao.com.br/', trailingslashit($canonical));
	return $canonical;
});

/**
 * Adds webp filetype to allowed mimes
 * 
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * 
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
add_filter('upload_mimes', 'wpse_mime_types_webp');
function wpse_mime_types_webp($mimes)
{
	$mimes['webp'] = 'image/webp';

	return $mimes;
}
