<?php
/**
 * The template for displaying the 404 template in the NZN Mobilidade theme.
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

get_header();
?>

<main id="site-content" role="main">

	<div class="section-inner thin error404-content">

		<h1 class="entry-title"><?php _e( 'Página não encontrada', 'nznmobilidade' ); ?></h1>

		<div class="intro-text"><p><?php _e( 'A página que você estava procurando não foi encontrada. Pode ter sido removido, renomeado ou não existir.', 'nznmobilidade' ); ?></p></div>

		<?php
		get_search_form(
			array(
				'label' => __( 'Não encontrado.', 'nznmobilidade' ),
			)
		);
		?>

	</div><!-- .section-inner -->

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php
get_footer();
