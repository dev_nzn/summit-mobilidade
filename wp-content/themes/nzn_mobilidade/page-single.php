<?php
/**
 * Template Name: Template especiais single
 * Template Post Type: post, page
 *  
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

get_header();
$cover_thumb = wp_get_attachment_url(get_post_thumbnail_id($post->ID) ); 
?>
<main id="site-content" role="main" class="single_post_content">
    <div class="container banner-top">
        <div class="row d-flex justify-content-center">
            <div class="col-12">
                <?php
                    get_template_part( 'template-parts/home-super_banner' );
                ?>
            </div>
        </div>
    </div>
    <article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <?php 
                if (empty($cover_thumb)) { 
                    ?>

                <?php
                } else {
                    ?>
                    <div class="col-lg-10 col-md-11 col-12">
                    <div class="bg-post-single">
                        <div class="bg-post" style="background: url(<?php echo $cover_thumb; ?>);">
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
                
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-8 col-md-9 col-11">
                        <div class="content-blog">
                            <div class="header-blog">
                                <?php
                                    if ( function_exists('yoast_breadcrumb') ) {
                                        yoast_breadcrumb( '<div id="breadcrumbs" class="links-breadcrumb">','</div>' );
                                    }
                                    the_title( '<h1>', '</h1>' );
                                ?>
                                <div class="date-time-card">
                                    <span class="data-time">
                                    <?php the_time('j \d\e F \d\e Y') ?>
                                    </span>
                                    <i class="data-time">&bull;</i>
                                    <span class="data-time">5 min de leitura</span>
                                </div>
                                <strong class="excerpt_post"><?php echo strip_tags(the_excerpt()); ?></strong>
                            </div>
                            <div class="body-blog">
                                    <?php the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>