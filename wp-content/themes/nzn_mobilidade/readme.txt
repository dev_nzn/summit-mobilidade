# Tema Wordpress desenvolvido exclusivamente para o Summit Mobilidade - Estadão

Nome do tema: NZN Mobilidade

---

# Estrutura de arquivos e diretórios

- nzn_mobilidade
  - assets (dir)
  - classes (dir)
  - inc (dir)
  - template-parts (dir)
  - templates (dir)
  - 404.php
  - comments.php
  - category.php
  - footer.php
  - functions.php
  - header.php
  - index.php  
  - page.php
  - print.css
  - screenshot.png
  - searchform.php
  - single.php
  - singular.php
  - style-rtl.css
  - style.css
  - taxonomy-especiaistax.php
  ***

# Files e responsabilidades : themes/nzn_mobilidade

- **assets** : Diretório para armazenar arquivos JS, CSS e IMAGENS
- **classes** : Diretório original WP não mexes
- **inc** : Diretório original WP não mexes
- **template-parts** : Diretório para armazenar os [Template part`s](https://codex.wordpress.org/pt-br:get_template_part)
- **templates** : Diretório para armazenar os [Modelos de páginas](https://codex.wordpress.org/Templates).
- **category.php**: Arquivo da página de categoria.
- **footer.php** : Footer da página
- **functions.php** : Inicialização de funções do tema.
- **header.php** : Header da página.
- **index.php** : Página que exibe os resultados das pesquisas do searchform.php e lista de "notícias"
- **page.php** : Página padrão usada para "Especiais", "Patrocinadores" como template "Page template"
- **page-99.php** : Página "Agência 99" - localizada no menu
- **print.css** : CSS Padrão WP - não mexer
- **screenshot.png** : Thumb do tema
- **searchform.php** : Arquivo para realizar as pesquisas dos usuários.
- **single.php**: Página interna dos posts
- **singular.php**: Página padrão do WP - não está sendo usada
- **style-rtl.css**:  CSS Padrão WP - não mexer
- **style.css**: Arquivo inicial de estilo do tema (Obs: Nós não colocamos os estilos do tema aqui, as folhas de estilo e arquivos javascritps são colocados dentro do diretório **assets**)
- **taxonomy-especiaistax.php**: Página de taxonomia desenvolvida para os Especiais

# Principais componentes : themes/nzn_mobilidade/template-parts

- **carousel-fixpost.php**: Carousel de noticias fixas que aparece na home
- **edicao-anterior.php**: Componente localizado na página sobre -  apresenta galeria dos eventos anteriores - não está mais sendo usado
- **embed-video.php**: Componente de video localizado na página sobre -  apresenta video dos eventos anteriores - não está mais sendo usado 
- **feed-agencia99.php**: Componente de lista noticias de feed externo https://patrocinados.estadao.com.br/medialab/rss2SummitMobilidade.php?tkn=AtpvqPBGkl5Hkk99Glkp0sdBBzlkndsr - componente utilizado em page-99.php
- **home-arroba_banner.php**: Componente de arroba(tipo de banner) banner - reutilizado em mais de um lugar
- **home-super_banner.php**: Componente de super(tipo de banner) banner - reutilizado em mais de um lugar
- **last-news_posttype.php**: Componente de últimas noticias relacionadas somente ao "canal agro" 
- **last-news.php**: Componente de 'ltimas noticias
- **list_conteudos-mobilidade.php**: Componente lista conteúdo de feed externo "https://mobilidade.estadao.com.br/feed" - não está mais sendo usado por enquanto
- **list_conteudos-especiais.php**: Componente lista de conteúdos especiais patrocinados - quando tem a lista aparece na home
- **most-views.php**: Componente de posts mais vistos - localizado na sidebar da home
- **news-component.php**: Componente de newsletter - configurada em widgets footer#1
- **palestrantes.php**: Componente de lista palestrantes - usado na página sobre - por enquanto inutilizado
- **patrocinadores.php**: Componente de lista de patrocinadores - usado na página sobre - por enquanto inutilizado
- **programacao.php**: Componente de lista de programacao - usado na página sobre - por enquanto inutilizado
- **redes-sociais.php**: Componente de redes sociais
- **related-posts.php**: Componente de posts relacionados gerais

# Principais Templates : themes/nzn_mobilidade/template
- **template-home.php**: Template da home
- **template-sobre.php**: Template "sobre o evento" - localizado no menu 
- **template-mapa-metro.php**: Template "sobre o evento" - localizado no menu em "Notícias"
- **template-premio-evento.php**: Template "Prêmio mobilidade" - localizado no menu 

# Como usar?

#### Clonar o projeto em /wp-content/themes/

```
git clone https://dev_nzn@bitbucket.org/dev_nzn/summit-mobilidade.git
```
Todo e qualquer css adicional deve ser colocado em:

/assets/sass/theme_style.css

Dentro do diretório themes/nzn_mobilidade/BD e arquivo all-in-one
Há o arquivo de banco de dados que pode ser feito o upload, como também o arquivo .wpress, gerado pelo plugin "All-in-one Migration"

Como usar o All in one migration
Instalar o plugin no wordpress e dar import no arquivo presente nessa pasta com a extensão .wpress


# Como usar os compos personalizados? Segue o link no drive com os tutoriais

Àreas comuns dos summits - https://docs.google.com/presentation/d/1QIqDSJta7im1wY0Wjq56ESYDZeaj48RDeB9vEXyh5Gc/edit?usp=sharing
Patrocinadores - https://docs.google.com/presentation/d/1-8N9epzUi96MhIpZe3zDBr4oj3vY4V22DPFhV-MR7Pg/edit?usp=sharing
Palestrantes - https://docs.google.com/presentation/d/1Rd9VF4gkVhpzyQOj2-qhooMp-M9o2KJ8dMk4WiI8HcI/edit?usp=sharing
Especiais - https://docs.google.com/presentation/d/1Rd9VF4gkVhpzyQOj2-qhooMp-M9o2KJ8dMk4WiI8HcI/edit?usp=sharing