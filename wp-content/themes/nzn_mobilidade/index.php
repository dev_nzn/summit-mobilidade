<?php
/**
 * The main template file 
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

get_header();
$blog_page_image = wp_get_attachment_url( get_post_thumbnail_id(get_option( 'page_for_posts' )) );
?>


    <main id="site-content" class="custom-template-page archive_post" role="main">
        <?php
 	if ( ! is_page_template() ) {
		$archive_title    = "";
		$archive_subtitle = "";
	}
	if ( is_search() ) {
		global $wp_query;

		$archive_title = sprintf(
			'%1$s %2$s',
			'<span class="color-accent">' . __( 'Pesquisa:', 'nznmobilidade' ) . '</span>',
			'&ldquo;' . get_search_query() . '&rdquo;'
		);

		if ( $wp_query->found_posts ) {
			$archive_subtitle = sprintf(
				/* translators: %s: Number of search results. */
				_n(
					'Foi encontrado %s resultado para sua pesquisa.',
					'Foram encontrados %s resultados para a sua pesquisa.',
					$wp_query->found_posts,
					'nznmobilidade'
				),
				number_format_i18n( $wp_query->found_posts )
			);
		} else {
			$archive_subtitle = __( 'Nenhum resultado encontrado.', 'nznmobilidade' );
		}
	} elseif (  is_home() ) {
		$archive_title    = get_the_archive_title();
		$archive_subtitle = get_the_archive_description();
	}

	// if ( $archive_title || $archive_subtitle ) {
	?>

            <header class="entry-header archive-header has-text-align-left header-group">
                <div class="entry-header-inner section-inner ">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb( '<div id="breadcrumbs" class="links-breadcrumb">','</div>' );
						}
					?>
					<?php
						if(is_search()){
							?>
<h1 class="entry-title nome-categoria-paginas-internas">
					<?php echo wp_kses_post( $archive_title ); ?>
					</h1>
							<?php
						}
					?>
                                    <?php if ( $archive_title && ! is_search()) { ?>
                                    	<h1 class="entry-title nome-categoria-paginas-internas">
                                        <?php echo wp_kses_post( 'Notícias' ); ?>
										</h1>
									<?php } elseif(has_term('', 'especiaistax')) {?>
										<h1 class="entry-title nome-categoria-paginas-internas">
											<?php single_term_title(); ?>
										</h1>
									<?php } elseif(is_search()) { 
										
										wp_kses_post( $archive_title );

									} elseif(has_term())  { ?>
                                    <h1 class="entry-title nome-categoria-paginas-internas">
                                       Especiais
                                    </h1>
									<?php } else { ?>
										<h1 class="entry-title nome-categoria-paginas-internas">
                                        <?php $category = get_the_category();
											  echo $category[0]->cat_name; ?>
                                    </h1>
									<?php } ?>
                            </div>
                        </div>
                    </div>
					<?php if(!is_search()) {  ?>
                    <img src="<?php echo $blog_page_image; ?>" class="feature-image-title wp-post-image" alt="noticias" height="125.042" width="803.327" />
                    <?php } ?>
					<?php 				
			if ( $archive_subtitle ) { ?>
                    <div class="archive-subtitle section-inner thin max-percentage intro-text">
                        <?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?>
                    </div>
                    <?php } ?>
                </div>
            </header>
			<!-- .archive-header -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="descricao-page-posts">
                            <?php if ( $archive_title ) {
                            echo apply_filters( 'the_content', get_post_field( 'post_content', get_option( 'page_for_posts' ) ) ); 
                             } elseif(is_archive()) { 
								 echo category_description(''); 
							} else{ 
								echo term_description('');
							} ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container post-list">
                <div class="row">
                    <?php
			// }
			if ( have_posts() ) {
				$i = 0;
				while ( have_posts() ) {
					$i++;
				
					the_post();
					if($i == 6){
						get_template_part( 'template-parts/home-arroba_banner' );
					}
			?>
                        <?php
				get_template_part( 'template-parts/content-archive', get_post_type() );
			?>
                            <?php
				}
			} elseif ( is_search() ) {
			?>
                                <div class="no-search-results-form section-inner thin">
                                    <?php
				get_search_form(
					array(
						'label' => __( 'search again', 'nznmobilidade' ),
					)
				);
			?>
                                </div>
                                <!-- .no-search-results -->
                                <?php
			}
			?>
                                    <?php get_template_part( 'template-parts/pagination' ); ?>
                </div>
            </div>
    </main>
    <!-- #site-content -->

    <?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

    <?php
get_footer();

