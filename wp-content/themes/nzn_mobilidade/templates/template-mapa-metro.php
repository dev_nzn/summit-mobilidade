<?php

/**
 * Template Name: Metro Sao Paulo
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage nzn_agro
 * @since NZN agro 1.0
 */

get_header();
?>

<main class="container">

  <div class="row">
    <div class="col-md-4 custom_column-xl-2  col-12">
      <div class="subway__sidebar">
        <h1>Navegue por tópicos</h1>
        <ul>
          <div class="subway__sidebar--item">
            <i class="fas fa-caret-right"></i>
            <li><a class="anchor" data-section="section-1" href="javascript:void(0)">Quais são as linhas de metrô de São Paulo?</a></li>
          </div>
          <li>
            <ul>
              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-1" href="javascript:void(0)">Linha 1: Azul Metrô de São Paulo</a></li>
              </div>

              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-2" href="javascript:void(0)">Linha 2: Verde Metrô de São Paulo</a></li>
              </div>

              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-3" href="javascript:void(0)">Linha 3: Vermelha Metrô de São Paulo</a></li>
              </div>

              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-4" href="javascript:void(0)">Linha 4: Amarela Metrô de São Paulo</a></li>
              </div>

              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-5" href="javascript:void(0)">Linha 5: Lilás Metrô de São Paulo</a></li>
              </div>

              <div class="subway__sidebar--item">
                <i class="fas fa-caret-right subway__sidebar--hide"></i>
                <li><a class="subway__sidebar--subitem anchor" data-section="line-6" href="javascript:void(0)">Linha 15: Prata Metrô de São Paulo</a></li>
              </div>
            </ul>
          </li>
          <div class="subway__sidebar--item">
            <i class="fas fa-caret-right subway__sidebar--hide"></i>
            <li><a class="anchor" data-section="section-2" href="javascript:void(0)">Qual é o horário de funcionamento do metrô de São Paulo?</a></li>
          </div>

          <div class="subway__sidebar--item">
            <i class="fas fa-caret-right subway__sidebar--hide"></i>
            <li><a class="anchor" data-section="section-3" href="javascript:void(0)">Como funciona o metrô de São Paulo?</a></li>
          </div>

          <div class="subway__sidebar--item">
            <i class="fas fa-caret-right subway__sidebar--hide"></i>
            <li><a class="anchor" data-section="section-4" href="javascript:void(0)">Mapa do metrô de São Paulo.</a></li>
          </div>
        </ul>
      </div>
    </div>
    <div class="col-md-8 col-12">
      <!-- Introdução -->
      <section class="subway__introduction subway__section">
        <h1>Metrô de São Paulo</h1>
        <p>
          Com mais de 100 quilômetros de extensão, o <b>Metrô de São Paulo</b> é um dos
          <a href="https://summitmobilidade.estadao.com.br/guia-do-transporte-urbano/os-meios-de-transporte-mais-utilizados-em-sao-paulo/">meios de transporte</a>
          mais utilizados na cidade e transporta 4 milhões de pessoas todos os dias. Saiba como funciona essa estrutura e quais são as seis linhas em operação
        </p>
      </section>

      <!-- Linhas de metrô em SP -->
      <section id="section-1" class="subway__lines subway__section">
        <h2>Quais são as linhas de metrô em São Paulo?</h2>

        <!-- Linha Azul -->
        <div id="line-1" class="subway__line">
          <h3>Linha 1: Azul Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/Linha-Azul-metro-sp.webp" alt="Linha Azul">
          <p>
            Os seus extremos são as estações Jabaquara e Tucuruvi. A <b>Azul do Metrô de São Paulo</b> passa por importantes pontos turísticos da cidade,
            como a Pinacoteca do Estado (próximo à Estação Luz), o bairro da Liberdade, o Parque do Ibirapuera e o Museu de Arte Moderna de São Paulo
            (perto da Estação Maria Rosa).
          </p>
        </div>

        <!-- Linha verde -->
        <div id="line-2" class="subway__line">
          <h3>Linha 2: Verde Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/linha-verde-metro-sp.png" alt="Linha Verde">
          <p>
            Ligando as estações Vila Madalena e Vila Prudente, a Linha 2 passa por lugares famosos, como a Avenida Paulista e a Rua Augusta (próximas à Estação Consolação),
            o Museu do Futebol (nas imediações da Estação Clínicas) e o Beco do Batman (perto da Estação Vila Madalena)
          </p>
        </div>

        <!-- Linha vermelha -->
        <div id="line-3" class="subway__line">
          <h3>Linha 3: Vermelha Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/linha_3_-_vermelha.png" alt="Linha Vermelha">
          <p>
            Com 22 quilômetros e 16 estações, a linha vermelha é a mais movimentada de São Paulo e liga as Estações Barra Funda e Itaquera.
            A <b>Linha Vermelha</b> pode ser usada para acessar o Teatro Municipal (pela Estação Anhangabaú), a Praça da República, a Galeria do Rock (pela Estação República)
            e a Praça da Sé (pela Estação Sé)
          </p>
        </div>

        <!-- Linha Amarela -->
        <div id="line-4" class="subway__line">
          <h3>Linha 4: Amarela Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/linha-amarela-metro-sp.jpg" alt="Linha Amarela">
          <p>
            A <b>Linha Amarela</b> tem trens automatizados que ligam o Morumbi à Estação Luz. Tem 10 estações e opera em regime de parceria público-privada com a Via Quatro
            desde 2010. Por meio dela, é possível acessar o bairro Vila Madalena (pela Estação Fradique Coutinho), o Memorial da Resistência (próximo à Estação Paulista)
            e o Largo da Batata (pela Estação Faria Lima)
          </p>
        </div>

        <!-- Linha Lilás -->
        <div id="line-5" class="subway__line">
          <h3>Linha 5: Lilás Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/linha-lilas-metro-sp.jpg" alt="Linha Lilás">
          <p>
            A <b>Linha Lilás</b> do
            <a href="https://summitmobilidade.estadao.com.br/guia-do-transporte-urbano/quais-sao-as-diferencas-entre-trem-vlt-e-metro/" target="_blank">Metrô de São Paulo</a>
            liga o Capão Redondo à Chácara Klabin, tem 17 estações e também opera em regime de concessão. Passa pelas conhecidas regiões de Santo Amaro e Moema, apresentando
            20 quilômetros de extensão.
          </p>
        </div>

        <!-- Linha Prata -->
        <div id="line-6" class="subway__line">
          <h3>Linha 15: Prata Metrô de São Paulo</h3>
          <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/mapa-linha-15-prata.jpg" alt="Linha Prata">
          <p>
            Totalmente suspensa e com trens monotrilhos, a Linha Prata liga as Estações Vila Prudente e Hospital Cidade Tiradentes, contendo 17 estações.
            É a linha de metrô mais nova de São Paulo.
          </p>
        </div>
      </section>

      <section id="section-2" class="subway__section subway__introduction">
        <h2>Qual é o horário de funcionamento das estações de metrô de São Paulo?</h2>
        <p>
          As estações de metrô de São Paulo funcionam das 4h40 à meia-noite. Desse modo, é possível se atualizar dos horários em tempo real baixando o
          <a href="https://play.google.com/store/apps/details?id=br.com.metrosp.appmetro&hl=pt_BR" target="_blank">aplicativo</a>
          disponibilizado pela Companhia do Metropolitano de São Paulo.
        </p>
      </section>

      <section id="section-3" class="subway__section subway__introduction">
        <h2>Como funciona o metrô de São Paulo?</h2>
        <p>O metrô de São Paulo pode ser pago com o ticket unitário, comprado em cabines dentro das estações,
          ou com o Bilhete Único, um cartão recarregável aceito também em trens e ônibus. Atualmente (28/04/2021) o valor da tarifa é de R$ 4,83
        </p>
      </section>

      <section id="section-4" class="subway__section">
        <h2>Confira o mapa do metrô de São Paulo</h2>
        <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/mapa-da-rede-metro-jan-2021-scaled.jpg" alt="Mapa metrô">
      </section>
    </div>
  </div>
</main>

<div class="row">
  <div class="col-12">
    <?php get_template_part('template-parts/news-component'); ?>
  </div>
</div>

<div class="row">
  <div class="col-12">
    <?php get_template_part('template-parts/related-posts'); ?>
  </div>
</div>

<link rel="stylesheet" href="https://www.jqueryscript.net/demo/click-tap-image/dist/css/image-zoom.css">
<style>
  section {
    padding: 0 !important;
  }

  .related-content {
    border: none !important;
  }

  @media screen and (max-width: 767px) {
    .related-content {
      display: initial !important;
    }
  }
</style>




<script>
  jQuery(document).ready(function($) {

    // Anchor adjust
    $(document).on("click", ".anchor", function() {
      $(".fa-caret-right").each(function() {
        $(this).addClass("subway__sidebar--hide");
      })

      const iconEl = $(this).parent().prev(".fa-caret-right");
      iconEl.removeClass("subway__sidebar--hide");

      const el = $(this).attr("data-section")
      $('html, body').animate({
        scrollTop: $("#" + el + "").offset().top - 150
      }, 500);
    })


    $(document).scroll(function() {
      var h = document.documentElement,
        b = document.body,
        st = 'scrollTop',
        sh = 'scrollHeight';

      const percent = (h[st] || b[st]) / ((h[sh] || b[sh]) - h.clientHeight) * 100;
      const el = $(".subway__sidebar");
      if (percent >= 65) {
        el.addClass("subway__sidebar--hidden")
      } else {
        el.removeClass("subway__sidebar--hidden")
      }
    })
  });
</script>


<?php get_template_part('template-parts/footer-menus-widgets'); ?>
<?php get_footer(); ?>