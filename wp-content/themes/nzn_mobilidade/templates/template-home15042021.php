<?php

/**
 * Template Name: Home Templete
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

get_header();


?>
<main id="site-content" class="home-page" role="main">

   <?php
   if (!is_home()) {
      $archive_title    = get_the_archive_title();
      $archive_subtitle = get_the_archive_description();
   }

   if (have_posts()) {

      $i = 0;

      while (have_posts()) {
         $i++;
         if ($i > 1) {
            echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
         }
         the_post();
         if (!get_field('banner_home')) {
            get_template_part('template-parts/content-cover-home', get_post_type());
         }
      }
   } elseif (is_search()) {
   ?>

      <div class="no-search-results-form section-inner thin">

         <?php
         get_search_form(
            array(
               'label' => __('search again', 'nznmobilidade'),
            )
         );
         ?>

      </div><!-- .no-search-results -->

   <?php
   }
   if (!get_field('bloco_duas_colunas')) {

   ?>

      <div id="inovarparaincluir" class="first-section">
         <div class="container">
            <div class="row d-flex align-items-end  bg-withe">
               <?php if (is_active_sidebar('home_mobilidade')) { ?>
               <?php dynamic_sidebar('home_mobilidade');
               } ?>

               <?php if (is_active_sidebar('sobre_o_evento_mobilidade')) { ?>
               <?php dynamic_sidebar('sobre_o_evento_mobilidade');
               } ?>
            </div>
         </div>
      </div>
      <?php if (is_active_sidebar('home_mobilidade')) { ?>

   <?php
      } //sidebar ativa
   } //remove bloco
   ?>


   <!-- Logo patrocinadores -->
   <div class="container patrocinadores__home" style="margin-bottom:5%;margin-top:5%">
      <div class="row">
         <div class="col-md-4 col-12">
            <h4 style="text-align:left;" class="title">Realização</h4>
         </div>
         <div class="col-md-4 col-12">
            <h4 style="text-align:left;" class="title">Oferecimento</h4>
         </div>
         <div class="col-4" style="padding-left:0px !important;">
            <h4 style="text-align:left;" class="title">Patrocinadores</h4>
         </div>
      </div>
      <div class="row align-items-center">
         <div class="col-4">
            <img style="width:200px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/logo-estadao-cavalo.png" alt="estadao">
         </div>
         <div class="col-4">
            <img width="110px" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/Grupo_CCR.png" alt="ccr">
         </div>
         <div class="col-4">
            <div class="row justify-content-center align-items-center" style="flex-wrap:nowrap !important">
               <img style="width:110px;height:110px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/03/99logo.png" alt="99">
               <img style="width:200px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/Bosch_4-color_Bosch_SL-pt_4C_L.png" alt="bosch">
               <img style="width:120px;height:110px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/03/Logo-Honda-Asas-Vermelho.png" alt="honda">
            </div>
         </div>
      </div>
   </div>
   <!-- Fim Logo patrocinadores -->

   <!-- Logo patrocinadores mobile -->
   <div class="container patrocinadores__home--mobile">
      <div class="patrocinadores__container mb-5 mt-5">
         <div>
            <h4 style="text-align:left;" class="title mb-5">Realização</h4>
            <img style="width:180px;margin-left:50%" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/logo-estadao-cavalo.png" alt="estadao">
         </div>
      </div>
      <div class="patrocinadores__container mb-5">
         <div>
            <h4 style="text-align:left;" class="title mb-5">Oferecimento</h4>
            <img style="width:100px;margin-left:100%" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/Grupo_CCR.png" alt="ccr">
         </div>
      </div>
      <div class="patrocinadores__container">
         <div>
            <h4 style="text-align:left;" class="title">Patrocinadores</h4>
            <div class="row justify-content-center align-items-center" style="flex-wrap:nowrap !important">
               <img style="width:80px;height:80px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/03/99logo.png" alt="99">
               <img style="width:180px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/04/Bosch_4-color_Bosch_SL-pt_4C_L.png" alt="bosch">
               <img style="width:100px;height:100px;padding:20px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/03/Logo-Honda-Asas-Vermelho.png" alt="honda">
            </div>
         </div>
      </div>
   </div>
   <!-- FimLogo patrocinadores mobile -->

   <?php
   if (is_front_page()) :
      get_template_part('template-parts/carousel-fixpost');
   endif;
   ?>

   <?php get_template_part('template-parts/news-component');
   ?>

   <div class="third-section">
      <div class="container">
         <div class="row d-flex align-items-stretch">
            <div class="col-md-12 col-lg-8">
               <?php
               get_template_part('template-parts/last-news');
               ?>
            </div>
            <div class="col-md-12 col-lg-4">
               <?php
               get_template_part('template-parts/most-views');
               ?>
            </div>
         </div>
      </div>
   </div>

   <div id="super-banner" class="container">
      <div class="row">
         <div class="col-12">
            <?php
            get_template_part('template-parts/home-super_banner');
            ?>
         </div>
      </div>
   </div>

   <div class="fourth-section">
      <div class="container">
         <div class="row d-flex align-items-stretch">
            <div class="col-12">
               <?php
               get_template_part('template-parts/list_conteudos-especiais');
               ?>
            </div>
            <div class="col-12">
               <?php
               get_template_part('template-parts/list_conteudos-mobilidade');
               ?>
            </div>
         </div>
      </div>
   </div>



   <?php get_template_part('template-parts/pagination'); ?>

</main><!-- #site-content -->
<?php get_template_part('template-parts/footer-menus-widgets'); ?>
<?php get_footer(); ?>