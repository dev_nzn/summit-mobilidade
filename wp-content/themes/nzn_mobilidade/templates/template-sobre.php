<?php

/**
 * Template Name: Sobre o evento
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage nzn_agro
 * @since NZN agro 1.0
 */

get_header();
?>


<main id="site-content" class="custom-template-page pages_page" role="main">

    <!-- Header página -->
    <header class="entry-header tax has-text-align-left header-group ">
        <div class="entry-header-inner section-inner ">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1 class="entry-title nome-categoria-paginas-internas">Sobre o Summit Mobilidade</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Conteúdo principal -->
    <div class="container">

        <div class="row">
            <div class="col-lg-6">

                <!-- Texto introdução -->
                <div class="introducao">
                    <div>
                        <h3>Transição para uma nova cidade</h3>
                        <p>Diante de uma necessidade mundial, fizemos o que parecia improvável pouco tempo atrás: mudamos o escritório para casa, o jeito de nos locomover e a utilização dos espaços públicos. Muito ainda vai mudar e muito ainda precisa
                            mudar. As cidades não serão mais as mesmas. A grande novidade desta edição é o formato totalmente reformulado, com uma nova estrutura ainda mais dinâmica, interativa e relevante. Será uma semana dedicada à mobilidade urbana,
                            com eventos online, gratuitos e muito mais conteúdo: painéis temáticos, painéis branded, análise dos editores do <b>Estadão</b> , websérie com 5 capítulos e, para encerrar a semana de eventos, teremos uma premiação especial
                            a pessoas e projetos que se destacaram em diversidade, inclusão e mobilidade.</p>
                    </div>
                </div>

                <!-- Cards -->
                <div class="sessao-sobre">
                    <div class="cards">
                        <div class="cards__item">
                            <i class="fas fa-wifi"></i> Conecte-se com os líderes do mercado
                        </div>
                        <div class="cards__item ">
                            <i class="far fa-comment-alt"></i> Compartilhe experiências
                        </div>
                        <div class="cards__item ">
                            <i class="far fa-star"></i> Assista palestras dos principais nomes do setor
                        </div>
                    </div>
                </div>


                <!-- Quem já participou -->
                <div class="sessao-sobre">
                    <div class="participantes">
                        <h2>Quem já participou</h2>
                        <div class="tmcarousel__container" items-per-view="2">
                            <!-- Previous button -->
                            <div>
                                <div class="tmcarousel__button-left">
                                    <i class="fas fa-chevron-left"></i>
                                </div>
                            </div>


                            <?php if (have_rows('adicionar_paletrantes')) : ?>
                                <?php
                                while (have_rows('adicionar_paletrantes')) : the_row();
                                    $foto = get_sub_field('foto');
                                    $nome = get_sub_field('nome');
                                    $profissao = get_sub_field('profissao');
                                    $descricao = get_sub_field('descricao');
                                ?>
                                    <div class="tmcarousel__item hidden">
                                        <div class="participantes__container">
                                            <?php
                                            if (!empty($foto)) : ?>
                                                <img src="<?php echo esc_url($foto['url']); ?>" alt="<?php echo esc_attr($foto['alt']); ?>" />
                                            <?php
                                            else : ?>
                                                <img src="https://summitagro.estadao.com.br/wp-content/uploads/2020/10/SemFoto.jpg" width="120" height="128" />
                                            <?php
                                            endif; ?>
                                            <h3>
                                                <?php echo $nome; ?> </h3>
                                            <p>
                                                <?php echo $profissao; ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_postdata(); ?>




                            <!-- Next button -->
                            <div>
                                <div class="tmcarousel__button-right">
                                    <i class="fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                        <div class="tmcarousel__pagination"></div>
                    </div>
                </div>

                <!-- Depoimentos -->
                <div class="sessao-sobre">
                    <div class="depoimento">
                        <h2>O que dizem sobre summit mobilidade</h2>
                        <div class="tmcarousel__container" items-per-view="1">
                            <!-- Previous button -->
                            <div>
                                <div class="tmcarousel__button-left">
                                    <i class="fas fa-chevron-left"></i>
                                </div>
                            </div>


                            <div class="tmcarousel__item hidden">
                                <div class="depoimento__container">
                                    <p>“O tema de 2021 foi Inovar para Incluir e o que a gente fez foi exatamente isso: inclusão. A gente abriu o evento para que mais pessoas pudessem acompanhar e discutir mobilidade”.</p>
                                    <img class="depoimento__imagem" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/02/unnamed.jpg" alt="">
                                    <h3 class="depoimento__nome--palestrante">Luis Fernando Bovo</h3>
                                    <span>Diretor de Projetos Especiais do Estadão</span>
                                </div>
                            </div>

                            <div class="tmcarousel__item hidden">
                                <div class="depoimento__container">
                                    <p>“Esse assunto nunca foi tão urgente. Com a pandemia e o isolamento, a gente tem sentido na pele o quanto as cidades precisam estar adaptadas e prontas para reconectar pessoas e possibilitarem que elas andem com
                                        segurança”.
                                    </p>
                                    <img class="depoimento__imagem" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/02/unnamed.jpg" alt="">
                                    <h3 class="depoimento__nome--palestrante">Pamela Vaiano </h3>
                                    <span>Diretora de Comunicação da 99</span>
                                </div>
                            </div>

                            <div class="tmcarousel__item hidden">
                                <div class="depoimento__container">
                                    <p>“A população está vivendo cada vez mais e as nossas cidades foram feitas em uma época em que a gente não tinha tanta consciência assim. É muito importante a gente falar de mobilidade para todos, porque assim teremos
                                        uma vida melhor”.</p>
                                    <img class="depoimento__imagem" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/02/unnamed.jpg" alt="">
                                    <h3 class="depoimento__nome--palestrante">Bruno Mahfuz </h3>
                                    <span> Sócio-fundador do Guiaderodas</span>
                                </div>
                            </div>

                            <!-- Next button -->
                            <div>
                                <div class="tmcarousel__button-right">
                                    <i class="fas fa-chevron-right"></i>
                                </div>
                            </div>
                        </div>
                        <div class="tmcarousel__pagination"></div>
                    </div>
                </div>

                <!-- Ultimas edicoes -->
                <div class="sessao-sobre">
                    <div class="edicoes">
                        <h3>Edições anteriores</h3>
                        <div>
                            <ul>
                                <li><a class="tab active" data-content="2019" class="active" href="javascript:void(0)">Vídeo</a></li>
                                <li><a class="tab" data-content="2020" class="active" href="javascript:void(0)">Fotos</a></li>
                                <hr>
                            </ul>
                            <div class="edicoes__conteudo">
                                <div class="tab__content active" id="2019">
                                    <video src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/02/Summit-Mobilidade-2020-Oficial-1.mp4" controls></video>
                                </div>
                                <div class="tab__content" id="2020">
                                    <div class="tab__content--images">
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_0668-scaled.jpg">
                                        </div>
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_9991-scaled.jpg">
                                        </div>
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_0057-scaled.jpg">
                                        </div>
                                    </div>

                                    <div class="tab__content--images">
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_0010-scaled.jpg">
                                        </div>
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_0780-scaled.jpg">
                                        </div>
                                        <div>
                                            <img src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2020/10/MG_0759-scaled.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Ultimas edicoes -->
                <div class="sessao-sobre">
                    <div class="realizacao">
                        <h3>Conheça o Estadão Blue Studio</h3>
                        <p>Nós conectamos pessoas às marcas.</p>
                        <p>
                            Reúne o conhecimento de mais de 145 anos do <b>Estadão</b> com os maiores especialistas do mercado em desenvolver conteúdos de alta performance e credibilidade. No Estadão Blue Studio, você conta com um time de profissionais preparados para criar projetos especiais, branded contents, publicações e eventos que evidenciam e diferenciam a sua marca de maneira criativa e inovadora para uma audiência qualificada. Conte com a gente!
                        </p>
                        <p>Mais informações em: <a target="_blank" href="https://bluestudio.estadao.com.br/">Estadão Blue Studio</a> </p>
                    </div>
                </div>
            </div>

            <div class="widget_text widget_home col-lg-6">
                <!-- [contact-form-7 id="9416" title="Inscrições"] -->
                <div id="agende-se" class="formulario">
                    <div class="formulario__card">
                        <h3>Faça parte da mudança</h3>
                        <p>Agende-se para a próxima edição do Summit Mobilidade Urbana<br /><strong>que acontecerá de 16 a 20 de maio de 2022</strong></p>
                        <?php echo do_shortcode('[contact-form-7 id="9416" title="Inscrições"]'); ?>

                    </div>
                </div>
                <!-- <div class="widget_text widget_home_content" style="position:fixed;width:25%;">
          <h2 class="widgettitle">Garanta seu ingresso</h2>
          <div class="textwidget custom-html-widget">
            <h3 style="color:#4f3f99;font-weight: 600;margin-bottom:20px">100% online, gratuito e ao vivo</h3>
            <h3 style="color:#777777">17 a 21 de maio de 2021</h3>
            <a target="_blank" href="http://bit.ly/3rUvPfl" rel="noopener">Inscreva-se gratuitamente</a>
          </div>
        </div> -->
            </div>
        </div>
    </div>


</main>
<?php get_template_part('template-parts/footer-menus-widgets'); ?>
<?php get_footer(); ?>