<?php

/**
 * Template Name: Página do evento 2021
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage nzn_agro
 * @since NZN agro 1.0
 */

get_header();
?>

<?php $palestrante_cont = 0; ?>

<link href="https://vjs.zencdn.net/7.10.2/video-js.css" rel="stylesheet" />
<main id="site-content" class="custom-template-page pages_page" role="main">
    <!-- First section -->
    <section class="event__firstsection">
        <div class="container">
            <div class="row" style="align-items: center;">
                <div class="col-md-6 col-12">
                    <div class="event__firstsection-introduction">
                        <span>Temática 2021</span>
                        <h1>Transição para uma nova cidade</h1>
                        <div class="event__firstsection-divider"></div>
                        <p>Diante de uma necessidade mundial, fizemos o que parecia improvável pouco tempo atrás: mudamos o escritório para casa, o jeito de nos locomover e a utilização dos espaços públicos.</p>
                        <p>Muito ainda vai mudar e muito ainda precisa mudar. As cidades não serão mais as mesmas.</p>
                        <p>A grande novidade desta edição é o formato totalmente reformulado, com uma nova estrutura ainda mais dinâmica, interativa e relevante. Será uma semana dedicada à mobilidade urbana, com eventos online, gratuitos, e muito mais
                            conteúdo: painéis temáticos, painéis branded, análise dos editores do Estadão, websérie com 5 capítulos e, para encerrar a semana de eventos, teremos uma premiação especial a pessoas e projetos que se destacaram em diversidade,
                            inclusão e mobilidade.</p>
                    </div>
                </div>

                <div class="col-6">
                    <div class="event__firstsection-form">
                        <h1>Garanta seu ingresso</h1>
                        <h2>100% online, gratuito e ao vivo</h2>
                        <h3>17 a 21 de maio de 2021</h3>
                        <button onclick=" window.open('https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&partnerref=EmailConvite&eventid=3064799&sessionid=1&key=F1557556498BEEB5EC9CC0C5A72BA822&regTag=2183298&V2=false&sourcepage=register','_blank')">INSCREVA-SE GRATUITAMENTE</button>
                        <br>
                        <div class="text__information">
                            <h3>Baixe o APP de Eventos do Estadão para interagir com os participantes do evento</h3>
                            <div style="display: flex;justify-content: right;padding: 5% 0%;">
                                <a href="https://apps.apple.com/br/app/estad%C3%A3o-eventos/id1507950176" target="_blank"><img style="margin-right:15px;width: 180px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/apple.png"></a>
                                <a target="_blank" href="https://play.google.com/store/apps/details?id=net.moblee.estadao&amp;hl=pt_BR&amp;gl=US"><img style="width: 180px;margin-left:15px;" src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/05/android.png"></a>
                            </div>
                            <span>OBS: Devido ainda ao cenário pandemico, alguns eventos ou publicações poderão sofrer alterações em data ou formato. Informaremos com a máxima antecedencia. </span>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>
    <!-- End First section -->
    <div class="row">
        <div class="col-12">

            <div class="event__firstsection-form1 w-100">
                <h1>Conheça o prêmio mobilidade</h1>
                <button onclick=" window.open('https://summitmobilidade.estadao.com.br/regulamento-premio-vozes-da-mobilidade/')">PRÊMIO MOBILIDADE</button>
                <br>
                <div class="text__information">
                    <span>Inscrições até 26 de abril</span>
                </div>
            </div>
        </div>
    </div>
    <section class="event__secondsection">
        <div class="container">
            <div class="row" style="align-items: flex-start;">
                <div class="col-md-6 col-12">
                    <div class="play-btn"><span></span></div>
                    <video id="myVideo" controls src="https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/02/Summit-Mobilidade-2020-Oficial-1.mp4"></video>
                </div>

                <div class="col-md-6 col-12">
                    <div class="event__secondsection-introduction">
                        <h1>Sobre o Estadão Blue Studio</h1>
                        <div class="event__secondsection-divider"></div>
                        <p>
                            <?php the_field('texto_sobre'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End second section -->


    <section class="event__thirdsection">
        <div class="container">
            <div class="row">
                <div class="event__thirdsection-introduction col-12">
                    <h1>Programação 2021</h1>
                    <div class="event__thirdsection-divider"></div>
                </div>
                <div class="event__thirdsection-tabs">
                    <ul>
                        <?php
                        $cont = 0;
                        if (have_rows('escala_programacao')) :
                            while (have_rows('escala_programacao')) : the_row();
                                $data = get_sub_field('data');

                        ?>
                                <?php if ($cont == 0) : ?>
                                    <li>
                                        <a data-tab="tab<?php echo $cont; ?>" class="tab-link active" href="javascript:void(0)">
                                            <?php echo $data; ?>
                                        </a>
                                    </li>
                                <?php else : ?>
                                    <li>
                                        <a data-tab="tab<?php echo $cont; ?>" class="tab-link" href="javascript:void(0)">
                                            <?php echo $data; ?>
                                        </a>
                                    </li>
                                <?php endif;  ?>
                        <?php
                                $cont++;
                            endwhile;
                        endif
                        ?>
                    </ul>
                </div>
            </div>
            <div class="row">

                <?php
                $cont = 0;
                if (have_rows('escala_programacao')) :
                    while (have_rows('escala_programacao')) : the_row();
                        $data = get_sub_field('data');
                        $sessoes = get_sub_field('sessoes');
                        $tituloEscala = get_sub_field('titulo');
                        $descricaoEscala = get_sub_field('descricao_escala');
                ?>

                        <div id="tab<?php echo $cont; ?>" <?php if ($cont == 0) {
                                                                echo "class='col-12 event_thirdsection-panel'";
                                                            } else {
                                                                echo "class='col-12 event_thirdsection-panel hide'";
                                                            } ?>>

                            <div class="event__thirdsection-description">
                                <h2><?php echo $tituloEscala; ?></h2>
                                <p><?php echo $descricaoEscala; ?></p>
                            </div>


                            <?php if (have_rows('sessoes')) : ?>
                                <?php $sec_cont = 1; ?>
                                <?php while (have_rows('sessoes')) : the_row(); ?>
                                    <?php
                                    $titulo = get_sub_field('titulo');
                                    $horario = get_sub_field('horario');
                                    $descricao = get_sub_field('descricao');
                                    $palestrantes = get_sub_field('paineis');
                                    $tituloPainel = get_sub_field('titulo_painel');
                                    ?>
                                    <!-- Loop de Paineis -->
                                    <div id="accordion">
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <div class="row justify-content-between align-items-center event__content">
                                                    <div class="event__content-title">
                                                        <span><?php echo $tituloPainel; ?> </span>
                                                        <h1>
                                                            <?php echo $titulo; ?>
                                                        </h1>
                                                        <span class="event__time"><i class="far fa-clock"></i> <?php echo $horario; ?></span>
                                                        <!--<h3><a href="#"><i style="font-size:1rem" class="fas fa-plus"></i> Adicionar ao Google Agenda</a></h3>-->
                                                    </div>
                                                    <div>
                                                        <a data-toggle="collapse" data-target="#collapse<?php echo $sec_cont; ?>" aria-expanded="true" aria-controls="collapse<?php echo $sec_cont; ?>">
                                                            <i class="arrow-down fas fa-chevron-down"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="collapse<?php echo $sec_cont; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                                <div class="card-body event__thirdsection-peoplebox">
                                                    <div class="event__thirdsection-maintext">
                                                        <?php echo $descricao; ?>
                                                    </div>
                                                    <!-- Loop de mediadores -->
                                                    <?php if (have_rows('paineis')) : ?>
                                                        <?php while (have_rows('paineis')) : the_row(); ?>
                                                            <?php
                                                            $palestranteNome = get_sub_field('titulo');
                                                            $palestranteTitulo = get_sub_field('destaque');
                                                            $palestranteFoto = get_sub_field('foto');
                                                            $palestranteCargo = get_sub_field('descricao');
                                                            $palestranteBio = get_sub_field('mini_bio_descricao');
                                                            ?>
                                                            <div class="row align-items-center event__thirdsection-mediator">
                                                                <div class="col-md-2 col-6 event__thirdsection-mediator--column-img">
                                                                    <img src="<?php echo $palestranteFoto; ?>" alt="">
                                                                </div>
                                                                <div class="col-md-6 col-6 event__thirdsection-mediator--column-text">
                                                                    <span><?php echo $palestranteTitulo; ?></span>
                                                                    <h1>
                                                                        <?php echo $palestranteNome; ?>
                                                                    </h1>
                                                                    <h2>
                                                                        <?php echo $palestranteCargo; ?>
                                                                    </h2>
                                                                    <h3><a data-toggle="modal" data-target="#modal<?php echo $palestrante_cont; ?>" href="javascript:void(0)"> Saiba mais <i class="fas fa-chevron-right"></i></a></h3>
                                                                </div>
                                                            </div>
                                                            <div class="event__thirdsection--mediator-divider"></div>

                                                            <!-- Modal -->
                                                            <div class="modal fade" id="modal<?php echo $palestrante_cont; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <div>
                                                                                <h1><?php echo $palestranteTitulo; ?></h1>
                                                                                <div class="event__thirdsection-divider--2"></div>
                                                                                <div class="event__thirdsection--mediator-introduction">
                                                                                    <img src="<?php echo $palestranteFoto; ?>" alt="">
                                                                                    <h2>
                                                                                        <?php echo $palestranteNome; ?>
                                                                                    </h2>
                                                                                    <span><?php echo $palestranteCargo; ?> </span>
                                                                                    <p>
                                                                                        <?php echo $palestranteBio; ?>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php $palestrante_cont++; ?>
                                                        <?php endwhile; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Fim Loop de Paineis -->
                                    <?php $sec_cont++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </div>
                <?php
                        $cont++;
                    endwhile;
                endif
                ?>
            </div>
        </div>
    </section>



    <!-- Fourth section -->
    <section class="event__fourthsection">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-12">
                    <h1>Edições anteriores</h1>
                    <div class="event__fourthsection-divider"></div>
                    <h2>Fique por dentro das edições passadas.</h2>
                </div>
                <div class="col-md-9 col-12 galery">


                    <div class="thumbnail">
                        <?php
                        $cont = 1;
                        if (have_rows('edicoes_anteriores')) :
                            while (have_rows('edicoes_anteriores')) : the_row();
                                $img = get_sub_field('imagem');
                                $alt = get_sub_field('titulo');
                        ?>

                                <?php if ($cont < 7) : ?>
                                    <img src="<?php echo $img; ?>" alt="<?php echo $alt; ?>" class="thumb">
                                <?php else : ?>
                                    <img src="<?php echo $img; ?>" alt="<?php echo $alt; ?>" class="thumb hide--onmobile">
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- fifth section -->
    <section class="event__fifthsection">
        <div class="container">
            <div class="event__fifthsection-introduction">
                <h1>Realização</h1>
                <div class="event__fifthsection-divider"></div>
                <p>
                    <?php the_field('texto_realizacao'); ?>
                </p>
            </div>
            <div style="padding:5% 20% !important;">
                <?php get_template_part('template-parts/patrocinadores'); ?>
            </div>


        </div>
    </section>

    <section class="event__mobilefixed">
        <h1>17 a 21 de maio de 2021</h1>
        <button onclick=" window.open('https://event.on24.com/eventRegistration/EventLobbyServlet?target=reg20.jsp&partnerref=EmailConvite&eventid=3064799&sessionid=1&key=F1557556498BEEB5EC9CC0C5A72BA822&regTag=2183298&V2=false&sourcepage=register','_blank')">Inscreva-se gratuitamente</button>
    </section>
</main>
<?php get_template_part('template-parts/footer-menus-widgets'); ?>
<?php get_footer(); ?>

<script>
    (function($) {
        $(document).ready(function() {
            $(document).on("click", ".tab-link", function() {
                let data = $(this).attr("data-tab");
                $('.tab-link').each(function() {
                    $(this).removeClass('active');
                });
                $('.event_thirdsection-panel').each(function() {
                    $(this).addClass('hide');
                })
                $("#" + data + "").removeClass("hide");
                $(this).addClass("active");
            })

            $(document).on("click", ".play-btn", function() {
                var vid = document.getElementById("myVideo");
                vid.play();
                $(this).addClass("hide");
            })
        })

    })(jQuery);
</script>

<style>
    .col-md-9,
    .col-md-6,
    .col-md-3 {
        padding: 0 !important;
    }

    body {
        padding-right: 0 !important;
    }

    .event_thirdsection-panel {
        margin: 50px 0px 0px 0px !important;
        padding: 0 !important;
    }

    .event__firstsection {
        width: 100%;
        height: auto;
        background: url('https://summitmobilidade.estadao.com.br/wp-content/uploads/2021/03/background.png');
        background-repeat: no-repeat;
        background-size: cover;
        font-family: 'Poppins', sans-serif !important;
    }

    .event__firstsection-introduction {
        color: #fff !important;
        font-family: 'Poppins', sans-serif !important;
        width: 100%;
    }

    .event__firstsection-divider {
        height: 5px;
        border-radius: 10px;
        width: 6%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__firstsection-introduction span {
        font-size: 14px;
        font-weight: bold !important;
        text-transform: uppercase;
    }

    .event__firstsection-introduction h1 {
        font-size: 32px;
        font-weight: bold !important;
        letter-spacing: 1px;
    }

    .event__firstsection-introduction p {
        font-size: 16px;
        margin-bottom: 22px;
    }

    .event__firstsection-form {
        height: auto;
        width: 500px;
        background-color: #fff;
        padding: 7% 7%;
        border-radius: 5px;
        font-family: 'Poppins', sans-serif !important;
        float: right;
    }

    .event__firstsection-form h1 {
        font-family: 'Poppins', sans-serif !important;
        color: #4F3F99;
        letter-spacing: 0.2px;
        margin-bottom: 10px;
    }

    .event__firstsection-form h2 {
        color: #DE4092;
        font-size: 14px;
        font-weight: bold;
        font-family: 'Poppins', sans-serif !important;
        letter-spacing: 0.2px;
        margin-bottom: 10px;
    }

    .event__firstsection-form h3 {
        font-size: 14px;
        font-weight: bold;
        font-family: 'Poppins', sans-serif !important;
        letter-spacing: 0.2px;
    }

    .event__firstsection-form button {
        width: 100%;
        padding: 20px;
        border-radius: 5px;
        font-size: 16px;
        margin-bottom: 15%;
        margin-top: 10%;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    }

    .event__firstsection-form span {
        font-size: 14px;
        font-family: 'Poppins', sans-serif !important;
        font-weight: normal;
        color: #414141;
    }

    .event__firstsection-form .text__information {
        border-left: 2px solid #DE4092;
        padding-left: 10px;
    }

    .event__firstsection-form1 {
        height: auto;
        width: 100%;
        background-color: #fff;
        padding: 2% 2%;
        border-radius: 5px;
        font-family: 'Poppins', sans-serif !important;
        float: right;
    }

    .event__firstsection-form1 h1 {
        font-family: 'Poppins', sans-serif !important;
        color: #4F3F99;
        letter-spacing: 0.2px;
        margin-bottom: 10px;
        text-align: center;
    }

    .event__firstsection-form1 h2 {
        color: #DE4092;
        font-size: 14px;
        font-weight: bold;
        font-family: 'Poppins', sans-serif !important;
        letter-spacing: 0.2px;
        margin-bottom: 10px;
        text-align: center;
    }

    .event__firstsection-form1 h3 {
        font-size: 14px;
        font-weight: bold;
        font-family: 'Poppins', sans-serif !important;
        letter-spacing: 0.2px;
        text-align: center;
    }

    .event__firstsection-form1 button {
        width: 30%;
        min-width: 200px;
        padding: 20px;
        border-radius: 5px;
        font-size: 16px;
        margin: 5% auto;
        display: block;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    }

    .event__firstsection-form1 span {
        font-size: 14px;
        font-family: 'Poppins', sans-serif !important;
        font-weight: normal;
        color: #414141;
        text-align: center;
        display: block;
        position: relative;
    }

    .event__firstsection-form1 span:before {
        border-left: 2px solid #DE4092;
        margin-left: -9px;
        position: absolute;
        content: '';
    }

    .event__secondsection {
        background-color: #F8F8F8;
        padding: 110px 0;
    }

    .event__secondsection video {
        width: 100% !important;
        border-radius: 10px;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
    }

    .event__secondsection-introduction {
        padding: 0% 0% 0% 10%;
    }

    .event__secondsection-introduction span {
        color: #707070;
        font-size: 14px;
        font-weight: bold;
        text-transform: uppercase;
        letter-spacing: 0.42px;
        opacity: 1;
    }

    .event__secondsection-introduction h1 {
        font: normal normal bold 32px/38px Poppins;
        letter-spacing: 0.96px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__secondsection-introduction p {
        font: normal normal normal 16px/22px Poppins;
        letter-spacing: 0px;
        color: #414141;
        opacity: 1;
    }

    .event__secondsection-divider {
        height: 5px;
        border-radius: 10px;
        width: 10%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__thirdsection-introduction h1 {
        font: normal normal bold 32px/28px Poppins;
        letter-spacing: 0.96px;
        color: #4F3F99;
        opacity: 1;
        margin-left: 0 !important;
        padding-left: 0 !important;
    }

    .event__thirdsection-divider {
        height: 5px;
        border-radius: 10px;
        width: 4%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__thirdsection-divider--2 {
        height: 5px;
        border-radius: 10px;
        width: 10%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__thirdsection-tabs {
        margin-top: 30px 0px 50px 0px;
    }

    .event__thirdsection-tabs ul {
        position: relative;
        margin: 0 !important;
        border-bottom: 1px solid #d2d7da;
        line-height: 4.3em
    }

    .event__thirdsection-tabs ul li {
        display: inline;
        margin: 0;
        padding-right: 40px;
    }

    .event__thirdsection-tabs ul li a {
        text-align: left;
        font: normal normal normal 18px/24px Poppins;
        letter-spacing: 0px;
        color: #707070;
        opacity: 1;
        display: inline-block;
        position: relative;
    }

    .event__thirdsection-tabs ul li a.active {
        font: normal normal 600 18px/24px Poppins;
        letter-spacing: 0px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__thirdsection-tabs ul li a.active:after {
        position: absolute;
        content: '';
        border-bottom: 4px solid #DE4092;
        width: 100%;
        transform: translateX(-50%);
        bottom: -15px;
        left: 50%;
    }

    .event__content {
        padding: 4%;
    }

    .event__content-title {
        max-width: 80% !important;
    }

    .event__content span {
        text-align: left;
        font: normal normal 600 14px/24px Poppins;
        letter-spacing: 0px;
        color: #707070;
        opacity: 1;
    }

    .event__content h1 {
        text-align: left;
        font: normal normal 600 22px/24px Poppins;
        letter-spacing: 0px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__content h3 a {
        text-align: left;
        font: normal normal 600 14px/36px Poppins;
        letter-spacing: 0px;
        color: #707070;
        opacity: 1;
    }

    .event__content .event__time {
        text-align: left;
        font: normal normal 600 12px/36px Poppins;
        letter-spacing: 0px;
        color: #DE4092;
        opacity: 1;
    }

    .event__thirdsection-maintext {
        text-align: left;
        font: normal normal normal 16px/22px Poppins;
        letter-spacing: 0px;
        color: #414141;
        opacity: 1;
        margin-bottom: 30px;
        max-width: 60%;
        margin-left: 10%;
    }

    .event__thirdsection-peoplebox {
        padding: 38px 42px;
        width: 100%;
    }

    .event__thirdsection-description {
        margin-bottom: 45px;
    }

    .event__thirdsection-description h2 {
        font: normal normal 600 22px/24px Poppins;
        letter-spacing: 0px;
        color: #DE4092;
        opacity: 1;
    }

    .event__thirdsection-description p {
        text-align: left;
        font: normal normal normal 16px/24px Poppins;
        letter-spacing: 0px;
        color: #707070;
        opacity: 1;
    }

    .event__thirdsection-mediator {
        margin-left: 10%;
        margin-top: 3%;
    }

    .event__thirdsection-mediator--column-img {
        padding-left: 0px !important;
        max-width: 10% !important;
    }

    .event__thirdsection-mediator--column-text {
        padding-left: 0px !important;
    }

    .event__thirdsection-mediator img {
        width: 72px;
        height: 72px;
        border-radius: 100%;
        margin-right: 30px;
        object-fit: cover;
    }

    .event__thirdsection-mediator span {
        text-align: left;
        font: normal normal 600 14px/24px Poppins;
        letter-spacing: 0px;
        color: #707070;
        opacity: 1;
    }

    .event__thirdsection-mediator h1 {
        text-align: left;
        font: normal normal 600 16px/22px Poppins;
        letter-spacing: 0px;
        color: #DE4092;
    }

    .event__thirdsection-mediator h2 {
        text-align: left;
        font: normal normal normal 14px/22px Poppins;
        letter-spacing: 0px;
        color: #707070;
    }

    .event__thirdsection-mediator h3 a {
        text-align: left;
        font: normal normal 600 12px/24px Poppins;
        letter-spacing: 0px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__thirdsection--mediator-divider {
        height: 1px;
        background-color: #707070;
        width: 100%;
        opacity: 0.2;
        margin-top: 20px;
    }

    .modal-content {
        padding: 80px;
        background-color: #F7F7F7;
    }

    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 40% !important;
        }
    }

    .modal-content h1 {
        text-align: left;
        font: normal normal bold 32px/28px Poppins;
        letter-spacing: 0.96px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__thirdsection--mediator-introduction {
        background-color: #fff;
        padding: 35px;
    }

    .event__thirdsection--mediator-introduction h2 {
        text-align: left;
        font: normal normal 600 22px/28px Poppins;
        letter-spacing: 0px;
        color: #DE4092;
        margin-top: 10%;
    }

    .event__thirdsection--mediator-introduction span {
        text-align: left;
        font: normal normal normal 16px/28px Poppins;
        letter-spacing: 0px;
        color: #707070;
    }

    .event__thirdsection--mediator-introduction p {
        text-align: left;
        font: normal normal normal 16px/22px Poppins;
        letter-spacing: 0px;
        color: #414141;
        opacity: 1;
    }

    .event__thirdsection--mediator-introduction img {
        width: 100px;
        height: 100px;
        border-radius: 100%;
        object-fit: cover;
    }

    .event__fourthsection {
        background-color: #F8F8F8;
        padding: 110px 0;
    }

    .event__fourthsection h1 {
        text-align: left;
        font: normal normal bold 32px/38px Poppins;
        letter-spacing: 0.96px;
        color: #4F3F99;
        opacity: 1;
    }

    .event__fourthsection h2 {
        text-align: left;
        font: normal normal normal 16px/22px Poppins;
        letter-spacing: 0px;
        color: #414141;
        opacity: 1;
    }

    .event__fourthsection-divider {
        height: 5px;
        border-radius: 10px;
        width: 18%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__fifthsection {
        margin-top: 100px;
    }

    .event__fifthsection-centered {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
    }

    .event__fifthsection-centered h1 {
        margin-bottom: 30px;
    }

    .event__fifthsection-centered ul {
        list-style-type: none;
        display: flex;
        margin: 0 !important;
        align-items: center;
    }

    .event__fifthsection-introduction {
        width: 50%
    }

    .event__fifthsection h1 {
        text-align: left;
        font: normal normal bold 32px/38px Poppins;
        letter-spacing: 0.96px;
        color: #4F3F99;
        opacity: 1;
        margin-top: 50px;
    }

    .event__fifthsection-subtitle {
        text-align: center !important;
        font: normal normal 600 18px/24px Poppins !important;
        letter-spacing: 0px !important;
        color: #4F3F99 !important;
        opacity: 1;
    }

    .event__fifthsection p {
        text-align: left;
        font: normal normal normal 16px/24px Poppins;
        letter-spacing: 0px;
        color: #414141;
        opacity: 1;
    }

    .event__fifthsection-divider {
        height: 5px;
        border-radius: 10px;
        width: 10%;
        background-color: #FDC110;
        margin-top: 1.5%;
        margin-bottom: 3.5%;
    }

    .event__mobilefixed {
        display: none;
    }

    .arrow-down {
        font-weight: 900;
        color: #DE4092;
        opacity: 1;
        font-size: 2rem;
        cursor: pointer;
    }

    .hide {
        display: none !important;
    }

    /* PLAY BUTTON */

    .play-btn {
        cursor: pointer;
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        box-sizing: content-box;
        display: block;
        width: 32px;
        height: 44px;
        /* background: #fa183d; */
        border-radius: 50%;
        padding: 18px 20px 18px 28px;
    }

    .play-btn:before {
        content: "";
        position: absolute;
        z-index: 0;
        left: 50%;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
        display: block;
        width: 80px;
        height: 80px;
        background: #fff;
        border-radius: 50%;
        animation: pulse-border 1500ms ease-out infinite;
    }

    .play-btn:after {
        content: "";
        position: absolute;
        z-index: 1;
        left: 50%;
        top: 50%;
        transform: translateX(-50%) translateY(-50%);
        display: block;
        width: 80px;
        height: 80px;
        background: #fff;
        border-radius: 50%;
        transition: all 200ms;
    }

    .play-btn span {
        display: block;
        position: relative;
        z-index: 3;
        width: 0;
        height: 0;
        border-left: 32px solid #4F3F99;
        border-top: 22px solid transparent;
        border-bottom: 22px solid transparent;
    }

    @keyframes pulse-border {
        0% {
            transform: translateX(-50%) translateY(-50%) translateZ(0) scale(1);
            opacity: 1;
        }

        100% {
            transform: translateX(-50%) translateY(-50%) translateZ(0) scale(1.5);
            opacity: 0;
        }
    }

    /* Galery */

    .galery {
        width: 100%;
        border-radius: 8px;
        box-shadow: 5px 5px 25px rgba(0, 0, 0, 0.5);
        overflow: hidden;
    }

    .galery .thumbnail {
        display: flex;
        flex-wrap: wrap;
    }

    .galery .thumbnail .thumb {
        height: 150px;
        width: 25%;
        transition: all 0.2s;
        object-fit: cover;
        cursor: pointer;
        padding: 1px;
    }

    .galery .thumbnail .thumb:hover {
        box-shadow: 3px 3px 20px;
        opacity: 0.8;
    }

    .close {
        font-size: 3.5rem;
    }

    .modal-header {
        border: none !important;
    }

    @keyframes fade {
        to {
            opacity: 1;
        }
    }

    .effect {
        opacity: 0;
        animation: fade 0.2s forwards;
    }

    .active {
        opacity: 0.5;
    }

    /* Mobile */

    @media only screen and (max-width: 570px) {
        .event__firstsection-form {
            display: none;
        }

        .event__firstsection {
            height: auto;
            padding: 50px 30px;
        }

        .event__secondsection {
            padding: 50px 30px;
        }

        .play-btn:after {
            width: 50px;
            height: 50px;
        }

        .play-btn:before {
            width: 50px;
            height: 50px;
        }

        .play-btn {
            padding: 20px 0px 0px 15px;
        }

        .play-btn span {
            border-left: 22px solid #4F3F99;
            border-top: 12px solid transparent;
            border-bottom: 12px solid transparent;
        }

        .event__secondsection-introduction {
            padding: 50px 0px 0px 0px;
        }

        .event__thirdsection-introduction {
            padding: 0px 30px;
        }

        .event__thirdsection-introduction h1 {
            line-height: 1.2;
        }

        .event__thirdsection-tabs ul li {
            line-height: 5;
        }

        .event__thirdsection-tabs {
            padding: 0px 35px;
        }

        .event__thirdsection-tabs ul {
            margin-left: -0.5rem !important;
        }

        .event_thirdsection-panel {
            margin: 50px 0px 0px 0px !important;
            padding: 0 !important;
        }

        .event__thirdsection-divider {
            width: 20%;
        }

        .event__content {
            padding: 30px;
        }

        .event__thirdsection-peoplebox {
            padding: 30px;
        }

        .event__thirdsection-maintext {
            max-width: 100%;
            margin-left: 0px;
        }

        .event__thirdsection-mediator.col-md-2 {
            max-width: 10% !important;
        }

        .event__thirdsection-mediator--column-img {
            padding-left: 0px !important;
            max-width: 30% !important;
        }

        .event__fourthsection {
            padding: 50px 30px !important;
        }

        .event__fourthsection h2 {
            margin: 25px 0px 25px 0px;
        }

        .galery .thumbnail .thumb {
            width: 50%;
        }

        .hide--onmobile {
            display: none;
        }

        .event__fifthsection {
            display: none;
        }

        .modal-content {
            padding: 30px;
        }

        .modal-dialog {
            margin: 0 !important;
        }

        .modal-header {
            border-bottom: none !important;
        }

        .event__thirdsection--mediator-introduction {
            margin-top: 30%;
        }

        .event__thirdsection--mediator-introduction img {
            margin-left: -12%;
            top: 14%;
        }

        .event__thirdsection--mediator-introduction h2 {
            margin-top: 25%;
        }

        .event__mobilefixed {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            height: 104px;
            background: #F7F7F7 0% 0% no-repeat padding-box;
            box-shadow: 0px -3px 6px #00000029;
            opacity: 1;
            position: fixed;
            bottom: 0;
            z-index: 999 !important;
        }

        .event__mobilefixed h1 {
            font: normal normal bold 14px/22px Poppins;
            letter-spacing: 0px;
            color: #4F3F99;
            opacity: 1;
        }

        .event__mobilefixed button {
            background: #33CC99 0% 0% no-repeat padding-box;
            border-radius: 3px;
            opacity: 1;
            font-size: 1.4rem;
        }
    }
</style>