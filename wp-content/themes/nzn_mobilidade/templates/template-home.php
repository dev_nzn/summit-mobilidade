<?php

/**
 * Template Name: Home Templete
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

get_header();


?>
    <main id="site-content" class="home-page" role="main">

        <?php
    if (!is_home()) {
        $archive_title    = get_the_archive_title();
        $archive_subtitle = get_the_archive_description();
    }

    if (have_posts()) {

        $i = 0;

        while (have_posts()) {
            $i++;
            if ($i > 1) {
                echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
            }
            the_post();
            if (!get_field('banner_home')) {
                get_template_part('template-parts/content-cover-home', get_post_type());
            }
        }
    } elseif (is_search()) {
    ?>

            <div class="no-search-results-form section-inner thin">

                <?php
            get_search_form(
                array(
                    'label' => __('search again', 'nznmobilidade'),
                )
            );
            ?>

            </div>
            <!-- .no-search-results -->

            <?php
    }
    if (!get_field('bloco_duas_colunas')) {

    ?>

                <div id="inovarparaincluir" class="first-section">
                    <div class="container">
                        <div class="row d-flex align-items-end  bg-withe">
                            <?php if (is_active_sidebar('home_mobilidade')) { ?>
                            <?php dynamic_sidebar('home_mobilidade');
                    } ?>

                            <?php if (is_active_sidebar('sobre_o_evento_mobilidade')) { ?>
                            <?php dynamic_sidebar('sobre_o_evento_mobilidade');
                    } ?>
                        </div>
                    </div>
                </div>
                <?php if (is_active_sidebar('home_mobilidade')) { ?>

                <?php
        } //sidebar ativa
    } //remove bloco
    ?>
                    <!-- <div class="container patrocinadores__home" style="margin-top:5%">
                        <?php
        get_template_part('template-parts/patrocinadores');
        ?>
                    </div> -->
                    <?php
    if (is_front_page()) :
        get_template_part('template-parts/carousel-fixpost');
    endif;
    ?>
                        <a class="agende-se" href="https://summitmobilidade.estadao.com.br/sobre-o-evento#agende-se"><img src="<?php echo get_template_directory_uri() ?>/assets/images/agende-se-para-a-proxima-edicao.png" />
</a>
                        <?php get_template_part('template-parts/news-component');
    ?>

                        <div class="third-section">
                            <div class="container">
                                <div class="row d-flex align-items-stretch">
                                    <div class="col-md-12 col-lg-8">
                                        <?php
                    get_template_part('template-parts/last-news');
                    ?>
                                    </div>
                                    <div class="col-md-12 col-lg-4">
                                        <?php
                    get_template_part('template-parts/most-views');
                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="super-banner" class="container">
                            <div class="row">
                                <div class="col-12">
                                    <?php
                get_template_part('template-parts/home-super_banner');
                ?>
                                </div>
                            </div>
                        </div>

                        <div class="fourth-section">
                            <div class="container">
                                <div class="row d-flex align-items-stretch">
                                    <div class="col-12">
                                        <?php
                    get_template_part('template-parts/list_conteudos-especiais');
                    ?>
                                    </div>
                                    <div class="col-12">
                                        <?php
                    get_template_part('template-parts/list_conteudos-mobilidade');
                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <?php get_template_part('template-parts/pagination'); ?>

    </main>
    <!-- #site-content -->
    <?php get_template_part('template-parts/footer-menus-widgets'); ?>
    <?php get_footer(); ?>