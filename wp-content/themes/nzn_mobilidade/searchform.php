<?php
/**
 * The searchform.php template.
 *
 * Used any time that get_search_form() is called.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

/*
 * Generate a unique ID for each form and a string containing an aria-label
 * if one was passed to get_search_form() in the args array.
 */
$nznmobilidade_unique_id = nznmobilidade_unique_id( 'search-form-' );

$nznmobilidade_aria_label = ! empty( $args['label'] ) ? 'aria-label="' . esc_attr( $args['label'] ) . '"' : '';
?>
<form role="search" <?php echo $nznmobilidade_aria_label; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- Escaped above. ?> method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label for="<?php echo esc_attr( $nznmobilidade_unique_id ); ?>">
		<span class="screen-reader-text"><?php _e( 'Search for:', 'nznmobilidade' ); // phpcs:ignore: WordPress.Security.EscapeOutput.UnsafePrintingFunction -- core trusts translations ?></span>
		<input type="search" id="<?php echo esc_attr( $nznmobilidade_unique_id ); ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Digite o que procura', 'placeholder', 'nznmobilidade' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	
	<input type="submit" class="search-submit btn-inscreva" value="<?php echo esc_attr_x( 'Buscar', 'submit button', 'nznmobilidade' ); ?>" />
</form>
