(function($) {
	
  $(document).ready(function() {
    $('.tab').on("click", function(){
      let el = $(this);
      let content = el.attr("data-content");
      $('.tab').each(function(index){
        $(this).attr("class", "tab");
      })
      el.attr("class", "tab active");

      $('.tab__content').each(function(){
        $(this).attr("class", "tab__content");
      })

      $("#"+content+"").attr("class", "tab__content active")
    })
    
  });	
})( jQuery );
