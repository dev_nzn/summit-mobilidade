(function($) {
	
  $(document).ready(function() {

    let carousel = getRootDocument();
    carousel.each(function(carousel_index){
      let window_size = $( window ).width();
      let max_items;

      let refer = $(this);

      if(window_size <= 460){
        max_items = 1;
      }else{
        max_items = parseInt(refer.attr("items-per-view"));
      }
      
      let carousel_items = $(this).children('.tmcarousel__item'); 
      let total_items = carousel_items.length;
      let total_pages = total_items / max_items;
      let pagination_el = $(this).next('.tmcarousel__pagination');
      createPagination(total_pages,pagination_el);

     
      carousel_items.each(function(item_index){
        let item = $(this);
        item.attr("card-index", item_index);
        item.attr("carrousel-index", carousel_index)
        if(item_index <=  max_items - 1){
          item.attr("class", "tmcarousel__item active")
          item.addClass("active");
        }else{
          item.attr("class", "tmcarousel__item hidden")
        }
      }) 
    })

    // Next
    $('.tmcarousel__button-right').on("click", function(){
      let carousel = $(this).parent().parent('.tmcarousel__container');
      let carousel_items = carousel.children('.tmcarousel__item'); 
      let last_page = verifyIfIsLastPage(carousel);

      if (last_page.indexOf("active") < 0){
        next_page(carousel);

          let window_size = $( window ).width();
          let max_items;
    
          if(window_size <= 460){
            max_items = 1;
          }else{
            max_items = parseInt(carousel.attr("items-per-view"));
          }
          let last_active = carousel.children('.active').last();    
          let last_card_index = parseInt(last_active.attr('card-index'));
        
       
  
          carousel_items.each(function(){
            $(this).addClass("hidden");
            $(this).removeClass("active");
          })
          
  
          let next_card_index = last_card_index + 1; 
          for (var i = next_card_index; i < next_card_index + max_items; i++) {
            let next_card = carousel.children('.tmcarousel__item').filter('[card-index = "'+i+'"]');
            next_card.removeClass("hidden");
            window.setTimeout(function(){
              next_card.addClass("active");
            }, 100);
          } 
      }
    })

     // Previous
     $('.tmcarousel__button-left').on("click", function(){
      let carousel = $(this).parent().parent('.tmcarousel__container');
      let carousel_cards = carousel.children('.tmcarousel__item'); 

      previous_page(carousel);

      let window_size = $( window ).width();
      let max_items;
      if(window_size <= 460){
        max_items = 1;
      }else{
        max_items = parseInt(carousel.attr("items-per-view"));
      }
      let first_active = carousel.children('.active').first();
      let first_card_index = parseInt(first_active.attr('card-index'));

      carousel_cards.each(function(){
        $(this).addClass("hidden");
        $(this).removeClass("active");
      })
      

      let previous_card_index = first_card_index - 1; 
      for (var i = previous_card_index; i > previous_card_index - max_items; i--) {
        let previous_card = carousel.children('.tmcarousel__item').filter('[card-index = "'+i+'"]');
        previous_card.removeClass("hidden");
        window.setTimeout(function (){
          previous_card.addClass("active");
        }, 100);
      }
    })
  });

  function createPagination(total_pages, el){
    for(i=0;i<total_pages;i++){
      if(i ===  0){
        el.append(`<i class="fas fa-circle active"></i>`);
      }else{
        el.append(`<i class="fas fa-circle"></i>`);        
      }
    }
  }

  function next_page(root_el){
    var next_page = root_el.next(".tmcarousel__pagination").children('.active').next('.fa-circle');
    root_el.next(".tmcarousel__pagination").children().each(function(){
      $(this).removeClass("active");
    })

    next_page.attr("class", "fas fa-circle active")
  }

  function previous_page(root_el){
    var previous_page = root_el.next(".tmcarousel__pagination").children('.active').prev('.fa-circle');

    root_el.next(".tmcarousel__pagination").children().each(function(){
      $(this).removeClass("active");
    })

    previous_page.attr("class", "fas fa-circle active")
  }

  function getRootDocument(){
    return $('.tmcarousel__container');
  }

  function verifyIfIsLastPage(root_el){
    return root_el.next(".tmcarousel__pagination").children().last().attr("class");
  }

  
	
})( jQuery );
