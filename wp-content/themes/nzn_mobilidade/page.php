<?php
/**
 * Template Name: Pages Templete
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

 get_header();

 ?>
    <main id="site-content" class="custom-template-page taxonomy_post" role="main">

<?php
 if ( ! is_page_template() ) {
   $archive_title    = get_the_archive_title();
   $archive_subtitle = get_the_archive_description();
}


if ( have_posts() ) {

   $i = 0;

   while ( have_posts() ) {
      $i++;
      if ( $i > 1 ) {
         echo '<hr class="post-separator styled-separator is-style-wide section-inner" aria-hidden="true" />';
      }
      the_post();

      get_template_part( 'template-parts/entry-header', get_post_type() );

   }
} elseif ( is_search() ) {
   ?>

   <div class="no-search-results-form section-inner thin">

      <?php
      get_search_form(
         array(
            'label' => __( 'search again', 'nznmobilidade' ),
         )
      );
      ?>

   </div><!-- .no-search-results -->

   <?php
}
?>
<?php if ( is_front_page() || is_page( 'sobre-o-evento' ) ): ?>
<div class="first-section ">
      <img src="<?php echo get_template_directory_uri() ?>/assets/images/detail-right.svg" class="img-right" />
   <div class="container">
      <div class="row d-flex align-items-stretch bg-withe">
            <?php 
            if ( is_active_sidebar( 'sobre_o_evento_mobilidade' )) { ?>
                     <?php dynamic_sidebar( 'sobre_o_evento_mobilidade' );} ?>
            <!-- #publication .widget-area -->
      </div>
   </div>
      <img src="<?php echo get_template_directory_uri() ?>/assets/images/detail-left.svg" class="img-left" />
</div>
<?php endif; ?>
<?php if (is_page( 'sobre-o-evento' ) ): ?>

<?php get_template_part( 'template-parts/embed-video' ); ?>

<?php get_template_part( 'template-parts/programacao' ); ?>

<?php get_template_part( 'template-parts/edicao-anterior' ); ?>
<div class="container pat">
   <div class="row">
      <?php get_template_part( 'template-parts/patrocinadores' ); ?>
   </div>
</div>
<?php endif; ?>

<?php if (is_page( 'patrocinadores' ) ): ?>
  <div id="page-patrocinadores" class="container ">
     <div class="row">
         <?php get_template_part( 'template-parts/form-patrocinadores' ); ?>
         <?php get_template_part( 'template-parts/patrocinadores' ); ?>
      </div>
  </div>
<?php endif; ?>
<?php if (is_page( 'palestrantes' ) ): ?>
  <div id="page-patrocinadores" class="container ">
      <?php get_template_part( 'template-parts/palestrantes' ); ?>
  </div>
<?php endif; ?>
<?php    
 if (is_page( 'especiais', 'especiaistax', 'especiaistype' ) ): 

$terms = get_terms(
   array(
       'taxonomy'   => 'especiaistax',
       'hide_empty' => false
   )
);
   ?>
<div class="container post-list especiais">
   <div class="row">
      <?php
       
if ( ! empty( $terms ) && is_array( $terms ) ) {
   $i = 0;
   foreach ( $terms as $term ) { 
      $i++;
      if($i <= 2){
         ?>
      <div class="col-lg-4 col-md-6 col-12">
      <div class="content-image">
         <a href="<?php echo esc_url( get_term_link( $term ) ) ?>">
         <h3 style="background:<?php the_field('cor_primaria_do_patrocinador', $term); ?>" class="categoryTitle"><?php echo $term->name; ?></h3>

      </a>
            <div class="img-moda-350-230">
            <a href="<?php echo esc_url( get_term_link( $term ) ) ?>" class="link-content">  
            <img src="<?php the_field('logo_patrocinador', $term) ?>"  /></a>
            </div>
         </div>

         <div class="content-text">
         <a href="<?php the_permalink(); ?>" class="link-content">  

         <div class="date-time-card">
            <span class="data-time"><?php the_time('j \d\e F \d\e Y') ?></span>
          
         </div>
         <h3 class="model-title-last-posts"><?php echo $term->description; ?></h3>
      </a>   
      </div>

           
      </div>
         <?php

      }

?>
  
      <?php
   }
?>
   <div class="col-4">
      <div id="arroba-banner">
         <?php
            if( have_rows('arroba_banner_gerais', 'option') ):

               while( have_rows('arroba_banner_gerais', 'option') ): the_row();
       
               $banner = get_sub_field('banner');
               $link = get_sub_field('link');
       
                   
       
           if( $link ){
               
                   $link_url = $link['url'];
                   $link_title = $link['title'];
                   $link_target = $link['target'] ? $link['target'] : '_self';
                   
                   ?>
               <div class="arroba_banner">
               <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                   <img src="<?php echo esc_url( $banner ); ?>" alt="<?php echo esc_html( $link_title ); ?>" />
               </a>
               </div>
       <?php
           }
       endwhile;
       endif;
         ?>
         </div>
      </div>
      <?php
} 
      ?>
   </div>
</div>
<?php

 endif; ?>

<?php get_template_part( 'template-parts/pagination' ); ?>

</main><!-- #site-content -->
    <?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
    <?php get_footer(); ?>