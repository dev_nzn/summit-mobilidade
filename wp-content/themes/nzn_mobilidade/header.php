<?php

/**
 * Header file for the NZN Mobilidade WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

?>
    <!DOCTYPE html>

    <html class="no-js" <?php language_attributes(); ?>>

    <head>

        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="profile" href="https://gmpg.org/xfn/11">

        <?php wp_head(); ?>
        <meta name="msvalidate.01" content="9BBC9E4226018A237D2265C80B01C1C6" />
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-125552095-9"></script>
        <!-- Google Tag Manager -->
        <script>
            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-PKPPGQP');
        </script>
        <!-- End Google Tag Manager -->
        <meta name="google-site-verification" content="xSeeKs8DqX01fivHqBUPQOW1ruPtcMmDvjaJLiqJsQ8" />
    </head>

    <body <?php body_class(); ?>>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());
            gtag('config', 'UA-125552095-9');
        </script>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKPPGQP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <?php
	wp_body_open();

//	get_template_part('template-parts/patrocinadores-header');

	?>
            <!-- <header id="site-header" class="header-footer-group " role="banner"> -->
            <header id="site-header" class="header-footer-group header-prime" role="banner">
                <iframe class="estadao-header" src="https://www.estadao.com.br/parceiros" frameborder="0" width="100%" height="100%" title="Barra Parceiros Estadão" style="width:100%;max-height:44px;"></iframe>
                <div class="container">
                    <div class="row">

                        <div class="header-inner section-inner">
                            <div class="header-titles-wrapper col-md-3 col-lg-2">

                                <?php

						// Check whether the header search is activated in the customizer.
						$enable_header_search = get_theme_mod('enable_header_search', true);

						if (true === $enable_header_search) {

						?>

                                    <button class="toggle search-toggle mobile-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
								<span class="toggle-inner">
									<span class="toggle-icon">
										<?php nznmobilidade_the_theme_svg('search'); ?>
									</span>
									<!-- <span class="toggle-text"><?php _e('Search', 'nznmobilidade'); ?></span> -->
								</span>
							</button>
                                    <!-- .search-toggle -->

                                    <?php } ?>

                                    <div class="header-titles">

                                        <?php
							// Site title or logo.
							nznmobilidade_site_logo();

							// Site description.
							// nznmobilidade_site_description();
							?>

                                    </div>
                                    <!-- .header-titles -->

                                    <button class="toggle nav-toggle mobile-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
							<span class="toggle-inner">
								<span class="toggle-icon">
									<?php nznmobilidade_the_theme_svg('ellipsis'); ?>
								</span>
								<!-- <span class="toggle-text"><?php _e('Menu', 'nznmobilidade'); ?></span> -->
							</span>
						</button>
                                    <!-- .nav-toggle -->

                            </div>
                            <!-- .header-titles-wrapper -->

                            <div class="header-navigation-wrapper  col-md-9 col-lg-10">

                                <?php
						if (has_nav_menu('primary') || !has_nav_menu('expanded')) {
						?>

                                    <nav class="primary-menu-wrapper" aria-label="<?php esc_attr_e('Horizontal', 'nznmobilidade'); ?>" role="navigation">

                                        <ul class="primary-menu reset-list-style">

                                            <?php
									if (has_nav_menu('primary')) {

										wp_nav_menu(
											array(
												'container'  => '',
												'items_wrap' => '%3$s',
												'theme_location' => 'primary',
											)
										);
									} elseif (!has_nav_menu('expanded')) {

										wp_list_pages(
											array(
												'match_menu_classes' => true,
												'show_sub_menu_icons' => true,
												'title_li' => false,
												'walker'   => new nznmobilidade_Walker_Page(),
											)
										);
									}
									?>

                                        </ul>

                                    </nav>
                                    <!-- .primary-menu-wrapper -->

                                    <?php
						}

						if (true === $enable_header_search || has_nav_menu('expanded')) {
						?>

                                        <div class="header-toggles hide-no-js">

                                            <?php
								if (has_nav_menu('expanded')) {
								?>

                                                <div class="toggle-wrapper nav-toggle-wrapper has-expanded-menu">

                                                    <button class="toggle nav-toggle desktop-nav-toggle" data-toggle-target=".menu-modal" data-toggle-body-class="showing-menu-modal" aria-expanded="false" data-set-focus=".close-nav-toggle">
											<span class="toggle-inner">
												<!-- <span class="toggle-text"><?php _e('Menu', 'nznmobilidade'); ?></span> -->
												<span class="toggle-icon">
													<?php nznmobilidade_the_theme_svg('ellipsis'); ?>
												</span>
											</span>
										</button>
                                                    <!-- .nav-toggle -->

                                                </div>
                                                <!-- .nav-toggle-wrapper -->

                                                <?php
								}

								if (true === $enable_header_search) {
								?>

                                                    <div class="toggle-wrapper search-toggle-wrapper">

                                                        <button class="toggle search-toggle desktop-search-toggle" data-toggle-target=".search-modal" data-toggle-body-class="showing-search-modal" data-set-focus=".search-modal .search-field" aria-expanded="false">
											<span class="toggle-inner">
												<?php nznmobilidade_the_theme_svg('search'); ?>
												<!-- <span class="toggle-text"><?php _e('Search', 'nznmobilidade'); ?></span> -->
											</span>
										</button>
                                                        <!-- .search-toggle -->

                                                    </div>

                                                    <?php
								}
								?>

                                        </div>
                                        <!-- .header-toggles -->
                                        <?php
						}
						?>

                            </div>
                            <!-- .header-navigation-wrapper -->
                        </div>
                        <!-- .header-inner -->
                    </div>
                    <!-- .row -->

                </div>
                <!-- .container -->



            </header>
            <!-- #site-header -->
            <?php
	// Output the search modal (if it is activated in the customizer).
	if (true === $enable_header_search) {
		get_template_part('template-parts/modal-search');
	}
	?>
                <?php
	// Output the menu modal.
	get_template_part('template-parts/modal-menu');