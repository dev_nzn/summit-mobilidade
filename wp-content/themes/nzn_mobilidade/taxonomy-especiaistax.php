<?php
/**
 * Template Name: Especiais Taxonomy Templete 
 * Template Post Type: post, page
 *
 * @package WordPress
 * @subpackage Nzn_Mobilidade
 * @since NZN Mobilidade 1.0
 */

 get_header();
 $blog_page_image = wp_get_attachment_url( get_post_thumbnail_id(get_option( 'page_for_posts' )) );
 $taxonomy = get_the_terms( $post->ID, 'especiaistax' );     
 foreach ( $taxonomy as $tax){
 ?>
    <main id="site-content" class="custom-template-page page_especial page_especiais_temp" role="main">
        <div class="container banner-top">
            <div class="row d-flex justify-content-center">
                <div class="col-12">
                    <?php
                    get_template_part( 'template-parts/home-super_banner' );
                ?>
                </div>
            </div>
        </div>
        <header class="header-especial" style="background:<?php the_field('cor_primaria_do_patrocinador', $tax); ?>">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-6">
                        <h3 class="titleFixHeader">
                            <?php echo $tax->name; ?>
                        </h3>
                    </div>
                    <div class="col-6">
                        <div id="detailsHeader">
                            <span><?php the_field('texto1', $tax); ?></span>
                            <a href="<?php the_field('link_patrocinador', $tax); ?>">
                                <img src="<?php the_field('logo_patrocinador', $tax); ?>"  />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- .archive-header -->
        <div class="container">
            <div class="row post-list">
                <?php 
                
        
        if ( have_posts() ) {
            $i = 0;
            while ( have_posts() ) {
                $i++;
                the_post();
                get_template_part( 'template-parts/content-archive', get_post_type('especiaistype'));
                
                if($i == 6){
                    get_template_part( 'template-parts/home-arroba_banner' );
                }

            }
        }


        wp_reset_postdata();
        ?>
            </div>
        </div>

    <?php } get_template_part( 'template-parts/pagination', get_post_type('especiaistype') ); ?>

    </main>
    <!-- #site-content -->
    <?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>
    <?php get_footer(); ?>