r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"344";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-05-30 23:09:40";s:13:"post_date_gmt";s:19:"2020-05-30 23:09:40";s:12:"post_content";s:5680:"Com milhões de pessoas permanecendo em casa pelo máximo de tempo possível, a demanda pelo transporte público diminuiu. O Metrô de Madri, por exemplo, relatou uma queda de 90% no número de passageiros. No Brasil, um levantamento realizado pela Associação Nacional das Empresas de Transportes Urbanos (NTU) no final de abril revelou uma quantidade de usuários 80% menor.

<a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a>

Por outro lado, a frota diminuiu apenas 25%, o que leva a uma conta difícil de fechar: há muito menos receita, mas os custos não diminuíram na mesma proporção. Essa fórmula traz o questionamento de como será o futuro dos metrôs e ônibus — dos transportes públicos, em geral — depois que o isolamento social terminar.

<a href="/como-e-possivel-comprovar-que-a-mobilidade-caiu-na-quarentena/">Como é possível comprovar que a mobilidade caiu na quarentena?</a>

A presidente da Associação dos Engenheiros e Arquitetos de Metrô (AEAMESP), Silvia Cristina Silva, fez um diagnóstico certeiro: "Sobre como serão os transportes no futuro pós-pandêmico, um ponto parece certo, porque já era certo antes da pandemia: o transporte público urbano no Brasil não poderá depender mais apenas da tarifa para existir".

André Garcia, advogado especialista em segurança viária, também acredita que a tarifa do transporte público precisa ser repensada após o fim da quarentena. Ele também argumenta que o saldo dessa conta não pode ser repassado para os usuários: "A tendência já existente de déficit de passageiros deve continuar. Então, o subsídio deve ser aumentado, já que não se deve cogitar o aumento na tarifa para o cidadão que já está prejudicado economicamente devido à pandemia", pondera o advogado.
<h2 id="transporte-individual"><strong>Transporte individual</strong></h2>
O arquiteto e urbanista Evandro Augusto teme um aumento no uso de veículos particulares e cita como exemplo a cidade de Wuhan. Lá, apenas 34% dos passageiros que utilizavam o transporte coletivo voltaram a usar após o fim da quarentena enquanto o uso de automóveis dobrou. Evandro ainda aponta que o uso maior dos automóveis pode representar até mesmo um agravante na situação de pandemia, considerando que estudos já sugeriram uma relação entre má qualidade do ar e mortes pela covid-19.

<a href="/google-mostra-impacto-do-novo-coronavirus-na-mobilidade-urbana/">Google mostra impacto do novo coronavírus na mobilidade urbana</a>

Pesquisadores de Harvard descobriram que o aumento das partículas de poluição de somente uma micrograma por metro cúbico de ar aumentaria as chances de morte em 15%. De forma sucinta: poluição e coronavírus é uma combinação perigosa", explicaram os especialistas.

Além disso, essa nova preferência pelos carros particulares pode colocar a perder os esforços realizados ao longo dos últimos anos para incentivar as pessoas a usar mais o transporte público.
<h2 id="setor-est-realizando-investimentos"><strong>Setor está realizando investimentos</strong></h2>
Sílvia Cristina entende a necessidade do setor de transporte coletivo de investir em estruturas de higienização e segurança para que os passageiros se sintam impelidos a usar o transporte público.

A presidente, inclusive, aponta esforços que já foram implementados: "Os operadores estão se preocupando mais com a higienização dos veículos e das áreas de uso dos passageiros". Além disso, cita as experiências com <a href="/metro-de-sao-paulo-testa-tecnologia-contra-coronavirus/">aparelhos que emitem raios ultravioletas e podem eliminar 100% dos vírus em um curto espaço de tempo.</a>
<figure class="kg-card kg-image-card kg-card-hascaption"><img class="kg-image" src="https://lh3.googleusercontent.com/agDV3ZV3tm2nqdExiekKUnsZrs9PDiZqX1OosSMHjpD2W6bKZ1RiowYgNWhADFSPmNckEmyS2YN9Z5hC_voQMJGMFbRmihY76ZBTR9K1-uSF5XO_FWgUFwV4MFzdDmU1uvFoNwAT" alt="Novas tecnologias estão sendo testadas para higienizar vagões (Fonte: Imprensa Alexandre Baldy/Reprodução) " />
<figcaption>Novas tecnologias estão sendo testadas para higienizar vagões em São Paulo. (Fonte: Imprensa Alexandre Baldy/Reprodução)</figcaption></figure>
Outras medidas são sugeridas pelos especialistas, como disponibilizar álcool em gel próximo às catracas e adotar sistemas de bilhetagem eletrônica (sem dinheiro físico) para diminuir as chances de contágio.

A melhor ideia, segundo Evandro Augusto, é a de resolver tudo pelo celular, assim não seria necessário utilizar nem os aparelhos de recarga de cartões disponibilizados em algumas cidades brasileiras. “A solução não é tão inovadora. Existem muitos países que já implantaram esse sistema há alguns anos”, argumenta o arquiteto.

<a href="/coronavirus-como-motoristas-de-delivery-devem-se-proteger/">Coronavírus: como motoristas de delivery devem se proteger?</a>

Mesmo com grandes desafios para a retomada da normalidade, Evandro acredita que o futuro do setor pode ser positivo: "A pandemia é sem dúvida algo terrível, mas força as empresas privadas e o setor público a repensar e a otimizar sua operação, assim como a voltar mais eficiente".

Fonte: WRI Brasil; Sílvia Cristina Silva, presidente da Associação dos Engenheiros e Arquitetos de Metrô (AEAMESP); André Garcia, advogado especialista em segurança viária; Evandro Augusto, arquiteto e urbanista.

<a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a>";s:10:"post_title";s:48:"Como ficam metrôs e ônibus após a quarentena?";s:12:"post_excerpt";s:133:"Investimentos em qualidade, segurança e higienização, bem como o apoio do poder público, são importantes para o futuro do setor
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:44:"como-ficam-metros-e-onibus-apos-a-quarentena";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 22:46:34";s:17:"post_modified_gmt";s:19:"2020-08-01 01:46:34";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:36:"487a5e1e-a9e3-4cf9-a4d5-52f17847ecfa";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}