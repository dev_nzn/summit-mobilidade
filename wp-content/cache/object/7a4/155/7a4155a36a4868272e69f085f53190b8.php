r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7290;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-07 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-07 13:00:00";s:12:"post_content";s:5719:"<!-- wp:paragraph -->
<p>Durante a abertura da Conferência Mundial de Inteligência Artificial (WAIC) em Xangai, o CEO da Tesla, Elon Musk, afirmou que a empresa deve dominar o nível 5 dos carros autônomos até o final de 2020. Esse nível tecnologia permite que o veículo tenha autonomia total.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os veículos vendidos pela empresa já apresentam o hardware (como sensores e outros dispositivos) que torna possível a capacidade de direção totalmente autônoma (FSD, em inglês). No entanto, para que isso realmente aconteça, será necessário realizar uma atualização do software Autopilot, responsável por controlar essas funções no carro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No Twitter, Musk afirmou que, de dois a quatro meses, deve lançar um pacote de atualizações do software. “Praticamente tudo teve que ser reescrito, incluindo o nosso software de etiquetagem, para que seja fundamentalmente '3D' a cada passo do treinamento à inferência”, declarou o CEO no microblog.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Níveis dos carros autônomos</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7292,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/19144715704063-1024x535.jpg" alt="" class="wp-image-7292"/><figcaption>Os veículos oferecidos pela Tesla já possuem hardware que permite automação máxima. (Fonte: Tesla/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>A capacidade de direção dos carros autônomos é dividida nos <a href="https://summitmobilidade.estadao.com.br/conheca-niveis-automacao-carro/">níveis de 0 a 5</a>. Até o nível 2, o motorista pode dirigir o veículo com o suporte do software de direção. Assim, uma pessoa é responsável por supervisionar e, caso necessário, intervir nas ações automatizadas, como controle da aceleração e dos freios, bem como o controle de navegação.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A partir do nível 3, o veículo já é capaz de realizar todos os comandos necessários para a direção de forma autônoma, mas ainda requer um motorista para realizar algumas ações requisitadas pelo sistema. O carro pode atuar sozinho em engarrafamento, por exemplo. Entretanto, dependerá de uma série de condições para poder operar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No nível 4, a tecnologia já permite a operação de táxis autônomos e pode funcionar sem pedais nemvolantes, mas ainda depende de certas condições para funcionar.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No nível 5, a automação é completa, e o veículo funciona em qualquer condição sem a necessidade de um ser humano embarcado.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Tecnologia da Tesla</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7291,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/19144946205064-1024x587.jpg" alt="" class="wp-image-7291"/><figcaption>Tesla é referência em carros elétricos. (Fonte: Tesla/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Pioneira no setor, o Autopilot da companhia de Musk existe há cerca de cinco anos. Apesar de anunciada como uma tecnologia de mãos livres, os motoristas devem manter as mãos no volante o máximo possível. Atualmente, o Autopilot da empresa é uma combinação de controle de cruzeiro avançado com direção automatizada, além de algumas funções de manobra.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Os limites da Deep Learning</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Esta não é a primeira vez que Musk anuncia a autonomia em seus veículos. Em 2019, o executivo afirmou que táxis autônomos estariam disponíveis em 2020. Entretanto, a empresa adiou o lançamento da tecnologia para 2021.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/empresa-paranaense-cria-primeiro-carro-autonomo-do-brasil/">Empresa paranaense cria primeiro carro autônomo do Brasil</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ainda que todos os veículos da empresa tenham radar frontal e detectores de objetos ultrassônicos de proximidade, o principal componente para a tecnologia autônoma é um conjunto de oito câmeras alimentadas por algoritmos de Inteligência Artificial de Deep Learning. A ferramenta ajuda os carros a interpretar as imagens e a navegar sem colisões.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os algoritmos de Deep Learning estão melhorando na detecção de objetos nas imagens, mas ainda há um problema a ser superado: esses sistemas não são confiáveis ao encontrar novas situações. Em 2016, por exemplo, o algoritmo da empresa não detectou um caminhão branco por falta de contraste em um céu claro, o que resultou em um acidente fatal.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: <em>PC Magazine</em>, <em>BBC</em>, Car and Drive, Tesla, Business Insider.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:68:"Tesla anuncia que terá carros autônomos com nível máximo em 2020";s:12:"post_excerpt";s:88:"Anúncio foi feito durante abertura da Conferência Mundial de Inteligência Artificial
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:64:"tesla-anuncia-que-tera-carros-autonomos-com-nivel-maximo-em-2020";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 18:07:54";s:17:"post_modified_gmt";s:19:"2020-08-06 21:07:54";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7290";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}