r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"305";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-05-05 10:58:38";s:13:"post_date_gmt";s:19:"2020-05-05 10:58:38";s:12:"post_content";s:6816:"A covid-19 tem provocado modificações profundas no cotidiano das pessoas. Estima-se que um terço do planeta esteja em isolamento social. Escolas, universidades, empresas, órgãos públicos: todos os setores mudaram profundamente o funcionamento.

<a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a>

E, assim como a H1N1 deixou como herança o hábito de se lavar mais as mãos e usar álcool em gel com frequência, é provável que novos hábitos adquiridos diante da pandemia atual sejam ainda mais duradouros, inclusive no campo da mobilidade.

<a href="/google-mostra-impacto-do-novo-coronavirus-na-mobilidade-urbana/">Google mostra impacto do novo coronavírus na mobilidade urbana</a>

É fácil compreender por que a mobilidade se torna uma das áreas com mais impacto. O modelo urbano da maior parte das cidades, sobretudo das capitais, assume o seguinte padrão: nos bairros mais centrais se concentram as principais lojas e escritórios, ao passo que a residência das pessoas que trabalham nesses locais fica em bairros residenciais afastados. Nesse sentido, a forma como elas se locomovem ganha ainda mais importância.
<h2 id="transporte-p-blico"><strong>Transporte público</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh6.googleusercontent.com/artBnysgh2ZYm6VORHyw8lRCuT8SB6XS3gZYVRNc0eDzKzHF1aJdj0vcqOvO_J8Gwtvde_tuC-o47-vQWPPTbVgsKFoJOZkM4sDSaZl9dWE8nM0xIkYn2mCrHDfU69CnGuBIO6_1" class="kg-image"><figcaption>(Fonte: Shutterstock)</figcaption></figure>
O Brasil, assim como outros países, apresenta um paradoxo no campo dos transportes: a maior parte dos trabalhadores urbanos, em regra, recebe salários modestos e não possuem carro — ou, caso tenham, não o adotam como ferramenta de mobilidade cotidiana. Afinal, estão envolvidos aspectos como custos, congestionamento etc.; ao mesmo tempo, ainda é o veículo particular, e não ônibus, metrô, trem e bicicleta, que estrutura o modelo viário urbano.
<h2 id="transporte-individual-em-foco"><strong>Transporte individual em foco</strong></h2>
Quando ocorrem problemas graves que exigem medidas mais drásticas de contenção na interação social, como a covid-19 impõe neste período, a malha de transporte coletivo se mostra incapaz de oferecer soluções atrativas à maior parte das pessoas que precisam organizar os trajetos diários. E fica evidente, nesse momento, como a falta de uma aposta estratégica em outros modais pode cobrar um preço caro.

<a href="/como-estruturas-urbanas-podem-se-tornar-hospitais-de-campanha/">Como estruturas urbanas podem se tornar hospitais de campanha?</a>

Um bom exemplo disso é que, em diversas capitais, que tendem a possuir as melhores redes de transporte coletivo, percebeu-se uma diminuição no fluxo de ônibus: em vez de aumentar a circulação ou manter o horário convencional, para que cada veículo operasse com menos passageiros e houvesse espaço entre eles, o que se viu foi uma diminuição da frota e ainda mais pessoas disputando espaço dentro dos coletivos.

Os empresários do setor alegam que não há margem financeira para outra decisão e o Poder Público das cidades, até o momento, não apresentou uma política que redimensione a importância desse tipo transporte, que exige um planejamento com mais comprometimento.

Isso se torna ainda mais grave porque, segundo especialistas, a covid-19, terá um ciclo mais lento que outras pandemias, como a H1N1. Em 2009, quando essa doença se espalhou pelo mundo, havia uma taxa de transmissão menor, com menos letalidade (é o que se estima, apesar de a subnotificação impedir um cálculo exato) e com medicações funcionais no combate aos efeitos do vírus ainda durante a curva da pandemia, como o Tamiflu.

<a href="/coronavirus-como-motoristas-de-delivery-devem-se-proteger/">Coronavírus: como motoristas de delivery devem se proteger?</a>

Já o vírus atual se espalha facilmente, apresenta grande letalidade e os testes de medicação e vacinas ainda levarão algum tempo para dar respostas aplicáveis ao conjunto da população mundial.

Em virtude disso, é possível que se tenha atualmente alguns fluxos de quarentena, intercalados com afrouxamento do isolamento, de acordo com o monitoramento epidemiológico.

Ou seja, o novo coronavírus pede uma solução de médio e longo prazos para diversos assuntos, inclusive quanto à mobilidade. E, na ausência de uma política pública que dê conta disso, as pessoas que podem voltam a investir em veículos próprios, na contramão das necessidades sociais e ambientais das cidades.
<h2 id="carro-delivery"><strong>Carro delivery?</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/y2cHluukzloxTL0J5kUvtH_xS6el2T0mEGgjPxrKeJJ9-34u1HW5U54A3gplIi1RD4PG4ttEGinZBPlipC74KwTydXBK6GIlrJH2a3bpT_oh6wZI9kLI3GBF-w0lXu2VOuIM9TiI" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
Por ser o primeiro foco da doença, com a maior população do mundo, e ter um Estado centralizador com políticas firmes, a China pode ser um bom laboratório para se entender as mudanças que a covid-19 acarreta. E, segundo a Ipsos, agência internacional de pesquisa presente em diversos países, já há tendências visíveis.

<a href="/bogota-amplia-ciclovias-para-conter-expansao-do-coronavirus/">Bogotá amplia ciclovias para conter expansão do coronavírus</a>

No aspecto da mobilidade, os dados mais relevantes apontam para o desejo de comprar veículos, em virtude da falta de confiança inspirada pelo transporte coletivo. E, como os veículos são um bem de médio e longo prazos, é provável que essa decisão fomente uma nova injeção no modelo centrado no carro.

Além disso, as pessoas tenderão a inserir os contatos digitais na sua rotina: as plataformas de conversas e interações por áudio e vídeo tendem a permanecer no repertório dos cidadãos comuns, seja para contatar familiares, resolver problemas do trabalho, estudar ou cuidar da saúde, em teleconsultas.

<a href="/5-livros-sobre-mobilidade-urbana/">5 livros sobre mobilidade urbana</a>

Essas tendências já se expressam no Brasil. Em um dos maiores portais de venda online de veículos, que relata ter o maior número de veículos cadastrados no Brasil, já existe até mesmo a possibilidade de escolher um carro e recebê-lo em casa, tal como se faz hoje com compras de farmácias, restaurantes e supermercados.

Fonte: BBC, Ipsos.

<a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=texto&amp;utm_campaign=summitmobilidade19&amp;utm_content=::::&amp;utm_term=::::">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a>";s:10:"post_title";s:48:"Como a covid-19 estimula o transporte individual";s:12:"post_excerpt";s:122:"Entenda como a aposta no modelo centrado no carro tende a se intensificar diante da insuficiência do transporte coletivo
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:48:"como-a-covid-19-estimula-o-transporte-individual";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 23:39:05";s:17:"post_modified_gmt";s:19:"2020-08-01 02:39:05";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:36:"d78e517d-3b10-445c-9dc9-e57dea15b931";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}