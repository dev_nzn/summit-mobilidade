r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7190;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-05 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-05 13:00:00";s:12:"post_content";s:5574:"<!-- wp:paragraph -->
<p>A pandemia de coronavírus já atingiu cerca de 15 milhões de pessoas em todo o mundo, de acordo com a Universidade John Hopkins, nos Estados Unidos. Em meio a políticas de bloqueio e desincentivo a aglomerações, as camadas mais pobres das cidades são as mais expostas à doença.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.sympla.com.br/summit-mobilidade-urbana-2020__818066">O Summit Mobilidade Urbana 2020 será gratuito e online. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Isso acontece tanto pela falta de acesso a serviços básicos, como água e saneamento, quanto pela precariedade das moradias e um grande número de familiares compartilhando espaços pequenos.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":7192,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1226080630-1-1024x512.jpg" alt="" class="wp-image-7192"/><figcaption>População mais carente está mais exposta ao coronavírus por falta de serviços básicos e limitações econômicas. (Fonte: David Bokuchava / Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Outro ponto diz respeito à dificuldade que ricos e pobres encontram para fazer o isolamento social. A empresa de tecnologia Cuebiq analisou dados de localização de GPS de celulares de 275 mil usuários em Jacarta, na Indonésia, conseguiu identificar uma grande diferença entre comportamentos de ricos e pobres durante o enfrentamento da crise de covid-19.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Redução de mobilidade é maior entre ricos</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Em 15 de março, o governo indonésio pediu para que as pessoas não saíssem de casa, e a mobilidade em Jacarta foi reduzida significativamente. Uma semana depois, o estado de emergência foi declarado, e houve um declínio ainda maior nos deslocamentos na capital da Indonésia. Entretanto, a redução foi desigual.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O estudo analisou os usuários nos bairros 10% mais ricos e nos 10% mais pobres. Em média, ricos ou não, os usuários ficaram em casa 2,5 horas a mais que o normal, passando de 13 para 15,5 horas por dia. No entanto, quando se analisaram as categorias de riqueza individuais, outro cenário foi identificado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/empresas-de-carros-autonomos-entregam-alimentos-durante-pandemia/">Empresas de carros autônomos entregam alimentos durante pandemia</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os usuários mais pobres só ficaram em casa 11% a mais do que antes. Os mais ricos ficaram 20% a mais. Durante o período analisado, os mais pobres se deslocaram para os bairros ricos – o que pode significar que estavam se deslocando a trabalho. Enquanto isso, os ricos reduziram em um quarto o número de bairros visitados.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Influência da desigualdade social em outros países</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7191,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1162752061-1-1024x683.jpg" alt="" class="wp-image-7191"/><figcaption>Durante o isolamento, os mais pobres se deslocaram para os bairros ricos – o que pode significar que estavam se deslocando a trabalho. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Diferenças nos padrões de mobilidade entre ricos e pobres durante o isolamento social não foram observadas apenas na Indonésia. Análises recentes sugerem que as cidades americanas de Seattle, Los Angeles, Detroit e Chicago registraram quedas igualmente acentuadas na mobilidade na primeira quinzena de março, com famílias mais ricas ficando em casa mais tempo do que seus pares de baixa renda. Isso demonstra que as políticas de bloqueio afetam os cidadãos de maneira diferente.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Como os dados podem ajudar</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Além de verificar disparidades sociais, <a href="https://summitmobilidade.estadao.com.br/google-mostra-impacto-do-novo-coronavirus-na-mobilidade-urbana/">os dados de localização dos celulares podem fornecer informações úteis para auxiliar no combate ao coronavírus</a>. Os dados ajudam a direcionar de maneira mais eficaz as campanhas de informação, a prestação de serviços e as melhorias de segurança para o transporte público.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Análise também permite um diálogo mais amplo sobre medidas para proteger a saúde e os meios de subsistência dos cidadãos, particularmente os mais vulneráveis. Além disso, pode ajudar as autoridades a determinar como, quando e onde suspender as medidas de bloqueio.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Banco Mundial, Universidade John Hopkins,</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:67:"Isolamento social é menor entre população mais pobre, diz estudo";s:12:"post_excerpt";s:86:"Desigualdade social deixa população de baixa renda mais vulnerável ao coronavírus
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:63:"isolamento-social-e-menor-entre-populacao-mais-pobre-diz-estudo";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:27:48";s:17:"post_modified_gmt";s:19:"2020-08-03 21:27:48";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7190";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}