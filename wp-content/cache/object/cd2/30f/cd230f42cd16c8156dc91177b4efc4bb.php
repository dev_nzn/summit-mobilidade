G�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7305;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-07 13:20:49";s:13:"post_date_gmt";s:19:"2020-08-07 16:20:49";s:12:"post_content";s:6768:"<!-- wp:paragraph -->
<p>O maior e mais importante evento de mobilidade do Brasil está chegando. Na próxima quarta-feira (12), o Summit Mobilidade Urbana 2020 vai discutir o papel da inovação na consolidação de espaços urbanos mais inclusivos e inteligentes, por meio de debates e fóruns transmitidos online e gratuitamente. <a href="https://www.sympla.com.br/summit-mobilidade-urbana-2020__818066">Mas é preciso se inscrever para participar</a>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Promovido pelo <strong>Estadão</strong>, com apresentação da 99 e patrocínio da CCR, Veloe e Alelo, o evento abordará também as possibilidades de transformações nas cidades durante o período de pandemia.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Aproveitando o momento único de reconfiguração urbana, também estarão em pauta temas como o combate às desigualdades socioespaciais e o enfrentamento à crise provocada pelo coronavírus. Serão aproximadamente sete horas de discussões com nomes que são referências no setor.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Mobilidade pós-pandemia</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A abertura da programação será realizada pela presidente da Didi Chuxing, <a href="https://estadaosummitmobilidade.com.br/speaker/jean-liu/">Jean Liu</a>, que contará a experiência chinesa com mobilidade urbana, durante e depois da pandemia, e a nova relação das pessoas com o transporte público.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Didi anunciou que os pedidos de viagens pelo seu aplicativo na China retomaram, em junho, aos níveis anteriores ao período de pandemia, com um pico diário que chegou a mais de 30 milhões de solicitações na China. A empresa está presente na Ásia, Oceania e América Latina, inclusive no Brasil, onde é proprietária do 99 app.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Sustentabilidade para todos</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7307,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1255574770-1.jpg" alt="" class="wp-image-7307"/><figcaption>Evento discute como aprimoramento na infraestrutura urbana melhora qualidade de vida nas cidades. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Asecretária municipal de Mobilidade e Transporte,, <a href="https://estadaosummitmobilidade.com.br/speaker/elisabete-franca/">Elisabete França</a> e o presidente da CCR Mobilidade, <a href="https://estadaosummitmobilidade.com.br/speaker/luis-augusto-v-de-oliveira/">Luís Valença</a>, debaterão como atualizar a infraestrutura instalada nas cidades para permitir deslocamentos mais flexíveis e sustentáveis.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A cicloativista <a href="https://estadaosummitmobilidade.com.br/speaker/thuanne-fonseca-teixeira-2/">Thuanne Fonsêca Teixeira</a> contará a experiência da Associação Metropolitana de Ciclistas do Recife (Ameciclo), em especial do projeto Bota Pra Rodar, que atua em comunidades periféricas com a missão de transformar a cidade em ambientes mais humanos, democráticos e sustentáveis.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/mulheres-na-pesquisa-cientifica-e-o-desafio-da-mobilidade-urbana/">Mulheres na Pesquisa Científica e o Desafio da Mobilidade Urbana</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A sustentabilidade também será abordada em um painel que discutirá como as cidades podem facilitar deslocamentos e ampliar o acesso a oportunidades de emprego, estudo e moradia, com a presença de representantes da gestão pública, de empresas, além de pesquisadores sobre gestão urbana com foco no combate às desigualdades socioespaciais.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Segurança nos deslocamentos</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A segurança nos deslocamentos urbanos, em especial, dos perfis mais vulneráveis, como ciclistas, pedestres e pessoas com mobilidade reduzida será discutido em um painel com a presença da especialista em Transporte Urbano do Banco Mundial, <a href="https://estadaosummitmobilidade.com.br/speaker/bianca-bianchi-alves/">Bianca Bianchi Alves</a>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O painel também terá representantes de organizações não governamentais, que refletirão sobre como democratizar o acesso sem distinções de gênero ou por presença de desabilidades.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Do ativismo à gestão pública</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7306,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_110041646-1.jpg" alt="" class="wp-image-7306"/><figcaption>Alternativas ao uso de automóveis serão discutidos no Summit Mobilidade Urbana. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>O ativista americano <a href="https://estadaosummitmobilidade.com.br/speaker/kenneth-e-kruckemeyer/">Ken Kruckemeyer</a> falará sobre como as cidades funcionam e de que forma é possível tornar o espaço urbano melhor para todos. A prefeita de Boa Vista, <a href="https://estadaosummitmobilidade.com.br/speaker/teresa-surita/">Teresa Surita</a>, contará sobre a transformação urbana promovida na capital de Roraima, com investimentos na primeira infância, energia solar e mobilidade urbana. O conjunto de ações tornou a cidade a 6a capital no Brasil com maior quilometragem de ciclovias por habitante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A busca por alternativas para reduzir os congestionamentos causados pelo aumento recente da frota de carros particulares em cidades de médio e grande porte será o mote do diálogo entre o Head do Waze Carpool para América Latina, <a href="https://estadaosummitmobilidade.com.br/speaker/douglas-tokuno/">Douglas Tokuno</a>, e diretor-presidente da Companhia do Metropolitano de São Paulo – Metrô, <a href="https://estadaosummitmobilidade.com.br/speaker/silvani-pereira/">Silvani Pereira</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">Confira a programação completa do Summit Mobilidade Urbana 2020 e inscreva-se já.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:53:"Summit Mobilidade Urbana 2020 será online e gratuito";s:12:"post_excerpt";s:97:"Evento será no dia 12 de agosto e debaterá caminhos para cidades mais diversas e democráticas
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:52:"summit-mobilidade-urbana-2020-sera-online-e-gratuito";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-07 13:21:30";s:17:"post_modified_gmt";s:19:"2020-08-07 16:21:30";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7305";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}