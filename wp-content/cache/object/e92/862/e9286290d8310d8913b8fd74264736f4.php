[�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7200";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-06 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-06 13:00:00";s:12:"post_content";s:5251:"<!-- wp:paragraph -->
<p>Assim como outras cidades do mundo, Curitiba (PR) teve de adaptar sua estrutura para enfrentar a pandemia do novo coronavírus. O aumento de casos da doença impactou fortemente a rotina dos moradores e o sistema de mobilidade do município.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mudanças temporárias foram implementadas em meio a esse cenário, mas transformações de longo prazo têm sido pensadas em prol de uma cidade mais sustentável no pós-pandemia. <a href="https://summitmobilidade.estadao.com.br/o-que-muda-no-cenario-das-cidades-apos-a-pandemia/">Esse tipo de perspectiva tem sido cada vez mais comum no mundo todo</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Mudanças imediatas</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>No entorno do Mercado Municipal da capital paranaense, onde há movimentação de um grande número de pessoas, principalmente aos sábados, a prefeitura ampliou o sistema de circulação de pedestres e ciclistas com novas ciclofaixas. A estrutura, que começou a ser montada no fim de maio, foi demarcada com a pintura do pavimento e cones ao longo do trecho, fazendo conexões com a ciclofaixa que já existe na região.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":7202,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/21102716685041.jpg" alt="" class="wp-image-7202"/><figcaption>Projeto aumenta espaço para pedestres e cria uma faixa para ciclistas. (Fonte: Prefeitura Municipal de Curitiba)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Foram implementados paraciclos em frente ao Mercado e áreas de estacionamento para motocicletas que atendem a serviços de entrega. A calçada também foi ampliada, com a restrição temporária a estacionamentos e sinalização para garantir a segurança e o distanciamento entre os pedestres.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De acordo com o presidente do Instituto de Pesquisa e Planejamento Urbano de Curitiba (Ippuc) Luiz Fernando Jamur, o objetivo é garantir aos clientes do comércio da região mais uma opção de mobilidade limpa e segura e reduzir o número de pessoas dentro dos estabelecimentos, evitando aglomerações.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Transporte exclusivo</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Pensando nos profissionais da área de saúde que estão atuando no combate à covid-19, a cidade criou no fim de junho o Expresso Exclusivo Saúde, um ônibus que abrange uma área com 13 hospitais, além de Unidades de Pronto Atendimento, e serve tanto pessoas que atuam diretamente no setor quanto em áreas de apoio em hospitais, como limpeza, alimentação e atendimento administrativo.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Futuro mais sustentável</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7201,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/21103145872049.jpg" alt="" class="wp-image-7201"/><figcaption>Novo projeto prevê a criação de mais 50 quilômetros de ciclofaixas. (Fonte: Prefeitura Municipal de Curitiba)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Diante das transformações temporárias nas ruas, a prefeitura tem pensado em estratégias de longo prazo para melhorar a mobilidade urbana e tornar a cidade mais sustentável. Um projeto anunciado em junho prevê a ampliação da estrutura cicloviária de Curitiba, com obras de pavimentação urbana e a construção de estações de transporte. Em relação às ciclofaixas, a meta é implantar até o fim do ano mais 50 quilômetros de vias sinalizadas, somadas aos mais de 208 quilômetros da malha já existente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/google-mostra-impacto-do-novo-coronavirus-na-mobilidade-urbana/">Google mostra impacto do novo coronavírus na mobilidade urbana</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os investimentos serão estendidos à área da inovação da capital, com a estruturação de um grande centro destinado ao empreendedorismo, definindo o primeiro prédio público de Curitiba com certificação internacional de sustentabilidade. O projeto da nova ala do edifício está sendo desenvolvido pelo Ippuc dentro dos parâmetros do Certificado LEED (Leadership in Energy and Environmental Design), que credencia os chamados "edifícios verdes" no mundo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Prefeitura de Curitiba</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:62:"Curitiba planeja mobilidade mais sustentável após a pandemia";s:12:"post_excerpt";s:86:"Prefeitura prevê ampliação de malha cicloviária e construção de "prédio verde"
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:60:"curitiba-planeja-mobilidade-mais-sustentavel-apos-a-pandemia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:36:03";s:17:"post_modified_gmt";s:19:"2020-08-03 21:36:03";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7200";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}