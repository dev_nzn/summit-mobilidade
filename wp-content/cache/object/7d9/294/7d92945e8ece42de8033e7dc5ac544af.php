r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6237";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-03 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-03 13:00:00";s:12:"post_content";s:7756:"<!-- wp:paragraph -->
<p>Segundo o urbanista e pesquisador brasileiro Flávio Villaça, espaços urbanos são marcados pelas condições de deslocamento do ser humano, seja como força de trabalho, nas viagens de casa para o emprego, seja como consumidor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De acordo com o último levantamento feito pelo Departamento Nacional de Trânsito (Denatran), em 2019 no Brasil existia um carro para cada quatro habitantes, e o número não para de crescer — há apenas dez anos, a proporção era de 7,4 habitantes por automóvel. Esse aumento exponencial da frota brasileira ocorre porque os espaços urbanos do País foram pensados e planejados tendo os carros como protagonistas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ao longo dos anos, as prefeituras das grandes cidades brasileiras tentaram resolver os problemas de trânsito criando rotas, alargando avenidas e, consequentemente, diminuindo o espaço ocupado por pedestres e ciclistas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/por-que-e-importante-integrar-planos-diretores-e-de-mobilidade/">Por que é importante integrar planos diretores e de mobilidade?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Investir em obras de infraestrutura rodoviária há muito tempo já não é suficiente para frear os problemas de mobilidade urbana. É preciso repensar a estrutura das cidades, para criar condições mais favoráveis para meios de mobilidade alternativos e mais sustentáveis.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Mudanças na estrutura urbana brasileira</strong></h2>
<!-- /wp:heading -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh4.googleusercontent.com/gHUb_-vKrlPvAafrEI9tU9SkViI61vm7fO_CxcczX_Ib9ZGPH_dHQaIzrrjHrQTvPJRYr5vVO5qUAWCC7uEPq_aWabxOYqyw4x-RmwvL8cRNeM1aQAh5FzYziPSoV8kdI4qns-TY" alt="Cidades precisam criar condições favoráveis para que a população opte por meios alternativos de transporte como a bicicleta."/><figcaption>Cidades precisam criar condições favoráveis para que a população opte por meios alternativos de transporte, como a bicicleta. (Fonte: Unsplash)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Segundo o Projeto Cidades Eficientes, uma iniciativa do Conselho Brasileiro de Construção Sustentável e do Instituto Clima e Sociedade (ICS), garantir a mobilidade em espaços urbanos deveria ser uma das principais metas dos governos municipais, uma vez que tem relação direta não apenas com a qualidade de vida dos habitantes mas também com a geração de impactos positivos sobre o meio ambiente e a promoção de um crescimento econômico mais sólido e sustentável.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>De acordo com a organização, garantir uma mobilidade mais sustentável e eficiente exigirá uma série de medidas combinadas que envolvem políticas públicas, incentivos fiscais, mudanças nos códigos de obras e, principalmente, alterações no planejamento urbano.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As principais medidas englobam o subsídio ao transporte público de qualidade com zero emissão de gases, o incentivo à mobilidade não motorizada (ampliando o número de ciclovias, calçadas confortáveis e travessias seguras para pedestres) e o desestímulo do uso de automóveis por meio de supressão de vagas de estacionamento, implantação de taxas por congestionamento, redução de velocidade, entre outros.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/como-o-plano-diretor-de-sao-paulo-tem-melhorado-a-mobilidade/">Como o Plano Diretor de São Paulo tem melhorado a mobilidade?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para a ONG Mobilize, um sistema de mobilidade urbana sustentável também envolve a implantação de sistemas sobre trilhos, como metrôs, trens e bondes modernos (VLTs), esteiras rolantes, elevadores de grande capacidade e soluções inovadoras, como os teleféricos de Medelín (Colômbia), ou sistemas de bicicletas públicas, como os implantados em várias cidades da Europa.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O principal desafio, entretanto, é garantir a realização de tantas mudanças em um cenário em que muitas das cidades brasileiras foram criadas sem o planejamento adequado e continuam crescendo de maneira desordenada.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Plano Nacional de Mobilidade Urbana</strong></h2>
<!-- /wp:heading -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh5.googleusercontent.com/igG2bTJfRf1dLO_W-4i4yeuVac4pjOXUlRaNJx_hLp58tOrsQ-4ODu-eWMEzsjQvgYfMu95nSr3BtIOiWC3l2NlLaN7ImspSgPOPEobuBPDWtHoqovZMi2K1t-SGOCk-1Twmuqeq" alt="O Plano Nacional de Mobilidade prioriza o uso de meios de transportes ativos e meios de transporte público coletivos."/><figcaption>O Plano Nacional de Mobilidade prioriza o uso de meios de transporte ativos e coletivos. (Fonte: Unsplash)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Com a criação do Estatuto das Cidades, em 2001, e do Ministério das Cidades, em 2003, as gestões municipais receberam ferramentas e mecanismos para melhorar a administração e o planejamento urbano. Surgiram várias políticas nacionais por setores, entre elas Política Nacional de Habitação (2005), de Saneamento Básico (2007), de Resíduos Sólidos (2010), de Mobilidade Urbana (2012) e da Metrópole (2015).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O Plano Nacional de Mobilidade Urbana prioriza modos de transporte ativos (como bicicletas) e transporte público coletivo, além de defender a mitigação dos custos ambientais, sociais e econômicos dos deslocamentos urbanos e incentivar o uso de energias renováveis menos poluentes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Segundo a <em>Agência Brasil</em>, os municípios brasileiros que deveriam elaborar Planos de Mobilidade Urbana (PMU) até 2019 tiveram seus prazos adiados. Assim, cidades com mais de 250 mil habitantes terão até 2022 para finalizá-los, enquanto os municípios de até 250 mil habitantes terão até 2023.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fazem parte desse grupo localidades com mais de 20 mil habitantes ou que façam parte de regiões metropolitanas, aglomerações urbanas e Regiões Integradas de Desenvolvimento Econômico (Rides), que, ao todo, tenham mais de 1 milhão de habitantes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/o-papel-do-governo-na-elaboracao-de-planos-de-mobilidade-das-cidades/">O papel do governo na elaboração de planos de mobilidade das cidades</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Após a conclusão da minuta do plano, a proposta deve ser discutida com a sociedade civil, votada pela Câmara Municipal e instituída por meio de lei municipal ou decreto.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Agência Brasil, WRI Brasil, ONG Mobilize-se, Associação Nacional dos Detrans, Companhia do Metropolitano de São Paulo, Projeto Cidades Eficientes</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:68:"Entenda a relação entre estrutura urbana e mobilidade sustentável";s:12:"post_excerpt";s:136:"Não há como incentivar o uso de meios de transporte mais sustentáveis sem preparar as grandes cidades brasileiras para essa mudança
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:67:"entenda-a-relacao-entre-estrutura-urbana-e-mobilidade-sustentavel-2";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 19:51:10";s:17:"post_modified_gmt";s:19:"2020-08-06 22:51:10";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=6237";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}