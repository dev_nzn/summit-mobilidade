r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"445";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-07-27 06:07:46";s:13:"post_date_gmt";s:19:"2020-07-27 06:07:46";s:12:"post_content";s:5970:"<!-- wp:paragraph -->
<p>Um dos desafios fundamentais das <a href="/quais-mudancas-da-quarentena-vem-para-ficar/">cidades do futuro</a> é fornecer um sistema de mobilidade urbana mais igualitário e sustentável. A transição para um novo modelo inclui a implantação de ônibus mais limpos e silenciosos, visando melhorar o conforto dos usuários e reduzir o impacto ambiental.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade 2020 será online e gratuito. Inscreva-se agora</a><a href="https://bit.ly/2OoAy6M">.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>É nesse cenário que a substituição de ônibus a diesel por opções elétricas surge como uma solução cada vez mais oportuna. O Chile é um bom exemplo desse tipo de aposta. Em 2017, <a href="/como-os-onibus-eletricos-tem-transformado-a-capital-do-chile/">o governo chileno publicou a Estratégia Nacional de Eletromobilidade</a>, documento que prevê que pelo menos 40% dos carros particulares e 100% dos veículos de transporte público sejam movidos a eletricidade até 2050.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"className":"kg-card kg-image-card kg-card-hascaption"} -->
<figure class="wp-block-image kg-card kg-image-card kg-card-hascaption"><img src="https://lh6.googleusercontent.com/QHl1noAoouiHLmbSbZaX0TwPYggNjN3UgGTQcP6CGwDrpWJ_pxHImBR3Y8jucld3dblmN5iVqsT98KOP4evFTWisFlk4ZTwmYeQ4qWYw82lbgUiqODat8Q2aPwP4mE1_Skr99r9y" alt=""/><figcaption>Ônibus elétrico em Maipú, comuna de Santiago do Chile. (Fonte: Cristian Silva Villalobos / Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Em uma parceria entre governo e setor privado, duas concessionárias de energia compraram há três anos os primeiros veículos elétricos de diferentes fabricantes chineses e os alugaram para operadoras de transporte, que pagariam o aluguel, em parte, com tarifas de usuários.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Em 2020, Santiago do Chile já tem uma frota de aproximadamente 776 coletivos movidos a eletricidade, a maior do mundo fora da China. A Avenida Grécia, um eletrocorredor operado por ônibus sustentáveis, destaca-se pela funcionalidade, inovação e <a href="/conheca-a-primeira-cidade-inteligente-social-do-mundo/">interatividade</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os ônibus elétricos têm transformado a capital chilena e mostrado ao mundo que a implantação desse modelo de transporte pode ser extremamente benéfica tanto do ponto de vista econômico quanto ambiental.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Confira três ganhos importantes da implantação de ônibus elétricos nas cidades.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="1-redu-o-da-polui-o-atmosf-rica"><strong>1. Redução da poluição atmosférica</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"className":"kg-card kg-image-card kg-card-hascaption"} -->
<figure class="wp-block-image kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/6V8iw4r6WTGR6kwu666vobVHw8aWKDwTpwcp9UyvD0OTz6VNfb-8f8Cwcqf7TGSj8Ds0RxtcE-DnWA1wi2E-G8qgE6AfZWT4L0_TvgH-Lena0iC1w9E9GFWsXolRe6GQVyoTRz1P" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>No Brasil, onde aproximadamente 93% das viagens motorizadas utilizam combustível fóssil, segundo a Empresa de Pesquisa Energética (EPE), a substituição dos ônibus convencionais por elétricos pode salvar vidas e ter impacto positivo na economia. De acordo com a companhia, o setor de transportes respondeu por 42% das <a href="/automoveis-sao-a-principal-fonte-de-emissao-de-gases-poluentes/">emissões de dióxido de carbono (CO2</a>) em 2015.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No mesmo ano, a revista <em>Lancet </em>revelou que a poluição do ar matou 70.685 pessoas no Brasil, número que seria consideravelmente reduzido com a queda da emissão de gases poluentes na atmosfera.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="2-redu-o-da-polui-o-sonora"><strong>2. Redução da poluição sonora</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"className":"kg-card kg-image-card kg-card-hascaption"} -->
<figure class="wp-block-image kg-card kg-image-card kg-card-hascaption"><img src="https://lh3.googleusercontent.com/yhbq2odZb0NABwOheZQ4HcOFm3nqzqgmGiNxCGbN4Dl18cZJNtBDRc7O1ZIudgu1X2-p1peYxQEvqKz-OPFCPc6WrXLHZcBN-Hffj99a7OsAPdv7iKB5AMRexhakgLiOSuJ3biEt" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>A Organização Mundial da Saúde (OMS) reconhece o barulho como um poluente, sendo o terceiro mais danoso à saúde humana, atrás apenas da poluição do ar e da água. A principal causa de ruídos nas grandes cidades é o trânsito.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os motores elétricos apresentam grande potencial para reduzir a poluição sonora nas metrópoles, por serem mais silenciosos do que os de combustão interna.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="3-aspectos-econ-micos"><strong>3. Aspectos econômicos</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Os ônibus elétricos ainda são mais caros do que os similares a combustão, porém os custos de energia e manutenção são inferiores quando se analisa o gasto com a operação global. Isso significa que os elétricos podem ser mais vantajosos economicamente quando considerada toda a vida útil e as despesas operacionais com energia e manutenção.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: WRI Brasil, Empresa de Pesquisa Energética, The Lancet, World Health Organization</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:48:"3 ganhos da implantação dos ônibus elétricos";s:12:"post_excerpt";s:119:"A implantação de ônibus elétricos é uma alternativa cada vez mais adequada às atuais demandas de sustentabilidade";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:60:"3-ganhos-da-implantacao-dos-onibus-eletricos-para-as-cidades";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 16:40:44";s:17:"post_modified_gmt";s:19:"2020-08-03 19:40:44";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:36:"ed61ac75-1dad-4378-a4b5-2c37e0b19517";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}