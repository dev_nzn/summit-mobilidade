r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7266";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-05 13:58:04";s:13:"post_date_gmt";s:19:"2020-08-05 16:58:04";s:12:"post_content";s:12642:"<!-- wp:paragraph -->
<p>Redesenhar as cidades com foco nas necessidades do pedestre não só transformaria a mobilidade como também mudaria profundamente a vida em sociedade no local. É o que acredita a mestre em planejamento de cidades e design urbano Letícia Leda Sabino, fundadora e coordenadora da Sampapé, organização que atua para melhorar a experiência de caminhar nas cidades brasileiras.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade 2020 será online e gratuito. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fundada em 2012, a Sampapé começou como um coletivo que promovia passeios a pé em São Paulo como forma de melhorar a relação das pessoas com a cidade. Com o tempo, o grupo ampliou suas atividades, estendeu-se como um movimento em defesa do caminhar e começou a atuar mais ativamente com o poder público para garantir o direito de acesso à população.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Uma das conquistas da Sampapé foi o Programa Paulista Aberta, que fecha uma das avenidas mais famosas e movimentadas do País para carros e ônibus nos fins de semana e feriados e a transforma em um espaço de lazer e convivência para pedestres e ciclistas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Confira uma entrevista com a especialista, que aponta como mudanças na mobilidade urbana podem resultar em mais segurança, diversidade e qualidade de vida.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Por que é necessário repensar a mobilidade para os pedestres?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Caminhar é o elo entre todas as outras formas de deslocamento, é o mais democrático e acessível, é a maneira de estarmos e de acessarmos a cidade. Mais do que uma forma de deslocamento, é um modo de alcançar direitos básicos e de socializar e viver a cidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Repensar a mobilidade tendo como foco os pedestres é garantir a todas as pessoas um direito básico, que é ter acesso a saúde, educação e outros serviços essenciais. E esse direito tem sido negado a uma grande parcela da população.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Quais são os principais desafios enfrentados pelos pedestres no Brasil?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Os desafios de caminhar nas cidades brasileiras são básicos e envolvem falta de estrutura, calçadas inadequadas, travessias inseguras, falta de lugar para parar e se sentar e ausência de elementos de conforto térmico, como fontes de água e árvores para ter sombra.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":7282,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/pasted-image-0-2020-08-05T135631.503-1-1024x684.png" alt="" class="wp-image-7282"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Outro problema é a falta do que acessar quando se está caminhando. Quantas pessoas nas grandes cidades moram a até 15 minutos de distância a pé de tudo de que precisam? Comércio, serviços, atendimento de saúde, ponto de ônibus ou estação de transporte de massa para quando precisa se deslocar para um lugar mais distante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Essa falta de acesso gera uma desigualdade enorme, porque uma parte da população utiliza automóvel, o que gera outras consequências negativas, como congestionamentos e poluição do ar. Outra parcela é dependente de um sistema de transporte público deficitário, desconfortável, lento e inseguro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os desafios de deslocamento e as dificuldades de acesso vão criando barreiras de desenvolvimento individual e social. A uma parte dos cidadãos, como mulheres e crianças de baixa renda, é negado o espaço público.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Como mudar essa realidade?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A primeira coisa é ter planejamento e saber formas de entender a cidade em que as pessoas estejam no centro dos debates e que a qualidade de vida, da existência e da convivência seja mais importante do que a produção e a velocidade. A mudança de visão consequentemente reflete maiores investimentos que trazem inúmeros benefícios, como ruas seguras e agradáveis para a convivência e o deslocamento e meios de transporte de massa que sejam seguros, eficientes e conectados ao caminhar.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/4-beneficios-de-caminhar-pela-cidade/">4 benefícios de caminhar pela cidade</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Cada cidade tem um contexto específico, mas basicamente é preciso ter mais acesso e mais espaço para se estar a pé em qualquer horário e em qualquer lugar. Para isso é necessário criar mais calçadões, alargar calçadas, criar térreos ativos com grande variedade de comércios e construir habitações sociais em zonas que já contam com infraestrutura de serviços.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>E o que está sendo feito?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Muito pouco está sendo feito. Uma grande mudança na forma de rever e repensar as cidades foi impulsionada pela Política Nacional de Mobilidade Urbana, em 2012, que exigiu que cidades com mais de 20 mil habitantes criassem seus planos de mobilidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Essa lei também oferece algumas diretrizes, e talvez uma das mais importantes seja a que diz que os modos de transporte ativos devem ter prioridade sobre os modos motorizados e que os modos coletivos motorizados têm prioridade sobre os individuais. Ter isso registrado em um plano já nos ajuda a conseguir moldar as cidades de outra forma. E isso estar definido por lei dá respaldo para a criação de várias políticas públicas que invertam um pouco a lógica atual.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/sp-incentiva-uso-bikes-para-conter-covid-19-no-transporte-publico/">SP incentiva uso de bikes para conter covid-19 no transporte público</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Um exemplo é a Paulista Aberta, uma política pública que mobilizamos e impulsionamos aqui em São Paulo e que contempla a abertura da avenida para as pessoas aos domingos e feriados. A medida tem um efeito simbólico, de criação de um imaginário de que outra cidade é possível, de entendimento da rua como um espaço público. O programa foi respaldado pela Política Nacional de Mobilidade Urbana e abriu precedente para projetos em outras cidades do País, mas infelizmente temos visto iniciativas muito tímidas e pontuais.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>O que melhoraria nas cidades brasileiras se elas fossem pensadas com foco nos pedestres?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Todas as esferas da vivência em sociedade apresentariam melhoras, como qualidade de vida, convivência, sociabilidade, sustentabilidade e acessibilidade. Nosso modelo democrático também sofreria mudanças, pois teríamos pessoas mais diversas com acesso aos espaços públicos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Haveria diminuição de desigualdade, menos mortes violentas no trânsito e mais segurança. Ocorreria uma mudança muito profunda na forma de existir nas cidades, e viveríamos de forma mais harmônica, equilibrada e justa.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Quais são os principais indicadores de caminhabilidade em uma cidade?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>O termo <em>walkability</em>, que é caminhabilidade em português, é uma forma de medir o quão caminháveis e agradáveis são as cidades, as ruas e os bairros. É uma medida muito complexa, porque não basta avaliar se uma rua tem buracos ou não; isso é muito básico.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Sampapé trabalha com algumas perspectivas, como incluir a cidadania nessa avaliação e nessa métrica. Criamos o Índice Cidadão de Caminhabilidade<em>,</em> uma análise mais humana e menos técnica que pode ser dividida em seis camadas. A primeira avalia a qualidade de calçadas e pisos; a segunda, os mobiliários urbanos e o quanto são úteis para as pessoas (bancos, bebedouros, iluminação, árvores para fazer sombra).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/por-que-elaborar-politica-de-genero-para-a-mobilidade-urbana/">Por que elaborar política de gênero para a mobilidade urbana?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Consideramos também fachadas e serviços e a interface que existe entre o espaço público e o privado. A maneira como a cidade é sinalizada e como se comunica com quem está a pé também é importante e algo muito deficitário nos municípios brasileiros.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Também avaliamos as travessias, no sentido de conectividade; e, por fim, as sensações: se o percurso tem muito ruído, é agradável aos olhos ou passa insegurança e se as pessoas se conhecem naquela vizinhança ou não.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Além dessa escala cidadã, conseguimos avaliar outros elementos por meio de grandes pesquisas e com o uso da tecnologia. Há como medir a largura das calçadas, o tamanho dos lotes, a velocidades das vias etc. Todos esses dados dão suporte e podem apontar os caminhos para melhorar as condições de caminhabilidade dos grandes centros.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Quais cidades do mundo estão dando um bom exemplo?</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7267,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_404805433.jpg" alt="" class="wp-image-7267"/><figcaption>Buenos Aires, na Argentina, tem tomado várias medidas para priorizar a circulação de pedestres nas áreas de maior densidade. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Por conta da pandemia de coronavírus, estamos vendo cidades acelerando os seus processos de mobilidade, mas é importante lembrar que esses locais já estavam com esses processos encaminhados, por isso puderam ter uma resposta tão rápida. Entre elas está Buenos Aires (Argentina), que há anos tem feito um trabalho importante de abertura de espaços para pedestres com a criação de grandes calçadões e compartilhamento de vias.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/momento-mobilidade-debates-de-como-diversificar-deslocamento/">Momento Mobilidade debate como diversificar o deslocamento</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Antes da pandemia, Londres (Inglaterra) já havia criado o conceito de Healthy Streets, ou ruas saudáveis, para o deslocamento da população a pé ou por modos de transporte ativos. Nova York (EUA) também vem transformando os seus espaços residuais em praças. O objetivo é que a população tenha acesso a comércios e serviços a menos de 10 minutos de distância a pé. O mesmo tem sido realizado em Paris (França).</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>De que maneira a população pode contribuir para esse debate?</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A única forma possível de fazer esse debate é com a população. É muito difícil mudar a mentalidade das pessoas, pois fomos criados em espaços que sempre deram prioridade para os veículos. É necessário entender que a cidade é nossa, é das pessoas, e o poder público tem o dever de acolher e traduzir os desejos e as necessidades dos cidadãos em projetos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:45:"Por que pensar a mobilidade para os pedestres";s:12:"post_excerpt";s:153:"Para Letícia Sabino, fundadora da ONG Sampapé, priorizar a circulação de pedestres é garantir o acesso a direitos básicos como saúde e educação
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:45:"por-que-pensar-a-mobilidade-para-os-pedestres";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 13:42:57";s:17:"post_modified_gmt";s:19:"2020-08-06 16:42:57";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7266";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}