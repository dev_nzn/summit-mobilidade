[�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:109;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-02-11 15:52:17";s:13:"post_date_gmt";s:19:"2020-02-11 15:52:17";s:12:"post_content";s:11792:"Desde 2015, em 11 de fevereiro é comemorado o Dia Internacional de Meninas e Mulheres na Ciência. Instituída pela Organização das Nações Unidas (ONU), a data é um marco importante porque não faltam motivos para reconhecer a importância do tema, mas também há inúmeros desafios pela frente. Segundo a Organização das Nações Unidas para a Educação, a Ciência e a Cultura (Unesco), apenas 28% de pesquisadores são mulheres.

<a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a>

A necessidade de se ter uma ciência com mais mulheres foi uma das razões que levaram a engenheira civil Haydée Svab, de 37 anos, para o campo da pesquisa. Mestre em Engenharia e Planejamento de Transportes e cofundadora da ASK-AR, empresa de consultoria em análise de dados, ela sentiu falta, desde a graduação, de uma formação mais humana e contextualizada da técnica.
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh6.googleusercontent.com/ICtsLosiQLtgg07BafKwxMhmwC5zoa4YwzBEePm3ES_4NkQ00IVgdZ9uH-Jh0CDDzS7GWeJyMyWqUboE1_f4Y5juqf9jsNdthnFryBkT0n3WR7iuGUswff8ht4ZrJj7eBZT67Ss2" class="kg-image"><figcaption>(Fonte: Divulgação/Arquivo Pessoal)</figcaption></figure>
A engenheira conta que a definição de qual profissional seguir (sua linha de pesquisa no mestrado se deu no campo de mobilidade urbana e cidades inteligentes) ocorreu após uma disciplina de Mobilidade Urbana ministrada por uma mulher. Para Haydée, esse é o segmento no qual o urbanismo, a engenharia e a tecnologia, aliados, podem melhorar a vida das pessoas nas cidades.

Confira a entrevista com a pesquisadora.

<strong>Por que é fundamental ter mulheres pesquisando e sendo porta-vozes do tema "mobilidade urbana"?</strong>

Porque a ciência não é neutra nem as ciências ditas exatas. As leis da física e as contas matemáticas são objetivas, mas sempre temos adjacentes vários modelos e interpretações possíveis. Por sermos humanos, somos forjados pelas nossas experiências, e, por mais empáticos que sejamos, uma pessoa não sabe como é ser a outra. Por isso, é importante que haja uma miríade de pontos de vista diferentes para fazer pesquisa e construir a ciência. Temos perguntas que são feitas e outras que são deixadas de lado. Há pesquisas que atraem mais financiamento e outras não.

Vou dar um exemplo: uma parte muito relevante dos deslocamentos das cidades é feita a pé (no município de São Paulo, mais de 30%), mas os modelos tradicionais de coleta de dados sub-representam essas viagens. Já se perguntou "por quê"? Mulheres e crianças são os grupos que mais andam a pé. Quais foram os grupos que, historicamente, pensaram os modelos de coleta de dados e desenharam as políticas públicas? Não foram as mulheres.
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="/content/images/2020/02/image-4.png" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
Enfim, mais do que defender que não há viés algum na ciência, acho preferível assumirmos nossos possíveis vieses e nossas limitações, que só poderão ser superados pela diversidade de quem faz pesquisa, considerando aí não apenas gênero mas também raça e cor, classe socioeconômica, entre outros. Aliado a isso, é preciso o debate transparente e respeitoso — sem isso não vamos muito longe.

<strong>Uma de suas pesquisas apontou a importância de uma cidade mais acessível para mulheres, que, em geral, acumulam tarefas profissionais e domésticas na rotina. Como a cidade deve ser para evitar essa sobrecarga?</strong>

A cidade é o testemunho concreto das relações sociais, então, para alterá-la, é preciso mudar as relações sociais que a produzem. Nesse sentido, para superar as inequidades presentes na vida das mulheres nas cidades, é fundamental alterar a divisão de tarefas e trabalhos; isto é, precisamos acabar com a naturalização do que seja "trabalho de mulher" e "trabalho de homem". E não há como fazer isso sem questionar o trabalho não remunerado e não valorizado realizado, principalmente, por mulheres no ambiente doméstico.

<a href="/mulher-transporte-publico-dificuldades/">Mulher no transporte público: dificuldades e soluções</a>

Em paralelo à igual valorização e divisão dos diversos tipos de trabalho e olhando especificamente para o espaço público urbano, uma cidade melhor para mulheres é aquela que oferece oportunidades — de trabalho, de estudo, de lazer, entre outras — de modo mais acessível. Para isso, é preciso que várias políticas se concretizem de forma integrada, entre elas uma política de mobilidade urbana que seja indutora de desenvolvimento e promotora de inclusão; uma política habitacional que otimize e democratize o uso dos recursos já disponíveis nas regiões centrais das cidades; uma política de desenvolvimento econômico que fortaleça uma rede de centralidades — lineares e polares —, respeitando e valorizando as vocações locais; uma política educacional que, entre outras iniciativas, ofereça creche com vagas, qualidade e cobertura territorial suficiente para que mães não sejam obrigadas a sair do mercado de trabalho apenas por serem mães.

<strong>Ao longo da última década, a ascensão de movimentos feministas no mundo todo ampliou a voz de mulheres em diversas áreas do conhecimento. De que maneira essa movimentação impactou o viés de novos estudos referentes à mobilidade urbana?</strong>

Acho que mais mulheres na área de mobilidade urbana têm trazido à tona questões como as diferenças nos padrões de mobilidade; e, indo além, existem pesquisas surgindo na área de "mobilidade do cuidado", que, muito resumidamente, compreende aqueles deslocamentos que alguém faz tendo o cuidado com outras pessoas como motivo.
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/ICVR6W-LZQlOpEnypCbuIxaIlvfuBDiKuaDGbURhTV4GJf1EoqkNhcVpE_LK6gEdiQsSPWHfe7WwTtyWbeIyl53tOV_arpNxBVg7qqqo7ISAQQQqQPsPPsJ338QgZKkmIurHpm9f" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
Uma mãe que leva o filho à escola, um pai que busca a filha no médico, um sobrinho que acompanha a tia ao mercado, uma mulher que vai ao mercado fazer compras para a casa ou família: todos esses são deslocamentos "de cuidado". Devido ao papel social que lhes é atribuído, as mulheres são as que mais fazem deslocamentos desse tipo, e essa abordagem difere da abordagem clássica de "motivos de viagem" (casa, trabalho, escola, lazer, compras etc.).

<a href="/preconceito-contra-mulheres-no-transito-e-entrave-a-mobilidade/">Preconceito contra mulheres no trânsito é entrave à mobilidade</a>

A segurança nos transportes também é um tema recorrente, em que têm sido feitas investigações valiosas. Mulheres e homens (cis e trans) percebem e valorizam atributos de segurança pessoal e pública de maneira diversa. Portanto, não apenas tornar um espaço efetivamente mais seguro, mas também que também pareça mais seguro para as pessoas implica, sem dúvidas, considerar as diferentes experiências e demandas; logo, não há solução simples para cidades cada vez mais complexas e heterogêneas.

Outro impacto que enxergo é a ascensão dos temas ligados à mobilidade ativa e sustentável. Estudos indicam que, em geral, as mulheres são mais sensíveis às pautas ambientais e estão mais dispostas a mudanças de comportamento para a preservação do meio ambiente. Esses tipos de linhas de pesquisa e estudo podem servir de base para que o planejamento das cidades passe a considerar os comportamentos e as necessidades específicas das mulheres.

<strong>Como foi a experiência de ajudar a fundar um núcleo de estudos de gênero na Poli, uma instituição tradicionalmente ocupada por homens?</strong>

Foi uma experiência desafiadora e recompensadora. No início, muitos colegas perguntavam (e ainda perguntam) por que seria necessário um grupo com essa tônica na engenharia. Para mim, a necessidade era evidente quando eu percebia a ausência de mulheres em alguns grupos de trabalho de que eu fazia parte ou a baixíssima porcentagem de professoras em certos departamentos da faculdade.

Queria entender por que isso ocorria e o discurso (nem sempre) velado de que a justificativa tinha a ver com "aptidões naturais masculinas e femininas" tinha duvidoso lastro científico e não me convencia. Hoje, já não frequento o grupo, mas o acompanho de longe e apoio pontualmente, e percebo que a receptividade aumentou e a sua existência já é legitimada frente às várias demandas que as mulheres enfrentam.

<strong>Na sua opinião, quais são os caminhos possíveis para que mais mulheres ocupem espaços no campo das ciências exatas?</strong>

Diversos estudos apontam que meninas e meninos se interessam igualmente por STEM (Science, Technology, Engineering e Mathematics) até a adolescência, que é quando a curiosidade de muitas meninas "desaparece". Parece que, nesse momento do desenvolvimento, as meninas passam a pesar mais os estereótipos e papéis de gênero, bem como a preocupação com a conformidade em relação às expectativas sociais.

<a href="/importancia-calcadas-para-planejamento-urbano/">A importância das calçadas para o planejamento urbano</a>

Então, é possível criar ações para estudantes do Ensino Fundamental II e do Ensino Médio, como desconstruir estereótipos de gênero por meio de exemplos concretos, como ter professoras de Matemática, Física, Química no ambiente escolar e que sirvam de inspiração. Ou, ainda, promover nas escolas palestras com mulheres profissionais que atuem nas áreas majoritariamente masculinas, como engenharias, economia, mercado financeiro, política etc.
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/zXsPxWj_VdV4ZMYViMivg9ckuOAqUiRRP6Koj9cNYFatJRdM36zow8L0PhH28VYfomGemdlUtrATKTOriIy1klAtCpehCOpdBLn8GcEruTXfCFdyZZGLqjRgfBywONlKnRiRgj5g" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
Também oferecer oportunidades iguais no dia a dia do aprendizado. Por exemplo, estimular a formação de grupos mistos e equilibrados em projetos de ciências e cuidar para que não haja a clássica "divisão sexual do trabalho" (como serem sempre as meninas as responsáveis pelo relatório ou design do trabalho em grupo), mostrando ser possível dividir e rotacionar as tarefas entre todos os membros dos grupos.

Para estudantes de cursos técnicos e de nível superior, além das iniciativas levantadas, é possível oferecer programas estruturados de mentorias em que mulheres mais experientes (ex-alunas do estabelecimento de ensino ou da mesma carreira pretendida) guiam e oferecem apoio no desenvolvimento intelectual das mais jovens. Todas essas iniciativas também se aplicam ao mercado de trabalho, com as devidas adaptações.

No mercado de trabalho, muitas das condições necessárias para que as mulheres permaneçam nas suas áreas são as mesmas de outros campos: remuneração igual para a mesma função, não penalização das mulheres por (potencialmente) serem mães, avaliação por mérito da mesma forma que os seus pares homens e ambiente de trabalho livre de assédio moral e sexual. Enfim, é possível pensar em diferentes ações de acordo com a etapa da vida, para que se possa ter e reter mais mulheres nas ciências exatas.

<a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=texto&amp;utm_campaign=summitmobilidade19&amp;utm_content=::::&amp;utm_term=::::">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a>";s:10:"post_title";s:65:"Mulheres na Pesquisa Científica e o Desafio da Mobilidade Urbana";s:12:"post_excerpt";s:169:"No Dia Internacional de Meninas e Mulheres na Ciência, o Summit Mobilidade Urbana entrevista a pesquisadora Haydée Svab, que fala sobre os desafios de gênero na área";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:64:"mulheres-na-pesquisa-cientifica-e-o-desafio-da-mobilidade-urbana";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-02 00:12:55";s:17:"post_modified_gmt";s:19:"2020-08-02 03:12:55";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:36:"199d8381-b1c8-486d-ba76-f56fa6acc9be";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}