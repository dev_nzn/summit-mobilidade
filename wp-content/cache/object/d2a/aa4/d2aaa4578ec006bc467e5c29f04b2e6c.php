r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"469";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-07-27 04:42:29";s:13:"post_date_gmt";s:19:"2020-07-27 07:42:29";s:12:"post_content";s:6986:"<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:image {"align":"center","id":7149,"sizeSlug":"large"} -->
<div class="wp-block-image"><figure class="aligncenter size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/07/stock-photo-burning-crop-on-countryside-farm-and-ecosystems-crisis-toxic-haze-from-dry-1749344726-7-1024x683.jpg" alt="" class="wp-image-7149"/><figcaption>uhuhuhuh</figcaption></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Sabe-se que a China alia história e modernidade. Porém, no caso da ferrovia Beijing-Zhangjiakou, isso pode ser mais literal do que se pensa: os antigos trilhos, construídos e inaugurados na primeira década do século passado, passarão a receber trens de alta velocidade (AV) que unem as duas cidades-sede das Olimpíadas de 2022.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade 2020 será online e gratuito. Inscreva-se agora</a><a href="https://bit.ly/2OoAy6M">.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Essa foi a primeira ferrovia construída na China por Zhan Tianyou, importante engenheiro da história do país, em um período no qual ingleses ocupavam o território chinês buscando controlar o mercado do ópio. Por isso, tinha um caráter estratégico.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/#Programacao">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O trajeto tinha muita importância também porque Zhangjiakou era a última cidade chinesa antes da ferrovia entrar em terras mongóis, que fazem divisa ao norte com a Rússia e ao sul com a China — uma combinação perigosa, considerando as revoluções comunistas na Rússia (1917) e na China do fim da década de 1940.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Atualmente, o trajeto Beijing-Zhangjiakou já admite trens que atingem 350 quilômetros por hora, dez vezes mais do que seus "avós". Contudo, o governo chinês pretende implementar veículos ainda mais rápidos para facilitar esse trajeto e que sediarão as Olimpíadas de Inverno de 2022.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="caminho"><strong>Caminho</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"className":"kg-card kg-image-card"} -->
<figure class="wp-block-image kg-card kg-image-card"><img src="https://lh5.googleusercontent.com/WLR1LV-DXN4cQKVs2Pk60XD1R90Zm7Av6j3L8_VmINab2km9hRAKrJ5lc7HZuZtuwVmHDJOFM6_SHz8PN3LIlT2LyTCUE91_zh4LnlMiINhFj0JGBSFeGXRjD9YKuvUOTE3pdoxj" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>China pretende implementar trens de alta velocidade na ferrovia Beijing-Zhangjiakou que superem a marca atual de 350 km/h. (Fonte: Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A ferrovia tem, hoje, dez estações ao longo dos seus 174 quilômetros. O plano do governo chinês é ampliá-la a partir da conexão com o metrô da capital do país, de forma a integrar os modais e permitir que a linha férrea seja usada por chineses e turistas para se locomoverem com rapidez durante os jogos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="/por-que-elaborar-politica-de-genero-para-a-mobilidade-urbana/">Por que elaborar política de gênero para a mobilidade urbana?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mais que fornecer mobilidade, o governo chinês deseja vender uma imagem de inovação que credencie o país aos olhos do mundo, sendo um novo polo de soluções inteligentes e de modernidade. Isso se torna especialmente importante diante da pandemia do novo coronavírus, cujos efeitos econômicos desafiam o longo ciclo de crescimento do PIB chinês.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Junto a tecnologias de informação e conectividade, como o 5G, as soluções em transporte são pontos em que a China vem alcançando excelência — de fato, uma necessidade para um país com mais de 1 bilhão de pessoas e cidades com milhões de habitantes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/#Programacao">Enfrentar desigualdades sócio-espaciais? Participe desse debate no Summit Mobilidade Urbana 2020. É online e gratuito.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No que se refere ao modal sobre trilhos, a China já possui mais de 120 mil quilômetros de ferrovias, que atingem a maior parte das cidades com mais de um milhão de habitantes. Além disso, cerca de 20% da malha ferroviária já são adaptados a veículos de alta velocidade, conhecidos popularmente como trens-bala.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="olimp-adas"><strong>Olimpíadas</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"className":"kg-card kg-image-card"} -->
<figure class="wp-block-image kg-card kg-image-card"><img src="https://lh5.googleusercontent.com/fxVBK8Cli_val4qXNH3FgUe6I5ieL1TUKQB6XZhsq0Y_cBHathkmAaLF16kU2uTEXGEZZHVKzAH3gKvu6YDESC4TOK5jJa1_R0ZIxoUKyMIVH8BR_aJN2drvixFvFsY12l4IG6Ll" alt=""/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Mudanças na estrutura de mobilidade fazem parte do planejamento do país para as Olimpíadas de Inverno de 2022. (Fonte: Mirko Kuzmanovic / Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A escolha de Pequim(ou Beijing, no idioma local) é bastante óbvia: trata-se da capital do país e sede do governo. Além de ter recebido as Olimpíadas de Verão em 2008, possui um dos melhores parques esportivos do mundo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="/como-a-cycle-superhighways-se-tornaram-modelo-de-mobilidade/">Como a Cycle Superhighways se tornaram modelo de mobilidade?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Com mais de 20 milhões de habitantes, a cidade é também um dos maiores destinos turísticos da Ásia e é sede de diversos clubes e equipes esportivas.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Já Zhangjiakou tem menos de 1 milhão de habitantes (uma cidade de pequeno porte para os padrões chineses) e deve se tornar uma referência importante para diversas modalidades esportivas após o fim do evento — uma vez que a estrutura exigida para os jogos olímpicos apresenta excelência.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:group -->
<div class="wp-block-group"><div class="wp-block-group__inner-container"><!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph --></div></div>
<!-- /wp:group -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:67:"Olimpíadas de Inverno: China terá trem de alta velocidade em 2022";s:12:"post_excerpt";s:145:"Ferrovia centenária da China está em obras para receber trens de alta velocidade até 2022, quando o país receberá as Olimpíadas de Inverno.";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:64:"olimpiadas-de-inverno-china-tera-trem-de-alta-velocidade-em-2022";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 16:54:57";s:17:"post_modified_gmt";s:19:"2020-08-03 19:54:57";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:36:"9bee2e99-a791-4241-935c-a3175b829f87";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}