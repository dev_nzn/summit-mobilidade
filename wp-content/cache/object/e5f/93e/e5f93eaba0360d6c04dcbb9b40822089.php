}�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6257;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-07-31 16:21:34";s:13:"post_date_gmt";s:19:"2020-07-31 19:21:34";s:12:"post_content";s:7011:"<!-- wp:paragraph -->
<p>Uma novidade em desenvolvimento pela Uber tende a revolucionar a mobilidade urbana: o Uber Air, com lançamento previsto para 2023, promete se incorporar ao rol de serviços da companhia e garantir ótima relação custo-benefício para táxi aéreo. Assim como os helicópteros, os veículos usados terão decolagem vertical e serão pensados para pequenos deslocamentos cotidianos, com pouco menos de 100 quilômetros de autonomia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.sympla.com.br/summit-mobilidade-urbana-2020__818066">O Summit Mobilidade Urbana 2020 será gratuito e online. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O abastecimento das aeronaves será por eletricidade, que gera poluição ambiental e auditiva próxima de zero. Há previsão de implementação desses veículos em diversas cidades do mundo, e o Brasil está na lista para receber os testes, sendo São Paulo e Rio de Janeiro as candidatas mais prováveis.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/uber-cria-nova-tecnologia-para-carros-autonomos/">Uber cria nova tecnologia para carros autônomos</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As experiências podem começar ainda neste ano, e a Embraer já foi contratada pela Uber para desenvolver um veículo elétrico de pouso e decolagem vertical (eVTOL). Como os modelos estão sendo construídos de forma regionalizada (diversas empresas estão desenvolvendo protótipos similares no mundo), é possível que haja pequenas adaptações em cada projeto, mas as tendências gerais devem ser as mesmas, como velocidade, capacidade de passageiros e cuidados de segurança.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Operação</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><em> </em></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong><img src="https://lh3.googleusercontent.com/Z_rDwa7_eYXkAPVXCAlHyyLeg8QQIiDJLu8CNSlXP__34C8LK-wOnLwTtpXdZxihS5s8fZ9yVGWhwxuhiuf-QxAKXdrg5sDvXCFFIWTEYlfrj2ixr1amIgcGeM6GDxmWYeTaE1my" width="558" height="372"></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Protótipo do veículo que operará no Uber Air. (Fonte: Uber/divulgação)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A companhia estima que, inicialmente, os carros voadores devam funcionar com quatro vagas e um motorista, que estará apto para resolver possíveis problemas e oferecer segurança aos passageiros enquanto a oferta ganha credibilidade. Na sequência, porém, o lugar ocupado por ele será transformado em uma quinta vaga para cliente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Embora a ideia possa assustar a princípio, a engenharia ligada aos carros autônomos tem como principal desafio a previsibilidade do comportamento dos demais integrantes do trânsito: o movimento de carros, pedestres, semáforos e outras variáveis tornam especialmente complexa a programação do veículo autônomo em terra, o que no ar pode ser bastante simplificado.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/">Como diversificar opções de deslocamento? Inscreva-se para esse debate gratuito e online que integra o Summit Mobilidade Urbana.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os veículos serão elétricos e terão autonomia de 96,5 quilômetros, pensados para pequenos trajetos, por isso não têm espaço para malas muito grandes. Se a capacidade não impressiona, a velocidade, sim: é previsto que eles transitem entre 240 quilômetros por hora e 320 quilômetros por hora, o que permite uma viagem de 30 quilômetros, entre um aeroporto e uma planta industrial, por exemplo, em até seis minutos. O mesmo trajeto de carro, sob trânsito moderado, levaria cerca de uma hora.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Além disso, devem funcionar a partir de Skyports, bases para os veículos aéreos. A ideia é que esses eVTOLs partam desses pontos, nos quais serão recarregados entre uma corrida e outra. Segundo a companhia, os espaços devem ficar próximos de locais com grande potencial de demanda, como centros esportivos, econômicos e industriais.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Tráfego aéreo</strong></h2>
<!-- /wp:heading -->

<!-- wp:core-embed/youtube {"url":"https://www.youtube.com/watch?v=mUEj6EnBzks#action=share","type":"video","providerNameSlug":"youtube","className":"wp-embed-aspect-16-9 wp-has-aspect-ratio"} -->
<figure class="wp-block-embed-youtube wp-block-embed is-type-video is-provider-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper">
https://www.youtube.com/watch?v=mUEj6EnBzks#action=share
</div></figure>
<!-- /wp:core-embed/youtube -->

<!-- wp:paragraph -->
<p>Vídeo desenvolvido pela empresa permite ver o funcionamento do eVTOL. (Fonte: Uber/Divulgação)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Ainda há uma série de questões em aberto em relação ao tráfego aéreo, sobretudo ao considerar que São Paulo e Rio de Janeiro têm aeroportos dispostos na área urbana (Congonhas e Santos Dumont). De acordo com a aproximação do calendário de testes, ainda não anunciado em definitivo pela Uber, aspectos operacionais serão apresentados em mais detalhes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O céu nova-iorquino é o mais movimentado de todas as cidades do mundo. Além de carros, bicicletas, ônibus, metrôs e os tradicionais táxis amarelos, a mobilidade da cidade conta com pequenos aviões e helicópteros, incluindo o Uber Copter, serviço da plataforma que permite o agendamento de corridas, tal como já é tradicional nos carros.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/empresa-paranaense-cria-primeiro-carro-autonomo-do-brasil/">Empresa paranaense cria primeiro carro autônomo do Brasil</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A atividade entrou em teste em São Paulo em 2016, mas o Uber Air tende a se consolidar com mais facilidade. Além de não depender de heliportos, os preços podem ser mais acessíveis que o do serviço de helicópteros.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O Uber Copter pode transportar até cinco passageiros, com preços que variam de acordo com a demanda e a distância, assim como ocorre com os automóveis. No entanto, o custo de aplicação é alto, razão pela qual o Uber Air, que não fica atrás em capacidade e velocidade, pode se destacar como um intermediário eficiente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:43:"Conheça o Uber Air, o carro voador da Uber";s:12:"post_excerpt";s:106:"Ainda em construção, a Linha 9 do metrô de Barcelona terá 47 quilômetros de extensão e 52 estações";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:43:"conheca-o-uber-air-o-carro-voador-da-uber-5";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 16:21:37";s:17:"post_modified_gmt";s:19:"2020-07-31 19:21:37";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:35:"summitmobilidade.nznvaas.io/?p=6257";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}