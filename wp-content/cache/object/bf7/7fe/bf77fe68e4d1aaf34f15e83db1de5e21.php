H�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7295";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-07 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-07 13:00:00";s:12:"post_content";s:4846:"<!-- wp:paragraph -->
<p>As bicicletas estão sendo importantes aliadas no enfrentamento da pandemia de covid-19, pois o veículo permite a mobilidade urbana com distanciamento físico, minimizando a exposição ao coronavírus. Em virtude disso, diversas cidades pelo mundo passaram a criar ciclovias temporárias (ou ciclovias pop-ups).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana será online e gratuito. Inscreva-se agora!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Bogotá, capital da Colômbia, ampliou a operação de 117 quilômetros de ciclovias de lazer, que existiam somente aos domingos, para todos os dias da semana. Paris, Berlim, Barcelona, Milão, Londres, Buenos Aires e Lima também adotam medidas para estimular o uso da bicicleta como uma forma segura de se deslocar durante e após a crise sanitária.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No Brasil, Belo Horizonte pretende implementar um corredor de 30 quilômetros exclusivo para ciclistas depois do fim da quarentena; os testes com marcações em pintura nas vias já foram iniciados. Rio de Janeiro e Fortaleza também devem adotar medidas de expansão do uso da bicicleta.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Ciclovias temporárias</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7297,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/13091616451094-1024x768.jpg" alt="" class="wp-image-7297"/><figcaption>Ciclovias pop-up têm baixo custo, com cones e sinalização nas faixas das vias que estão subutilizadas pelos carros. (Fonte: Prefeitura de Leceister)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>As ciclovias temporárias são uma estratégia de urbanismo tático, estruturadas com baixo custo e alto impacto. Um espaço das faixas de carros é separado por cones ou blocos de concreto para dar mais <a href="about:blank">segurança</a> a ciclistas. Devido à rápida implementação, várias cidades estão adotando o modelo para testar transformações permanentes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O momento é perfeito para esses experimentos. Com a diminuição da circulação de veículos, abre-se espaço para as ciclovias. A preocupação não é somente com distanciamento físico, poluição ou congestionamento. A estratégia também é uma forma de inclusão social, uma vez que a bicicleta é um veículo mais acessível para a maioria da população.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/qual-e-a-distancia-segura-para-pedalar-e-evitar-o-coronavirus/">Qual a distância segura para pedalar e evitar o coronavírus?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As medidas têm como objetivo, ainda, permitir que as pessoas andem e liberem espaço em transportes públicos e estradas. Além disso, ajuda a comunidade a retornar ao trabalho e às empresas com segurança, incentivando o comércio local e apoiando a recuperação econômica nas cidades.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Estratégia de mobilidade urbana</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7296,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/a-13122832495167.jpg" alt="" class="wp-image-7296"/><figcaption>Ciclovias temporárias fazem parte da estratégia de resiliência urbana contra a crise provocada pela pandemia de coronavírus. (Fonte: Prefeitura de Leicester)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Em grandes desafios, como terremotos e condições meteorológicas severas, a bicicleta já demonstrou a sua importância como resposta rápida para o deslocamento de equipes de emergência, por exemplo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A ciclomobilidade deve ser vista uma forma de proporcionar uma transição ecológica para a sociedade. O uso de bicicletas favorece à diminuição da população e a melhoria da qualidade do ar, especialmente à medida que novas pesquisas estabelecem ligações entre o ar sujo e as taxas de mortalidade por doenças como a covid-19.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Capital Mundial da Arquitetura, Jornal Público, Forbes, Cidade de Sydney, The New York Times, Prefeitura de Leicester.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:65:"Ciclovias temporárias viram tendência global durante a pandemia";s:12:"post_excerpt";s:109:"Também chamadas de ciclovias pop-up, as ciclovias temporárias são alternativas sustentáveis de mobilidade";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:63:"ciclovias-temporarias-viram-tendencia-global-durante-a-pandemia";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 18:10:34";s:17:"post_modified_gmt";s:19:"2020-08-06 21:10:34";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7295";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}