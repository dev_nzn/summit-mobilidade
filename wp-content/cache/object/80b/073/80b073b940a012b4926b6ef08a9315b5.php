r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7174;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-04 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-04 13:00:00";s:12:"post_content";s:6440:"<!-- wp:paragraph -->
<p>A Organização Mundial da Saúde (OMS) reconheceu, recentemente, a possibilidade de transmissão do novo coronavírus por meio do ar, por isso tecnologias de purificação associadas a sistemas de ventilação podem ser bem-vindas para reduzir a disseminação da covid-19 em meio à retomada de atividades e serviços de transporte coletivo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Foi nesse sentido que a maior operadora espanhola de transporte público, a Transports Metropolitans de Barcelona (TMB), desenvolveu um programa piloto para garantir a segurança sanitária de seus passageiros. A companhia administra oito linhas metroviárias e 156 estações do território da Catalunha.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Um sistema chamado Respira, baseado em inteligência artificial (IA), é capaz de aumentar o ar fresco dentro das instalações do sistema metroviário, além de controlar, de forma automatizada e preditiva, a umidade e a temperatura para dificultar a disseminação de microrganismos no sistema de transporte.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Transformação digital</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>A introdução da tecnologia ressalta a importância da aplicação de soluções tecnológicas na mobilidade urbana. A TMB tem 25% de sua frota automatizada, incluindo metrôs e ônibus urbanos. A implantação do Respira ampliou essa transformação para um dos sistemas básicos da infraestrutura de <a href="https://summitmobilidade.estadao.com.br/barcelona-tera-linha-de-metro-subterranea-mais-longa-da-europa-2/">metrô de Barcelona</a>, segundo comunicado da companhia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/quanto-os-acidentes-de-transito-cairam-durante-a-quarentena/">Quanto os acidentes de trânsito caíram durante a quarentena?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A solução, segundo a TMB, demonstrou as possibilidades de controle dinâmico e inteligente e ressaltou a importância do bom funcionamento dos equipamentos do sistema. A tecnologia permite a renovação do ar, enviando instruções individuais de velocidade para os ventiladores de acordo com a hora e o dia.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Inteligência artificial</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7176,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/11130119527196-1024x576.png" alt="" class="wp-image-7176"/><figcaption>Toda a ventilação é controlada por meio de um sistema automatizado. (Fonte: Transports Metropolitans Barcelona/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O sistema Respira é baseado em uma plataforma de IA capaz de reduzir o índice de calor de passageiros e trabalhadores do metrô, definindo vários critérios e colhendo informações em tempo real. Isso inclui temperatura, umidade e qualidade do ar interno nas estações, além de consumo de eletricidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Um algoritmo dinâmico interpreta os dados coletados para prever as condições ambientais nas estações de metrô, de acordo com a previsão do tempo e os níveis de ocupação esperados. A partir disso, dispara comandos para operar cada ventilador no intuito de aumentar a quantidade de ar fresco vindo do lado de fora, o que reduz o risco de espalhar microrganismos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dessa maneira, as viagens no metrô de Barcelona se tornarão mais confortáveis, além de mais seguras sanitariamente. Isso afeta diretamente a experiência dos passageiros, que, gradualmente, devem voltar a utilizar os serviços de transporte, mas ainda preocupados em adotar medidas de prevenção aprendidas durante a pandemia.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Programa piloto</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7175,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1722851428-1-1024x684.jpg" alt="" class="wp-image-7175"/><figcaption>Aos poucos, a população de Barcelona volta a se deslocar pela cidade e a utilizar os serviços de transporte público. (Fonte:&nbsp; Victoria Labadie / Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O tráfego de pessoas em Barcelona está aumentando semana a semana e já retomou 86% dos deslocamentos. No transporte público, esse número chega a 40%. Com isso, a implantação da tecnologia de purificação e controle do ar vem sendo ampliada na cidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/como-ficam-metros-e-onibus-apos-a-quarentena/">Como ficam metrôs e ônibus após a quarentena?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O sistema RESPIRA começou a ser instalado na Linha 1 do metrô de Barcelona. Durante os primeiros testes, o programa piloto foi capaz de reduzir a temperatura do sistema de transporte em 1,2 °C quando comparado a anos anteriores. O resultado motivou a TMB a ampliar a utilização da tecnologia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A partir de julho, a purificação do ar será iniciada nas linhas de 1 a 5 do metrô, que transportam 94% dos passageiros que circulam pelo sistema metropolitano catalão. O Ministério de Transportes, Mobilidade e Agenda Urbana da Espanha acompanha de perto a execução da iniciativa para avaliar a expansão para outros locais do país.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: The Major, El Periódico, Cities Today</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:65:"Barcelona testa sistema de ventilação contra covid-19 no metrô";s:12:"post_excerpt";s:138:"Um programa piloto de ventilação baseado em inteligência artificial no metrô deve ajudar no combate à disseminação do coronavírus
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:62:"barcelona-testa-sistema-de-ventilacao-contra-covid-19-no-metro";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:21:36";s:17:"post_modified_gmt";s:19:"2020-08-03 21:21:36";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7174";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}