G�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7285;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-07 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-07 13:00:00";s:12:"post_content";s:6752:"<!-- wp:paragraph -->
<p>Com a redução da circulação de veículos durante o período de quarentena, <a href="https://summitmobilidade.estadao.com.br/quanto-os-acidentes-de-transito-cairam-durante-a-quarentena/">o número de acidentes e vítimas de trânsito despencou em várias cidades brasileiras</a>. Alguns municípios chegaram a registrar queda de 50% no período.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A diminuição do número de vítimas de trânsito ajudou a frear a pressão sobre o Sistema Único de Saúde. Por ano, o tratamento de pacientes vindos de acidentes nas ruas representa a ocupação de 231 mil leitos, com custo de R$ 52 bilhões, de acordo com dados da Fundação Oswaldo Cruz (Fiocruz).&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Com as ruas vazias, as cidades têm atuado para remodelar seus sistemas de mobilidade urbana, promovendo uma mobilidade mais ativa e sustentável.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Como reduzir acidentes</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>As fatalidades causadas por acidentes de trânsito podem ser evitadas por meio de políticas de mobilidade que coloquem a vida humana em primeiro plano. Uma série de ações estratégicas devem ser adotadas para proteger motociclistas, ciclistas e pedestres, vítimas de metade de todos os acidentes fatais de trânsito.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Medidas integradas de um sistema de mobilidade seguro têm potencial de reduzir os riscos de ocorrências trágicas, ao diminuir a frequência e a distância de deslocamentos. O relatório <em>Sustentável e Seguro</em>, lançado pelo WRI Ross Center for Sustainable Cities<em> </em>e pelo Banco Mundial, apresenta algumas medidas que podem ser adotadas pelas cidades para reduzir os acidentes de trânsito.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Redução de limites de velocidade</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7287,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_153710405-1-1024x683.jpg" alt="" class="wp-image-7287"/><figcaption>Aumento de velocidade tem relação direta com crescimento de fatalidades no trânsito. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>A velocidade é um dos principais agravantes das fatalidades causadas por acidentes de trânsito. A redução dos limites de velocidade em uma via afeta pouco o tempo dos deslocamentos urbanos, mas tem um impacto direto na redução das vítimas. Um estudo sueco mostra que a cada 1% de aumento de velocidade, os acidentes fatais crescem 4%.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Velocidades mais baixas aumentam o tempo de resposta dos motoristas a eventos inesperados que podem causar colisões. Ao mesmo tempo, a velocidade reduzida torna mais fácil para transeuntes, como pedestres e ciclistas, a observação da movimentação dos automóveis.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Redesenho das ruas</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>O desenho das ruas, com calçadas mais largas, vias mais estreitas para os carros e espaços exclusivos para ciclistas, ajuda a separar pessoas vulneráveis dos veículos e tornar as vias mais adequadas a todos. <a href="https://summitmobilidade.estadao.com.br/3-transformacoes-urbanas-decorrentes-da-covid-19/">Essas ações podem ser adotadas de forma rápida e barata, por meio do urbanismo tático</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Além disso, faixas de pedestres elevadas e lombadas são eficazes para deixar o trânsito mais seguro, em especial perto de zonas residenciais, escolas e hospitais. Essas ações também desincentivam os motoristas a acelerarem, diminuindo a possibilidade de infortúnios.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Mobilidade integrada</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7286,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_189007922-1-1024x682.jpg" alt="" class="wp-image-7286"/><figcaption>Sistemas de BRT são capazes de reduzir o número de acidentes nas ruas. (Fonte:  T photography/ Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>O transporte coletivo de alta qualidade é mais seguro que se locomover de carro. Em regiões planejadas com orientação para o transporte coletivo, a taxa de mortes em acidentes de trânsito é cerca de um quinto menor do que a daquelas planejadas para o carro. <a href="https://summitmobilidade.estadao.com.br/vlt-ou-brt-qual-e-a-melhor-opcao/">Sistemas eficientes de <em>Bus Rapid Transport (BRT),</em> por exemplo, podem reduzir até pela metade os acidentes de trânsito</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/italia-oferece-incentivo-para-compra-de-bikes-durante-pandemia/">Itália oferece incentivo para compra de bikes durante pandemia</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Diversas opções de mobilidade devem ser integradas para desincentivar o uso dos veículos privados. Políticas de cobrança para a circulação de automóveis e a restrição de estacionamento nos espaços urbanos devem ser realizadas, enquanto são abertas novas ciclovias e linhas de transporte público de forma integrada.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Educação é importante</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>As campanhas de alerta para perigos e fatalidades do trânsito devem ser constantes. No entanto, as medidas de educação no trânsito precisam ser acompanhadas de fiscalização rigorosa e atividades de conscientização. As ações podem alcançar desde alunos de escolas até profissionais com interface direta com as ruas, como urbanistas, engenheiros, profissionais de saúde e policiais.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: WRI Brasil, The City Fix, Lundo Institute of Technology</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:66:"Como manter a queda nos acidentes de trânsito após a quarentena?";s:12:"post_excerpt";s:82:"Menor índice de acidentes pode ser mantido com estratégias de urbanismo tático
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:63:"como-manter-a-queda-nos-acidentes-de-transito-apos-a-quarentena";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 18:04:56";s:17:"post_modified_gmt";s:19:"2020-08-06 21:04:56";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7285";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}