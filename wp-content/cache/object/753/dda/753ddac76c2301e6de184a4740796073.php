r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7168";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-03 18:13:41";s:13:"post_date_gmt";s:19:"2020-08-03 21:13:41";s:12:"post_content";s:6274:"<!-- wp:paragraph -->
<p>Em meio a uma cultura que ainda impõe sobre as mulheres mais responsabilidades no cuidado com os filhos do que aospais, as mães costumam enfrentar uma árdua jornada, e esse cenário precisa ser levado em conta no planejamento das cidades.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O estudo <em>Evolução dos padrões de deslocamento na Região Metropolitana de São Paulo </em>mostra que a presença de crianças entre cinco e nove anos impacta os padrões de mobilidade das mulheres. As mães, além do estudo e trabalho, acabam assumindo diversas tarefas, como levar os filhos à escola, à creche e ao médico ou levá-las junto quando vão ao supermercado e à farmácia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A desigualdade fica mais evidente quando se observa a diferença de utilização dos modais entre os gêneros. No Brasil, a maioria dos espaços urbanos foram planejados para o melhor uso de carros – modal mais utilizado pelos homens.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/mulheres-na-pesquisa-cientifica-e-o-desafio-da-mobilidade-urbana/">Mulheres na Pesquisa Científica e o Desafio da Mobilidade Urbana</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por outro lado, a maioria das pessoas que se desloca a pé, de ônibus e metrô é do sexo feminino. Em São Paulo, por exemplo, em torno de 62% de quem circula a pé e 74% de quem utiliza o transporte coletivo são mulheres, segundo uma pesquisa do metrô paulista.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Problemas cotidianos</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7170,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1468718807-1-1024x605.jpg" alt="" class="wp-image-7170"/><figcaption>Mulheres são as principais usuárias do transporte coletivo. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Além de mulheres serem as principais usuárias do transporte coletivo, mães circulam mais pela cidade porque ainda são responsabilizadas por atividades da esfera do cuidado. Entretanto, raramente o planejamento da mobilidade urbana considera isso, podendo afetar o dia a dia de muitas mães. Exemplos disso são o tamanho dos degraus no transporte público, as calçadas em más condições, o tempo dos semáforos curtos, as travessias longas e a ausência de ciclovias. </p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Mobilidade urbana inclusiva</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>O documento <em>Políticas para o Desenvolvimento Orientado ao Transporte Sustentável Inclusivo — </em>lançado pelo Instituto de Políticas de Transporte e Desenvolvimento (ITPD) e pela Organização para o Meio Ambiente e Desenvolvimento da Mulher (WEDO) — apresenta recomendações para equilibrar a diferença entre os gêneros na mobilidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/mulher-transporte-publico-dificuldades/">Mulher no transporte público: dificuldades e soluções</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Segundo o documento, pesquisas devem ser realizadas para coletar dados sobre as necessidades específicas das mulheres e a liderança feminina, a qual deve ser cultivada para garantir a participação da mulher nas decisões de políticas estruturantes.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Ruas completas</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7169,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1341345131-1-1024x682.jpg" alt="" class="wp-image-7169"/><figcaption>Ciclismo deve ser incentivado para facilitar mobilidade de mulheres e crianças. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Os espaços urbanos devem ser projetados para propiciar caminhadas, ciclismo e acesso ao transporte público tanto de forma segura como integrada. Portanto, a arborização das vias é importante, e o transporte coletivo deve ser frequente, confiável e acessível. Outros modais intermediários, como táxis de bicicleta, precisam ser incluídos no planejamento.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Desenvolvimento integrado</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Uma densa rede de ruas e caminhos deve ser pensada de forma integrada com as atividades. Serviços básicos locais precisam estar a distâncias possíveis de se percorrer a pé, e o estacionamento de carros deve ser desincentivado.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Transporte inclusivo</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>O planejamento de mobilidade urbana precisa considerar que a mulher pode viajar com crianças, sacolas e compras. Dessa forma, desde o desenho dos veículos de transporte coletivo até os sistemas intermodais integrados com tarifas justas devem ser repensados.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Incentivo adequado</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Campanhas para incentivar as mulheres a usar o transporte público devem ser criadas considerando ações antiassédio tanto nas ruas quanto dentro do transporte coletivo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: The City Fix Brasil, Instituto de Políticas de Transporte e Desenvolvimento (ITPD).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:52:"Mobilidade urbana deve considerar cotidiano de mães";s:12:"post_excerpt";s:109:"Mulheres são as maiores usuárias do transporte coletivo e se dedicam a mais atividades na esfera do cuidado";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:51:"mobilidade-urbana-deve-considerar-cotidiano-de-maes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:13:43";s:17:"post_modified_gmt";s:19:"2020-08-03 21:13:43";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7168";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}