r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:3:"370";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-06-11 01:44:54";s:13:"post_date_gmt";s:19:"2020-06-11 01:44:54";s:12:"post_content";s:6374:"Nos últimos anos, diversas cidades têm empenhado esforços para se tornarem cidades inteligentes. Quando essas transformações são pensadas no âmbito da mobilidade urbana, é importante considerar quais são as estratégias de transporte e as formas de os cidadãos acessarem os espaços de maneira justa, saudável e ambientalmente correta.

<a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a>

Conheça algumas dimensões da mobilidade que podem contribuir para que o município responda às necessidades da população e caminhe no sentido de se tornar uma cidade inteligente.
<h2 id="raio-x"><strong>Raio X</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/lHoYfLwko58FNbLdsOGXOuPyBjHR6sp_Ai8ovfZE78LB2nmAhABKxqPRWtoL5189SIKy9fitnjUasdXAEIEUO3In2gRU0Ce4PYu86WKHyPELRT7CMAH9yBud78wIy5hrxScsuw6J" class="kg-image"><figcaption>Informação é matéria-prima para a tomada de decisão sobre o futuro das cidades. (Fonte: Shutterstock)</figcaption></figure>
Constituir uma cidade inteligente é mais do que colocar WiFi nas ruas ou adaptar o site da prefeitura para mobile first. Trata-se de pensar estratégias complexas, que envolvem planejamento e gestão de um arranjo de diversas variáveis. Por isso, segundo a doutora em Ciências Sociais Aspásia Camargo, que já presidiu o Instituto de Pesquisa Econômica Aplicada (Ipea) e ocupou a Secretaria Executiva do Ministério do Meio Ambiente, o primeiro passo de uma cidade inteligente deve ser conhecer a si própria. E isso vale para a mobilidade, cujas soluções estão necessariamente articuladas com outras áreas.

<a href="/micromobilidade-por-que-a-bicicleta-e-o-meio-mais-saudavel/">Micromobilidade: por que a bicicleta é o meio mais saudável?</a>

A melhor forma de ter um diagnóstico preciso é fazer um levantamento chamado Sistema de Informação Georreferenciada (SIG), que funciona como um raio X da cidade. Com isso, é possível quantificar e tabular variáveis que as impressões empíricas podem não revelar, como gargalos do sistema de transporte e fluxos mais comuns, que demandam ações rápidas.

O SIG é importante também para conhecer tecnicamente as possibilidades de intervenção. Por exemplo, para fazer um túnel ou ampliar a rede de metrô de uma região, a formação do solo e a vegetação atingida são variáveis importantes a serem consideradas. Além disso, o sistema pode apresentar os desafios da mobilidade diante de problemas ainda não percebidos. Se um local precisa de uma escola ou uma unidade básica de saúde, é necessário pensar em como as pessoas vão acessar esse novo equipamento público: há calçadas? Será necessário construir ciclovias e adaptar a rota de linhas de ônibus?

Outro exemplo se refere à articulação de regiões. Se a análise concluir que um espaço tem trabalhadores predominantemente residentes de outro, é necessário otimizar essa relação. Incentivar com isenção de impostos a transferência de empresas para o local de residência das pessoas ou criar uma linha de ônibus que conecte as regiões podem ser alternativas desenvolvidas a partir do diagnóstico desse instrumento.
<h2 id="tecnologia-social"><strong>Tecnologia social</strong></h2>
Uma vez feito o levantamento das informações que permitam conhecer o "metabolismo" da cidade, os gestores podem pensar em soluções que resolvam esses desafios. É a hora de usar a tecnologia e a disposição de inovação: as respostas de uma cidade inteligente precisam unir tecnologia, justiça social, responsabilidade ambiental e participação cidadã.

<a href="/4-beneficios-de-caminhar-pela-cidade/">4 benefícios de caminhar pela cidade</a>

Soluções <em>high tech </em>são bem-vindas, mas apenas se de fato resolverem os problemas urbanos, já que a ideia de uma cidade inteligente é torná-la funcional para todos os habitantes. No exemplo da abertura de uma nova linha de ônibus, talvez essa demanda, junto a outros fatores, justifique um sistema de ônibus com canaleta exclusiva (BRT), que tem ótima relação custo-benefício, embora sem o mesmo apelo visual de um veículo leve sobre trilhos (VLT).

Outra solução pode ser criar uma ciclovia bem estruturada, capaz de desenvolver a mudança de hábito da população e dos trabalhadores que precisam se deslocar diariamente. Mas isso deve ocorrer apenas se a distância, o tempo de deslocamento e a segurança forem pensadas em um mesmo arranjo.
<h2 id="mobilidade-centrada-nas-pessoas"><strong>Mobilidade centrada nas pessoas</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh4.googleusercontent.com/BpLuIwQ2fKFIxceccX783GXPtNeG8JVre641Grpk1vwcOX6OiRXfCaG00Zj3vXV9jtS7PoZJz21uXfK8qZSxDUIRj9J4oKKOZ-htXa-RFplTsKZJQzSg7IJL4rvDvuZClaZpOnFu" class="kg-image">

<figcaption>Soluções de mobilidade de uma cidade inteligente precisam considerar a complexidade da vida social. (Fonte: Shutterstock)</figcaption></figure>
É fundamental que a mobilidade seja centrada nas pessoas. Por isso, transformar uma cidade inteligente pode implicar o uso de tecnologia pouco complexa e a adoção de ações mais audazes política do que tecnologicamente, como o combate à gentrificação.

<a href="/como-esteiras-rolantes-podem-aprimorar-a-mobilidade-urbana/">Como esteiras rolantes podem aprimorar a mobilidade urbana?</a>

Revitalizar as praças como áreas verdes, de trânsito e convivência pode ser uma opção barata e funcional para tornar a cidade mais acolhedora e partilhada. "É preciso, antes de tudo, que a cidade inteligente seja o melhor lugar para se viver", relata Camargo.

Ainda segundo a especialista, pensar na mobilidade urbana de uma cidade inteligente implica a possibilidade de acesso a um corpo articulado de espaços e equipamentos públicos que alimentem o conhecimento. Cultura, ciência, arte, economia e inovação precisam fazer parte do cotidiano do cidadão, cuja cidade deve permitir acesso a museus, empregos, compras e exercícios, garantindo uma vida plena.

Fonte: Engenharia E, FGV, Proxxima

<a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a>";s:10:"post_title";s:47:"Como é a mobilidade de uma cidade inteligente?";s:12:"post_excerpt";s:71:"Mobilidade é fator fundamental para que uma cidade inteligente exista
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:45:"como-e-a-mobilidade-de-uma-cidade-inteligente";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 22:34:29";s:17:"post_modified_gmt";s:19:"2020-08-01 01:34:29";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:36:"24fec5da-9119-405c-8904-963e87e28a5d";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}