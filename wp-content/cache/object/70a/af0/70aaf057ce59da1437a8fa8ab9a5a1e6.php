r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7195;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-06 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-06 13:00:00";s:12:"post_content";s:7482:"<!-- wp:paragraph -->
<p>A Mercedes-Benz e a Nvidia anunciaram uma parceria para lançar uma frota de <a href="https://summitmobilidade.estadao.com.br/conheca-os-6-niveis-de-carros-autonomos/">carros autônomos</a> a partir de 2024. Em 2019, a montadora alemã vendeu 2,39 milhões de carros em todo o mundo e já possui protótipos, como S-Class S 500 Intelligent Drive, além de outros projetos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O acordo une a experiência da montadora alemã com a computação acelerada da empresa de tecnologia para desenvolver um veículo conectado à internet e com ferramentas como a <a href="https://summitmobilidade.estadao.com.br/de-cada-dez-veiculos-no-mundo-um-sera-carro-autonomo-ate-2030/">direção autônoma</a>, semelhante ao Cadillac Super Cruise, e baliza automática, como o Tesla Smart Summon.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Nvidia é conhecida principalmente por suas GPUs de jogos, mas tem parcerias com a Volkswagen e a Uber para colocar a tecnologia de Inteligência Artificial (IA) e recursos de veículos autônomos em seus carros. A empresa também lançou o primeiro sistema de direção automatizado nível 2+ disponível comercialmente no mundo.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Nvidia Drive</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"align":"center","id":7197,"width":659,"height":489,"sizeSlug":"large"} -->
<div class="wp-block-image"><figure class="aligncenter size-large is-resized"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/13064603985073.jpg" alt="Sistema da Nvidia será implementado em todos os modelos da montadora alemã. (Fonte: Mercedes-Benz/Divulgação)" class="wp-image-7197" width="659" height="489"/><figcaption>Sistema da Nvidia será implementado em todos os modelos da montadora alemã. (Fonte: Mercedes-Benz/Divulgação)</figcaption></figure></div>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>A parceria entre a Mercedes-Benz e a Nvidia existe há mais de 5 anos e também se expande em outras frentes. Em associação com a Bosch, as duas empresas planejam operar um serviço de robotaxi em San Jose, a partir do segundo semestre deste ano.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/carros-autonomos-podem-piorar-a-mobilidade-urbana/">Carros autônomos podem piorar a mobilidade urbana?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As duas empresas desenvolverão uma plataforma de chips e software a partir do sistema integrado em chip, chamado de SoC Orin, lançado pela Nvidia em dezembro de 2019. Os veículos da Mercedes também terão recursos implementados na plataforma Drive AGX da empresa de tecnologia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A montadora alemã tem sete modelos de <a href="https://summitmobilidade.estadao.com.br/4-testes-com-veiculos-autonomos-no-brasil-e-no-mundo/">carros autônomos</a> em seu portfólio e deve adicionar mais nove ainda neste ano. A arquitetura de software de IA da Nvidia Drive será utilizada em toda a frota.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Central de comando</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Ao combinar a computação de alto desempenho e a eficiência energética das plataformas Drive AGX com a experiência de mais de 1 século da Mercedes em engenharia automotiva, a arquitetura de veículos da próxima geração será mais capaz e econômica.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/como-a-pandemia-afeta-o-desenvolvimento-de-carros-eletricos/">Como a pandemia afeta o desenvolvimento de carros elétricos?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Atualmente, as funções de software do veículo são alimentadas por dezenas de unidades de controle eletrônico (ECUs) distribuídas por todo o carro. Cada unidade é especializada em uma determinada atividade. Uma ECU controla as janelas e a travas da porta, por exemplo. Outras controlam a direção hidráulica e a frenagem.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O sistema SoC centraliza a computação no carro, integrando os recursos avançados de software à medida que estiverem disponíveis, seja para experiências de usuário autônomo ou movido a IA, permitindo o controle no carro e no cockpit, desde a condução autônoma segura até os recursos inovadores de conveniência.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Pioneirismo da Mercedes-Benz</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7196,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/13064158390070.jpeg" alt="" class="wp-image-7196"/><figcaption>Museu Mercedes-Benz oferece deslocamentos dentro de seu estacionamento com um veículo sem motorista em parceria com a Bosch. (Fonte: Mercedes-Benz/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>A Mercedes possui diversos projetos e parcerias para o desenvolvimento de tecnologias de carros autônomos com empresas alemãs. Junto com a BMW e a Audi, possui a plataforma de navegação Here. Com a Bosch, oferece o serviço de estacionamento sem motorista no Museu Mercedes-Benz em Stuttgart.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Caminho pavimentado para os carros autônomos</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Atualmente, os seus veículos S CLASS possuem uma série de tecnologias que são necessárias para a automação da direção. Entre elas, estão os serviços de navegação, assistência para mudanças de rota, troca de faixas, limites de velocidade, parada de emergência e freio inteligente para evitar colisões.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/google-mostra-impacto-do-novo-coronavirus-na-mobilidade-urbana/">Google mostra impacto do novo coronavírus na mobilidade urbana</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Além disso, os veículos da montadora alemã apresentam um sistema para melhorar respostas rápidas a mudança bruscas de comportamento de pedestres e para alertar sobre mudanças de faixa, derrapamentos, semáforos e até obstáculos ainda não visíveis a olho humano durante a direção.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O S CLASS está totalmente conectado com informações sobre o tráfego em tempo real e pode emitir avisos sobre obstrução de trânsito para permitir rotas alternativas. Um serviço de assistência de estacionamento ajuda o motorista a achar uma vaga. O veículo tem, ainda, um sistema para estacionar de forma remota, permitindo o veículo entrar em pequenos espaços com segurança.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Nvidia, Mercedez-Benz, Tecmundo, Forbes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:61:"Mercedes-Benz e Nvidia se unem para lançar carros autônomos";s:12:"post_excerpt";s:94:"Empresas apostam em Inteligência Artificial para produzir carros autônomos a partir de 2024
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:59:"mercedes-benz-e-nvidia-se-unem-para-lancar-carros-autonomos";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-06 19:48:22";s:17:"post_modified_gmt";s:19:"2020-08-06 22:48:22";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7195";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}