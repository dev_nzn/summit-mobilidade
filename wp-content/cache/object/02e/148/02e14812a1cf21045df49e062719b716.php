[�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:110;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-02-11 17:05:58";s:13:"post_date_gmt";s:19:"2020-02-11 17:05:58";s:12:"post_content";s:6375:"Todos os verões, muitas cidades brasileiras sofrem com enchentes em decorrência das fortes chuvas do período. A mobilidade urbana é a primeira coisa a ser atingida pelos alagamentos, mas os problemas vão além: ferimentos, fatalidades, danos a residências e empresas são devastadores.

<a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a>

Dada a realidade das mudanças climáticas, esses desastres aumentarão, a menos que medidas proativas sejam tomadas para mitigar o problema.

A compreensão do ciclo da água é uma oportunidade para gerar uma relação positiva entre processos naturais, plantas e pessoas. As enchentes podem ser entendidas como um elemento regenerador para melhorar a vida nas áreas urbanas e deixar de ser um problema para a mobilidade. Estratégias inovadoras consideram a inundação como um processo natural com o qual trabalhar em vez de resistir. Soluções não estruturais e baseadas na natureza para adaptação às enchentes estão substituindo as tecnologias centralizadas e projetadas.

Existem muitas medidas preventivas em prática no mundo todo e que podem ser tomadas para garantir a segurança e a mobilidade nas cidades, mesmo durante chuvas torrenciais. A mudança já é visível na experimentação urbana em cidades da China, Holanda, Alemanha e dos Estados Unidos.
<h2 id="cidades-esponja">Cidades-esponja</h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh4.googleusercontent.com/-C1xw9ZNUCPrgI8fKlTGXeUUDHeKgjo9K3zjv375o2yDxv3lbTb0MvW3PeEwAT6fVQ3Xgz5Yz8NJqE7kGff0lJ5h5MbSnhNf4CscruQPpch9STRK5KKdvudFTM6cWemTmN8Yg7F8" class="kg-image"><figcaption>(Fonte: World Future Council/Reprodução)</figcaption></figure>
O conceito se tornou muito popular na China, um país que viu a taxa de inundações urbanas mais que dobrar nos últimos anos. Wuhan foi a primeira das 16 cidades a implantarem o modelo. Conhecida como "a cidade dos cem lagos", a região pavimentou os espelhos d'água e passou a sofrer constantemente com enchentes.

<a href="/grandes-projetos-de-infraestrutura-podem-transformar-cidades/">Grandes projetos de infraestrutura podem transformar cidades</a>

A cidade-esponja age como um sistema permeável que permite que a água seja filtrada pelo solo e alcance os aquíferos urbanos, facilitando a extração através de poços urbanos. Em vez de canalizar a água da chuva para longe, uma cidade-esponja a mantém para uso próprio dentro de seus próprios limites. As aplicações incluem irrigação de jardins e fazendas urbanas, substituição ou reposição da água usada para lavar os banheiros.
<h2 id="telhados-verdes">Telhados verdes</h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh4.googleusercontent.com/Vr9iAYladIE39dNNGh8fLqhpdg7qhsfw1a-_xa2Bno3b-1FMWjfINMG2-3g3KW4Lf4AZHOoPEHrtv7S9U7cUJshU2z4xvtw547WSKUSP_BWyt4cghlaBOnaFYCKcqZtwnrJ1Fk7D" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
Os telhados cobertos de vegetação, também conhecidos como telhados verdes, absorvem a água da chuva e ajudam a mitigar as inundações. Populares em toda a Europa, esses recursos foram aplicados em larga escala na cidade holandesa de Roterdã e estão presentes nos Estados Unidos e na Austrália.

<a href="/como-os-problemas-de-mobilidade-afetam-o-trabalhador-brasileiro/">Como os problemas de mobilidade afetam o trabalhador brasileiro?</a>

Durante fortes chuvas, os telhados verdes fornecem uma solução valiosa para armazenamento temporário de água, já que absorvem a precipitação, reduzindo a velocidade do escoamento da água e atrasando o pico na descarga. Como resultado, a pressão no sistema de esgoto é reduzida em épocas chuvosas.
<h2 id="pavimentos-perme-veis"><strong>Pavimentos permeáveis</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/P1dFmqp1L8FiBuKj6uoUXbn0QKqx3Cg37PuSc10W1_GkZQhmxUHlie06Vfsua96JleCHdNoUkIdtQmbBcOr282EaupTqxzXbIrEGNv65iEO7RmLj32bgb8tKEhssYmFmUsZn1Rx7" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
A cidade de Chicago, nos Estados Unidos, investiu significativamente na reinvenção do gerenciamento de águas pluviais na última década, incluindo a construção de mais de 100 "ruas verdes", com pavimento permeável que permite que a água seja filtrada e drenada para o solo.

<a href="/pesquisa-mobilidade-sao-paulo-preferencias/">Os hábitos de mobilidade em São Paulo: conheça as preferências</a>

Na rua sustentável, a água da chuva viaja pela calçada até rochas porosas, nas quais é descontaminada por micróbios. Em seguida, ela passa a alimentar as plantas vizinhas ou filtra a areia no fundo do solo para ir ao lago Michigan. Dessa forma, 80% da precipitação são desviados do sistema de esgoto, e as estradas não inundam mais.
<h2 id="restaura-o-das-plan-cies"><strong>Restauração das planícies</strong></h2>
<figure class="kg-card kg-image-card kg-card-hascaption"><img src="https://lh5.googleusercontent.com/zvYDDIE4fGyvzvua_v0G6XetPRVBkavF_lJlBBTaCCW2-crTl2khe8VhnPiYvEgXzVKTno7Uh2EvTh8sJ4gS1l2UIhKnvNtSHICEV78o9nmIUyWZSu2VSOcIJK0Uxq0oC7HqVFG-" class="kg-image">

<figcaption>(Fonte: Shutterstock)</figcaption></figure>
As planícies dos rios são os locais em que a água se acumula. Com a expansão urbana, muitos desses espaços se tornaram avenidas de vale e perderam a função de proteção, tornando-se pontos de alagamento e afetando a mobilidade urbana. Uma maneira bem ecológica de diminuir o problema é plantar vegetações nas margens dos rios que passam pelas cidades, pois, diferentemente do asfalto, o solo consegue absorver grande parte da água da chuva, impedindo o transbordamento.

Como alternativa em um local já urbanizado, o fluxo do rio pode ser desviado para um espaço designado ao longo do rio, evitando alagamentos. Uma cooperação entre as autoridades holandesas e alemãs, por exemplo, implementou 12 projetos ao longo do rio Reno, entre Karlsruhe (Alemanha) e Roterdã (Holanda).

<a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=texto&amp;utm_campaign=summitmobilidade19&amp;utm_content=::::&amp;utm_term=::::">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a>";s:10:"post_title";s:57:"Como cidades ao redor do mundo evitam danos de enchentes?";s:12:"post_excerpt";s:123:"Entenda de que maneira o ciclo da água pode deixar de ser um problema para a mobilidade por meio de propostas inteligentes";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:56:"como-cidades-ao-redor-do-mundo-evitam-danos-de-enchentes";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-02 00:11:18";s:17:"post_modified_gmt";s:19:"2020-08-02 03:11:18";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:36:"5f0c782c-87b1-4220-8002-6ffaacd5db8a";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}