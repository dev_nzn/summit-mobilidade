[�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7179";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-05 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-05 13:00:00";s:12:"post_content";s:6730:"<!-- wp:paragraph -->
<p>Toda a eletricidade consumida pelas propriedades municipais de Sydney, na Austrália, passará a ser fornecida por parques eólicos e solares. São aproximadamente 115 prédios (como bibliotecas, prefeituras e edifícios comerciais), 75 parques, 5 piscinas e 23 mil luzes nas ruas.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Um valor superior a 60 milhões de dólares australianos foi investido na transição para a utilização da energia renovável. O investimento deve economizar 500 mil dólares australianos anualmente durante a próxima década. Além disso, a medida pretende reduzir as <a href="https://summitmobilidade.estadao.com.br/acoes-governamentais-necessarias-para-diminuir-emissao-de-co2/">emissões de carbono</a> em 20 mil toneladas por ano.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A cidade fechou um contrato para fornecimento de energia com a empresa Flow Power, que será responsável por comprar e gerenciar a eletricidade. A expectativa é que o acordo possa gerar empregos para o enfrentamento das crises causadas pela pandemia do coronavírus e por uma longa seca que afeta a região de New South Wales (NSW).</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Energia Renovável de Sydney</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7181,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/12201845432002-1024x678.jpg" alt="" class="wp-image-7181"/><figcaption>Parque Eólico implantado na região de New South Wales é capaz de fornecer energia para 115 mil residências. (Fonte: Cidade de Sydney/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Em torno de três quartos da energia renovável de Sydney será gerada pelo vento, e o restante será solar. A eletricidade é gerada por duas fazendas solares e um parque eólico. Cada fazenda está em um estágio diferente de desenvolvimento, o que significa que o acordo será lançado por etapas a partir de julho, entrando totalmente em vigor em 2021.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/4-parques-lineares-e-como-eles-transformaram-o-cenario-urbano/">4 parques lineares e como eles transformaram o cenário urbano</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A estratégia de utilizar três fornecedores visa economizar recursos dos consumidores. Na Austrália, os preços no mercado de energia estão constantemente flutuando — às vezes a cada 5 minutos — com base na oferta e na demanda. Ao utilizar três geradores diferentes, a cidade pode comprar energia de qualquer gerador que possua eletricidade mais barata no momento necessário.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Fornecedores de eletricidade</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7180,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/12202048897003-1024x768.jpg" alt="" class="wp-image-7180"/><figcaption>Painéis são capazes de obter energia solar dos dois lados e seguir luz do Sol ao longo do dia. (Fonte: Cidade de Sydney/Divulgação)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>O maior fornecedor de energia renovável será o Parque Eólico Sapphire, o maior da região, que tem capacidade de 270 megawatts (gerados por 75 turbinas). Sozinho, o parque é capaz de fornecer energia elétrica para 115 mil residências.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Fazenda Solar Bomen, de propriedade da empresa privada Spark Infrastructure, produzirá 120 megawatts em uma área de 250 hectares. Para tanto, utiliza 310 mil painéis solares bifaciais, capazes de absorver luz solar dos dois lados, com tecnologia de rastreamento que muda cada painel ao longo do dia para capturar a energia do Sol.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/nova-york-torna-ruas-exclusivas-para-pedestres-durante-quarentena/">Nova York torna ruas exclusivas para pedestres durante quarentena</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O acordo também incentiva a participação da comunidade local. A Fazenda Solar Shoalhaven está sendo desenvolvida em parceria com a Repower Shoalhaven, uma empresa comunitária voluntária e sem fins lucrativos. Quando concluída, a fazenda solar de três megawatts terá cerca de 10 mil painéis.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Cidade verde</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Os australianos ainda obtêm a maior parte de sua eletricidade de fontes como termelétricas a base de carvão e a gás. No sentido contrário, Sydney tornou-se neutra em <a href="https://summitmobilidade.estadao.com.br/entenda-os-programas-de-precificacao-de-carbono/">carbono</a> em 2007 e foi a primeira cidade da Austrália a ser certificada em 2011.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/campanha-em-bolonha-premia-ciclistas-com-cerveja-e-sorvete/">Campanha em Bolonha premia ciclistas com cerveja e sorvete</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A cidade capacita as comunidades de negócios, residentes e visitantes de Sydney a tomar ações ambientais por meio de diversos programas direcionados, como o Better Building Partnership, CitySwitch Green Office e Smart Green Apartments, além de oferecer subsídios ambientais.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Até 2021, a cidade pretende reduzir as <a href="https://summitmobilidade.estadao.com.br/como-fomentar-cidades-de-baixo-carbono/">emissões</a> em suas operações em 44% em relação aos níveis de 2006, passando para 50% de energia renovável. O novo acordo ajudará o município a reduzir as emissões em 70% até 2024, seis anos antes do previsto inicialmente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Cities Today, Renew Economy, Cidade de Sydney, Sapphire Wind Farm, carbon Neutral Cities.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:59:"Prédios públicos de Sydney terão energia 100% renovável";s:12:"post_excerpt";s:131:"Cidade australiana fechou um contrato para fornecimento de eletricidade renovável para parques, edifícios e iluminação pública";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:54:"predios-publicos-de-sydney-terao-energia-100-renovavel";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:21:46";s:17:"post_modified_gmt";s:19:"2020-08-03 21:21:46";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7179";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}