H�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7271";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-06 10:00:00";s:13:"post_date_gmt";s:19:"2020-08-06 13:00:00";s:12:"post_content";s:21709:"<!-- wp:paragraph -->
<p>Em poucas décadas, centenas de milhões de pessoas saíram do campo e se concentraram no espaço urbano, alterando sua forma de viver, morar, trabalhar e se relacionar. E como esse novo cenário teve impactos e ainda tem na saúde humana?&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esse é o tema de pesquisa do médico patologista Paulo Saldiva, do Hospital das Clínicas (HC) da Faculdade de Medicina da Universidade de São Paulo (USP). O especialista dirige o departamento de Patologia dessa universidade, além de que compõe o Comitê de Qualidade do Ar da Organização Mundial da Saúde.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Em seu mais recente livro, <em>Vida Urbana e Saúde — Os Desafios dos Habitantes das Metrópoles</em>, Saldiva apresenta algumas relações entre a forma de arranjo urbano e os impactos disso na saúde humana. Confira a entrevista concedia pelo médico ao Summit Mobilidade Urbana.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Por que a mobilidade urbana se tornou um assunto relevante do ponto de vista médico?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nos últimos anos, nós conseguimos comprar alimentos com facilidade e baixo custo, dispensando o gasto de energia em sua busca. Além disso, estamos cada vez mais sedentários: o trabalho exige que fiquemos sentados diante do computador, e a nossa única chance de romper com isso seria pela mobilidade ativa, como caminhar ou pedalar, o que reduz a obesidade e suas consequências, bem como melhora a densidade óssea.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porém, o carro nos mantêm sentados e, embora seja uma máquina que possa nos levar a dezenas de quilômetros por hora, andamos em velocidades incrivelmente baixas nos congestionamentos. Por isso, costumo dizer que o carro nos oferece seis "pneus": quatro no chão, um no step e um na cintura.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img src="https://lh5.googleusercontent.com/eP-LGJiOm8lOutFWhYNth0iBDDi5D1fqRyOl47CaMAchp2HsJk3HVjQ6tJhR7YxtBDQnlieaKxYegIfD2J6x5iV2Ejp1Re_pGqTYwdl48k0MC2BHiieWQmBgB16eX5xvRfh7jakZ" width="602" height="401"> A velocidade real de deslocamento em grandes cidades, como São Paulo, costuma ser muito inferior ao limite da via e à potência do veículo. (Fonte: Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A cidade passou da produção de bens para a venda de serviços — a única indústria que fica na cidade é a da construção civil — e, por isso, as áreas mais valorizadas têm um processo de despopulação: sai casa, entra condomínio comercial.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Com isso, existe um processo de expansão da cidade em que o preço do solo varia muito, e as pessoas que menos têm precisam morar cada vez mais longe (a exceção são os condomínios de luxo localizados a algumas dezenas de quilômetros da cidade, onde as pessoas compraram a ilusão de que seria possível morar em uma região tranquila e chegar na cidade em dez minutos).&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Hoje, morar no centro custa muito caro. E, como ficamos em congestionamentos, os níveis de poluição em um corredor de tráfego são o dobro ou o triplo do que se respira em outros locais da cidade. Quando se está na marginal, percebemos como a água é suja, inclusive mais que o ar, mas ninguém obriga você a bebê-la ou tomar banho ali.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Qual o impacto dos congestionamentos na saúde das pessoas?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Dirigir em São Paulo não é exatamente um exercício de "elevação espiritual". Se você colocar um Holter, que mede arritmia e também consegue medir estresse, é possível verificar que a pressão diastólica e o nível de adrenalina se tornam muito maiores.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Objetivamente, estar submetido ao trânsito e à poluição altera os marcadores de inflamação. Os sintomas são brandos, mas ao longo do tempo essa inflamação vai cobrar um preço sob a forma de maior risco de câncer, aterosclerose (acúmulo de placas nas artérias) e de envelhecimento precoce dos pulmões.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Outro aspecto é o consumo de tempo: passamos cada vez mais tempo dentro de uma caixa metálica, e esse é um tempo improdutivo. Eu ando de bike até hoje, mas, durante um período em que sofri um acidente, vinha para a USP de ônibus: eu li nesses dois anos 14 livros.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mas se o ônibus for apertado, assim como no caso do carro, não se pode aproveitar o tempo para isso. É também um tempo que se deixa de investir nas pessoas que amamos ou na nossa formação. Imagine um jovem que trabalha para pagar seus estudos: se ele perder duas horas para ir e mais duas para voltar, em qual horário ele vai estudar? Existem evidências de que, quanto mais tempo se perde no trânsito, junto a outras variáveis, mais o salário e o desenvolvimento profissional pioram drasticamente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>O fato de se ter menos tempo para dormir é outro agravante a ser considerado?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Esse é um elemento central para a manutenção da saúde mental: nosso cérebro descansa sonhando, em fases mais profundas, quando se "reseta" o sistema. E a forma mais fácil de gerar ansiedade e depressão é justamente com privação de sono — os métodos de tortura mais antigos já incluíam isso.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O sono é influenciado também pela sonorização excessiva causada pelos ruídos. Às vezes, a gente percebe que a geladeira faz barulho quando a energia cai: a cóclea processa o som, mas o lóbulo frontal está tão estimulado que faz com que não se ouça mais.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Isso interfere em nosso mecanismo de proteção, que observava os ruídos exóticos e registrava como um perigo, possivelmente de um animal ou outra pessoa que se aproximava. Por isso, quando se está dormindo e ocorre um ruído ambiental, superficializa-se o sono — podemos até não acordar, mas o sono tem a arquitetura fragmentada, e já se sabe que isso está relacionado à hipertensão e a diabetes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Um trânsito mais bem-planejado pode reduzir o número de vítimas de lesões nos hospitais. Poderia falar sobre essa relação?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Nossa economia depende do modal motorizado, seja para o transporte de pessoas, seja para que uma pizza chegue quente ao seu destino. Por isso, usamos o modal sobre duas rodas. Só que se computarmos o custo das amputações, das mortes prematuras de jovens, das reabilitações e da manutenção da sobrevida dessas pessoas, há um grande prejuízo econômico.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O Código de Trânsito de 1998 permitiu o tráfego de motocicletas entre as vias de rolamento porque isso agilizaria o comércio e a indústria das motocicletas. Mas quem paga a conta é a saúde dessas pessoas e a família de quem sofreu invalidez precoce ou morte prematura. Existe uma pandemia de mortes e lesões traumáticas.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Na Suíça, eventualmente as pessoas caem de esqui; aqui não, todos os dias os pronto-socorros estão lotados de pessoas que apresentam trauma de crânio e, por isso, somos bons em neurocirurgia. Colocando isso na balança, veremos que estamos subsidiando um modal antigo que custa vidas humanas, sofrimento e dor.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>São Paulo adota o modelo de rodízio de veículos, mas há outros modelos possíveis, como o pedágio urbano. Como você avalia isso?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Estou convicto de que o pedágio urbano é necessário. O rodízio foi criado por questões ambientais e se tornou um instrumento de melhoria no fluxo de trânsito. Me parece lógico: se você deseja ter o privilégio de usar ar-condicionado e ouvir música, por que os 70% que andam de ônibus ou metrô precisam pagar por isso?&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Porém, há dois problemas na implementação. O primeiro é que não temos uma malha de transporte coletivo eficiente. Então, se o pedágio urbano for implementado hoje, aquela pessoa do Capão Redondo que não tem transporte vai pagar igual. O segundo é que a medida possui resistência porque seria mais um imposto em um país de impostos (que não são muito bem usados).&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Mas ele pode ter outras fontes de renda. Um comércio que fique ao lado do metrô tem uma vantagem competitiva enorme, então é justo que eles auxiliem no subsídio daquilo que os ajuda a ganhar dinheiro. O problema, portanto, não é técnico, mas político: como tornar viável é a chave da questão.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Conforme a saúde é afetada pela cultura da mobilidade centrada no carro, seria adequado que parte dessa tarifa também fosse revertida para o sistema público de saúde?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Eu acredito que não. É importante investir maciçamente em melhoria nas condições de transporte de baixa emissão, voltar a implementar ônibus elétricos (do modelo com fio, retirado das ruas) ou ônibus híbridos e melhorar a qualidade de calçadas e estrutura cicloviária. Ao investir em modais mais seguros e sustentáveis, a saúde já ganha.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Algumas cidades do mundo, como Shenzhen, na China, estão migrando sua frota para o modal elétrico. Do ponto de vista da saúde, o que se pode esperar dessas mudanças?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Veja, a China foi obrigada a investir nisso, inclusive por barreiras comerciais que vinha enfrentando. Então, sim, reduz poluição no corredor de ônibus, sobretudo em relação aos modelos velhos, a diesel. No entanto, você está falando com um sujeito que não é muito entusiasta do modal elétrico.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Por quê?&nbsp;</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Se você considerar o tempo de vida médio das baterias e de balanço energético, como essa energia elétrica é produzida, a mudança pode não ser tão favorável. Recentemente, li um artigo no qual mostrava que o carbono por quilômetro de um patinete elétrico é maior do que de um ônibus, mesmo a diesel.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A bateria de lítio e seu patinete possuem uma vida útil com cerca de dois anos e transportam apenas um passageiro. Então, a grande questão não se refere a como um monte de carros pode andar poluindo menos, mas sim a como ter modais coletivos eficazes. Precisamos repensar o modelo de mobilidade.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><img src="https://lh4.googleusercontent.com/nNBtWZ32UTOoCcqPgdZ8Yau6t1o0fxJEa9UWceX7Xwgs6nXeiLfLu7JzEkciLmtZB8OilJWWvJkL07vrueoU5y5Nh4e4-n6_LPBL8g-dP1eQQjxYDrd_bpxBjp7kAPGU0XKdemu_" width="602" height="399">Para Saldiva, a forma de ocupação do solo é um dos principais fatores que tornam o sistema de transporte coletivo de São Paulo insustentável. (Fonte: Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A bala de prata está na organização de um transporte coletivo funcional e não necessariamente na mudança da matriz energética. O Imperial College, em Londres, revitalizou uma região, que se tornou mais cara. Eles precisaram, inclusive, construir um prédio para que os técnicos das máquinas mais importantes conseguissem morar perto do local. Os demais profissionais moram a dezenas de quilômetros de lá, mas os trens são confortáveis, têm internet e eles pode ir trabalhando. A mobilidade para ele é uma extensão do trabalho, e o tempo perdido é muito pequeno.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quando eu dou aula na USP da Zona Leste, eu vou e volto no contrafluxo. No entanto, se você multiplicar o número de pessoas que vem de Itaquera e da Zona Sul pelo o de quilometragem seria como levar o Uruguai para Argentina pela manhã e trazê-lo de volta à tarde.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A demanda de passageiros/hora em São Paulo é insustentável, e o espalhamento da cidade torna inviável organizar uma matriz de transporte coletivo eficiente porque seria necessário estender muito o metrô. Barcelona possui a mesma quilometragem de metrô, mas funciona bem porque é compacta.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Recentemente, a polêmica do muro do Morumbi trouxe à tona a questão da desigualdade socioespacial. Como o "CEP" interfere no processo de adoecimento da população urbana?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Isso funciona tanto para doenças infectocontagiosas quanto para as crônicas, não transmissíveis. Por exemplo, o risco de morrer antes de 70 anos por infarto do miocárdio varia muito com o CEP, até oito vezes, a depender do território. Isso vai piorando conforme chega na periferia, com exceção do Brás, que tem uma das menores expectativas de vida mesmo estando na região central. Então há bolsões no centro, mas são pontuais.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/prefeitura-rejeita-pedido-de-muro-entre-morumbi-e-paraisopolis/">Prefeitura rejeita pedido de muro entre Morumbi e Paraisópolis</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Em Londres também é assim: no centro, a expectativa de vida chega a 90 anos; vai caindo conforme se aproxima da periferia na escala aproximada de um ano a menos para cada estação de metrô a mais. Em São Paulo, não é diferente.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O CEP é a unidade síntese de uma série de coisas: quanto o indivíduo precisa trabalhar para se manter? Quanto tempo ele consegue dedicar à sua saúde? Algumas pessoas vivem em tal condição que não têm nem o direito de adoecer, porque podem perder o emprego.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Isso é percebido também durante a pandemia: muita gente não pode ficar em casa ou tem algum membro da família que precisa sair à rua, podendo trazer o vírus para casa.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Variam também conforme o CEP a possibilidade de comprar comida de boa qualidade, que é mais cara do que os processados, e o acesso a equipamentos de saúde. Enfim, são uma série de fatores que possuem na mobilidade (ou na desmobilidade) um fator importante.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>Em outros momentos, você comentou que saímos de duas pandemias por século para duas pandemias por década. Qual é a relação do arranjo urbano e do modelo de mobilidade para esse tipo de ocorrência?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Há duas mobilidades importantes: a primeira é a global. A Peste Bubônica demorou cerca de dois séculos para ser uma doença europeia. A cólera, com o avanço do comércio inglês, demorou um século e foi levada para todo o mundo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A Gripe Espanhola veio com as tropas da Primeira Guerra Mundial em movimento e precisou de três anos para se espalhar pela Europa. O Sars-CoV-2 (vírus responsável pela pandemia atual) precisou de quatro semanas, porque o vírus viajou a jato: Ásia, França, Espanha, Itália e assim por diante. Esses ciclos são cada vez mais curtos, e há um elemento de mobilidade importante nisso.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/sp-incentiva-uso-bikes-para-conter-covid-19-no-transporte-publico/">SP incentiva uso de bikes para conter covid-19 no transporte público</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>E há a mobilidade microrregional, em mesoescala. Quem viaja de São Paulo para Piracicaba, Campinas ou Sorocaba vai de forma rodoviária. E, dentro da cidade, a transmissão ocorre pelas formas de mobilidade urbana, como ônibus ou metrô, além dos locais fechados, como shoppings.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A cidade possui uma complexidade porque, diferentemente do Morumbi, Paraisópolis possui uma densidade demográfica enorme. Além disso, as pessoas moram em casas pequenas sem TV a cabo, então claro que vão sair de casa e ficar mais propensas à contaminação.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Quando faço autópsias no HC, a maior parte das pessoas com forma mais grave do vírus são pobres. Eu não tenho dúvida de que, vendo o corpo das pessoas e as doenças que contraíram, vivo em um laboratório prático das consequências da urbanidade.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/como-o-plano-diretor-de-sao-paulo-tem-melhorado-a-mobilidade/">Como o Plano Diretor de São Paulo tem melhorado a mobilidade?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fora do Brasil, isso está mais maduro, mas já há no País uma cartografia urbana muito competente que relaciona a forma de viver e de se locomover na cidade à mortalidade infantil, bem como às doenças crônicas e as transmissíveis. A cidade não é igual para todos. Vai desde uma cidade ótima até uma cidade medieval.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><strong>É provável que algumas características desse período da pandemia venham para ficar. O que você acha que deve permanecer?</strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Em primeiro lugar, acredito que haja mais incentivo à não mobilidade. As pessoas vão se acostumar a fazer <em>home office</em>. Quando eu sou banca em uma tese de doutorado em Ribeirão Preto, por exemplo, o trabalho em si dura três horas, mas gasto o dia inteiro para isso, fora o custo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Outra situação seria ter de viajar para fazer uma apresentação de 40 minutos em um congresso em Manaus. Parece que vamos normalizar isso, que é útil, além do custo e do tempo, até para compatibilizar a agenda das pessoas. Uma <em>live</em> não é a mesma coisa de um encontro presencial, mas é possível substituir parte disso. Por isso, quem tem um escritório vazio na Avenida Paulista, vai demorar para alugar.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/por-que-diversificar-e-integrar-formas-de-deslocamento/">Por que diversificar e integrar formas de deslocamento?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Acredito também que o uso da bike, cujo mercado teve um <em>boom</em>, vai permanecer. Uma parte dessas pessoas vai descobrir que usar a bicicleta é possível e gostoso, assim como andar a pé — a cidade é um pouco feia, mas tem personalidade, algo que escapa no trajeto do carro.</p>
<!-- /wp:paragraph -->

<!-- wp:image {"id":7272,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="https://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1407367013-1-1024x410.jpg" alt="" class="wp-image-7272"/><figcaption>A Avenida Paulista é uma das rotas com melhor estrutura cicloviária. (Fonte: Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Vai haver também uma dramática redução de passageiros na frota de ônibus, o que tende a produzir uma crise no modal. A prefeitura não vai ter dinheiro para aumentar o subsídio, a frota vai ser reduzida, enfim, vai ser necessário repensar esse modelo.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por isso, é possível que haja um aumento espontâneo de mobilidade ativa, como a caminhada. Não será uma revolução, mas pode ser importante, afinal, alguns estudos indicam que o efeito desse padrão de mobilidade na saúde é mais relevante do que metabloqueadores e estatina — tanto que os institutos de pesquisa cardiológica têm pesquisas sobre esforço e o considera como um remédio.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Considero que as políticas que valorizam esse modelo cumprem um papel importante também para a saúde e a qualidade de vida. Para a saúde mental, é um ganho inestimável que a cidade deixe de ser um obstáculo e passe a fazer parte do cotidiano. Você pode deixar de ser um passageiro diante da paisagem e passar a incorporá-la.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:46:"As relações entre mobilidade urbana e saúde";s:12:"post_excerpt";s:193:"O escritor e patologista Paulo Saldiva, do Hospital das Clínicas da Universidade de São Paulo, fala das relações entre desigualdade socioespacial, infraestrutura urbana e saúde das pessoas";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:43:"as-relacoes-entre-mobilidade-urbana-e-saude";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-05 15:15:28";s:17:"post_modified_gmt";s:19:"2020-08-05 18:15:28";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:43:"https://summitmobilidade.nznvaas.io/?p=7271";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}