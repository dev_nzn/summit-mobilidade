r�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"6263";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-07-31 16:24:51";s:13:"post_date_gmt";s:19:"2020-07-31 19:24:51";s:12:"post_content";s:6827:"<!-- wp:paragraph -->
<p>Estados Unidos e China estão em uma competição acelerada em diversos campos da tecnologia, a exemplo do 5G, e a China deu recentemente mais um passo nessa disputa: outros dois satélites foram colocados em órbita para contribuir no aprimoramento do BeiDou, sistema de geolocalização chinês.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.sympla.com.br/summit-mobilidade-urbana-2020__818066">O Summit Mobilidade Urbana 2020 será gratuito e online. Inscreva-se agora!</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Com o novo lançamento, a China possui, agora, 35 satélites a serviço do desenvolvimento da tecnologia de referência geográfica. Isso deixa o BeiDou ainda mais próximo do GPS (<em>Global Positioning System</em>), que é vinculado ao Departamento de Defesa dos EUA. Embora o BeiDou ainda esteja circunscrito à China e a outros países da região, o governo do país espera conseguir competir com a tecnologia estadunidense.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/o-desafio-da-seguranca-digital-nos-carros-autonomos/">O desafio da segurança digital nos carros autônomos</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O GPS, que ainda é hegemônico em todo o mundo, possui outros concorrentes além do BeiDou: o Galileo, desenvolvido na Europa, que possui 22 satélites em órbita. O sistema russo Glonass, que também é uma tecnologia de geolocalização e trabalha com 24 satélites atualmente. Os japoneses, por sua vez, possuem o QZSS, com quatro satélites ao redor do globo. Porém, nenhum deles têm o mesmo potencial de ameaça à hegemonia do GPS, como o BeiDou.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Precisão</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong><img src="https://lh3.googleusercontent.com/Ze1QVrvdsRvVEnzR9VdbKq-39R9jNqziCMuDm0HscNtZxQrSydPjDMSJUAA4m2nWtbw1Hf-v2B2y4aYPDkIWHV5G9CrDIwqObjtUsdEKL-6FtdVn_G4YtN3fDLPSsktwstN3r_Gg" width="602" height="401"></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Desde a primeira implementação do programa, a China já enviou mais de 50 satélites ao espaço. (Fonte: Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Segundo o comunicado oficial da Administração Espacial da China emitido logo após o envio dos satélites, esses artefatos são capazes de fornecer recursos de serviço de posicionamento, navegação e cronometragem. Além disso, permitem a comunicação global de mensagens curtas de texto, bem como buscas e resgates internacionais.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Com os novos satélites em órbita, os técnicos chineses esperam ter um sistema mais preciso, reduzindo a precisão da tecnologia de alguns metros para poucos centímetros. Caso isso se confirme, o sistema estará pronto para ser "exportado", o que deve aquecer a corrida entre o país e os Estados Unidos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/">Como diversificar opções de deslocamento? Inscreva-se para esse debate gratuito e online que integra o Summit Mobilidade Urbana!&nbsp;</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Essa tem sido uma tendência de tecnologias da China, que já possui outros conglomerados de informação em hardware e software consolidados. A Huawei e a Xiaomi são exemplos de como a China pode ser capaz de se afirmar no mercado mundial de tecnologia.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>No fim de 2019, a China enviou dois satélites relacionados ao BeiDou para o espaço. Havia dúvida quanto ao lançamento de novos satélites em razão da pandemia do novo coronavírus, mas o calendário não foi alterado: em março foi lançado um satélite capaz de contribuir para o monitoramento do clima e navegação por localização.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Aplicação</strong></h2>
<!-- /wp:heading -->

<!-- wp:image -->
<figure class="wp-block-image"><img src="https://lh5.googleusercontent.com/VgEhD2V6WwPHgZvGjjgbGUlKcXuLTt-jj_C2ZKBi-c1R9JffG6hOro36hzrgJ7FbyBHS7lSuXj29KkUASDqeqYCp3m5_z-YvlI24wJX9gVXKcWyI_sLGl-yzuR6NE0kHkbS_sG5d" alt="Beidou logo.png"/></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>Sistema atinge capacidade de competir com o GPS 30 anos após seu primeiro lançamento. (Fonte: Wikimedia)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Tudo indica que os chineses casarão software e hardware, como em smartphones. Esse movimento se refere a uma decisão importante no negócio: o sistema opera desde 2011, mas ainda apresentava entraves na cobertura e era limitado a empresas. A abertura a qualquer pessoa física significa a entrada na corrida contra o GPS definitivamente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As aplicações são inúmeras. Com o BeiDou em condições de exportação, o sistema poderá traçar estratégias de georreferenciamento que contribuam para a consolidação das tecnologias chinesas em uma série de eventos.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/uber-ja-pode-retomar-testes-com-carros-autonomos-na-california/">Uber já pode retomar testes com carros autônomos na Califórnia</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por isso, a expertise chinesa em soluções de mobilidade tendem a ser exportadas junto a produtos. Um exemplo são os ônibus elétricos, que o país exporta para todo o mundo, é possível que o BeiDou passe a ser o sistema embarcado usado nesses veículos.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Outra possibilidade é o georreferenciamento para empresas. O BeiDou permitirá desde o monitoramento em tempo real de pastos e plantações, no caso de produtores rurais, até o controle de frotas de veículos para grandes empresas (a China sedia a Didi, empresa de tecnologia dona da 99, por exemplo).</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Há, além disso, aplicações próximas do cotidiano do cidadão comum, como o serviço de mapa e de rotas. Tão logo o BeiDou passe a estar incorporado nos produtos chineses, sobretudo smartphones, a massificação do sistema tende a ocorrer rapidamente e contribuir, junto ao 5G, para questionar a hegemonia estadunidense na área.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Tecmundo, Startse.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:45:"Conheça o BeiDou, concorrente chinês do GPS";s:12:"post_excerpt";s:106:"Ainda em construção, a Linha 9 do metrô de Barcelona terá 47 quilômetros de extensão e 52 estações";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:42:"conheca-o-beidou-concorrente-chines-do-gps";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 16:24:52";s:17:"post_modified_gmt";s:19:"2020-07-31 19:24:52";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:35:"summitmobilidade.nznvaas.io/?p=6263";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}