}�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:6254;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-07-31 16:17:46";s:13:"post_date_gmt";s:19:"2020-07-31 19:17:46";s:12:"post_content";s:6258:"<!-- wp:paragraph -->
<p>A crise proporcionada pela covid-19 impôs grandes desafios e ofereceu oportunidades para a <a href="https://summitmobilidade.estadao.com.br/integracao-do-transporte-publico-e-solucao-para-grandes-cidades/">remodelação</a> dos sistemas de mobilidade urbana. No Momento Mobilidade, uma iniciativa do <a href="https://www.youtube.com/watch?time_continue=5&amp;v=ur3P0DliXq4&amp;feature=emb_title">Summit Mobilidade Urbana 2020</a><em>,</em> houve debates sobre como diversificar as opções de deslocamento nesse contexto de transformações.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://www.sympla.com.br/summit-mobilidade-urbana-2020__818066">O Summit Mobilidade Urbana 2020 será gratuito e online. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para encontrar possíveis caminhos em meio à crise para o transporte nas cidades brasileiras, participaram da conversa a secretária de Transportes do município de São Paulo, Elisabete França, e o líder de relações institucionais da 99 app, Paulo Dallari. A visão de que o automóvel particular deve dar espaço a outros modais para uma mobilidade eficiente nos próximos anos foi partilhada por ambos durante a discussão.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>França destacou que a grande questão da mobilidade no momento é garantir os deslocamentos com segurança aos trabalhadores que precisam sair de casa para trabalhar nos sistemas coletivos e compartilhados. Para Dallari, o transporte coletivo terá uma retomada lenta no volume de passageiros, e os aplicativos podem ser grandes aliados.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Novo modelo</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong><img src="https://lh4.googleusercontent.com/SJrJm2uvhGXw7rMDRRFFGpXPZ_J7GKhmCOfl1dCgIc5amoA5KYkssK7l8QqI34rMCtM-1WaJZMB8ZFDDDE35oywHBDtOsjjuBvwBh3R0ZNRN4uM-4QgxxLlTzOQfiuMMaPaZL9l3" width="602" height="401"></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para especialistas, o uso compartilhado de veículos deve ser incentivado no pós-pandemia. (Fonte: Adao / Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A secretária defendeu o subsídio ao transporte coletivo de ônibus, por sua capilaridade. Ela também declarou que aposta na importância de aplicativos e no transporte sobre trilhos. Nesse sentido, no que tange à sustentabilidade, afirmou apoiar a estruturação de ciclovias e o alargamento de calçadas para alcançar uma mobilidade mais verde e menos poluente.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Já o líder disse que é necessário desestimular, com políticas públicas novas, as compras de carro, inclusive de usados. "Se a gente perder agora esse usuário do <a href="https://summitmobilidade.estadao.com.br/5-solucoes-de-transporte-publico-que-sao-referencias-mundiais-2/">transporte público</a> para a aquisição de um veículo, provavelmente vamos perder o passageiro do sistema compartilhado por muito mais tempo", ele avaliou.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Soluções estruturantes</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Por um lado, França argumentou a importância de uma estruturação financeira do setor de transportes. Ela lembrou que saúde e educação já possuem fundos específicos, logo, algo semelhante deveria ser feito para estruturar a mobilidade urbana, pois se trata de um serviço público essencial. “Vamos ter de criar um mecanismo que sustente e garanta a existência dos transportes públicos”, ela defendeu.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/">Como diversificar opções de deslocamento? Inscreva-se para esse debate gratuito e online que integra o Summit Mobilidade Urbana.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Por outro, Dallari ressaltou a importância da gestão de meio-fio nas cidades. Ele sugeriu que, em vez de considerar a abertura de vagas de estacionamentos para novos empreendimentos, as autoridades deveriam solicitar outras ferramentas, como áreas de embarque e desembarque.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/vlt-ou-brt-qual-e-a-melhor-opcao/">VLT ou BRT: qual é a melhor opção?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>"O espaço público das calçadas e meio fio poderia ser utilizado para usos que agreguem muito mais valor para a cidade e para o comércio", argumentou o executivo. Ele também mencionou estratégias como um incentivo para que as pessoas circulem a pé pela cidade e a ocupação do meio-fio com <em>food</em> <em>trucks</em>.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Medidas integradas</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p><strong><img src="https://lh3.googleusercontent.com/G1rEGflGBUjGOVz3nxmOwSzC5jx_56HP1c5Lq4MdRCiehHgXEGar0s_G1Akkt-ZLNSD_y4Vtk6sMp9xTKBvpUWPrbxz2M7dAxld7LXV6a8U8FU_ZEWX9-PNqM0Y_EXN5LYkNa8ku" width="602" height="401"></strong></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O ônibus é o transporte com maior capilaridade nas cidades, mas precisa de investimentos para manter seu equilíbrio financeiro. (Fonte: Alf Ribeiro / Shutterstock)</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Para ambos, o trabalho no campo da mobilidade deve ser integrado a todos os setores da sociedade. Congestionamentos e poluição do ar, por exemplo, podem afetar também o sistema de saúde. "O tema tem de ser enfrentado pelo conjunto das cidades, para que o motorista com carro particular seja retirado das ruas, diminuindo engarrafamentos inúteis e os custos que geram para as cidades", defendeu França.&nbsp;</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Momento Mobilidade Summit Mobilidade Urbana 2020.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:62:"Momento Mobilidade: debates de como diversificar deslocamentos";s:12:"post_excerpt";s:106:"Ainda em construção, a Linha 9 do metrô de Barcelona terá 47 quilômetros de extensão e 52 estações";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:61:"momento-mobilidade-debates-de-como-diversificar-deslocamentos";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-07-31 16:18:53";s:17:"post_modified_gmt";s:19:"2020-07-31 19:18:53";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:35:"summitmobilidade.nznvaas.io/?p=6254";s:10:"menu_order";i:0;s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}