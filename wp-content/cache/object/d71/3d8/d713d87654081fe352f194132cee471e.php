[�-_<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"7185";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2020-08-04 10:00:06";s:13:"post_date_gmt";s:19:"2020-08-04 13:00:06";s:12:"post_content";s:5651:"<!-- wp:paragraph -->
<p>A crise do coronavírus tem demandado diversas transformações nas cidades, e uma delas diz respeito a disparidades sociais, já que qualquer ação de enfrentamento à covid-19 no contexto urbano terá pouco efeito caso esse aspecto não seja levado em conta. A cidade deve se proteger por completo — da periferia ao centro.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://bit.ly/2OoAy6M">O Summit Mobilidade Urbana 2020 será online e gratuito. Inscreva-se agora.</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O surto de covid-19 evidenciou as <a href="https://summitmobilidade.estadao.com.br/pandemia-denuncia-fragilidades-na-estrutura-das-cidades/">disparidades sociais, econômicas e espaciais</a> de diferentes cidades. Os bairros economicamente desfavorecidos e a população mais carente estão sendo os mais atingidos, por isso requerem mais ações para combater a doença.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Mito da relação entre coronavírus e densidade urbana</strong></h2>
<!-- /wp:heading -->

<!-- wp:image {"id":7187,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1756442873-1-1024x640.jpg" alt="" class="wp-image-7187"/><figcaption>Populações mais carentes de todo o mundo foram mais atingidas pela pandemia. (Fonte: Felipe Mahecha / Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>No início da crise, com a confirmação dos primeiros surtos nas grandes cidades, acreditava-se que a densidade urbana tinha um papel fundamental na disseminação do vírus. A partir do momento em que foram verificadas diferenças na propagação da doença em um mesmo espaço, percebeu-se que o problema poderia ser mais complexo.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://summitmobilidade.estadao.com.br/como-se-proteger-do-coronavirus-no-transporte-publico/">Como se proteger do coronavírus no transporte público?</a></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>A geografia econômica da cidade, ou seja, a interação entre os cenários econômico e físico, passou a ser considerada um dos principais determinantes da transmissão da patologia. Isso quer dizer que regiões sem acesso a saneamento básico, com poucos espaços públicos, podem sofrer maior ação da doença.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>O risco de contágio aumenta com a ausência de infraestrutura nos bairros em que os moradores não têm outra opção senão sair de casa todos os dias em busca de trabalho, o que desfavorece o isolamento físico e social e incentiva aglomerações.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2><strong>Transformações necessárias</strong></h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Mudanças que eram necessárias antes do surgimento da covid-19 se tornam ainda mais urgentes sob uma ótica de saúde pública. As alterações devem ser planejadas não apenas levando em conta a segurança e a mobilidade urbana em um mundo alterado pelo novo coronavírus mas também a melhoria da habitabilidade.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Redesenho do espaço público</strong></h3>
<!-- /wp:heading -->

<!-- wp:image {"id":7186,"sizeSlug":"large"} -->
<figure class="wp-block-image size-large"><img src="http://summitmobilidade.nznvaas.io/wp-content/uploads/2020/08/shutterstock_1700397463-1-1024x684.jpg" alt="" class="wp-image-7186"/><figcaption>Falta de infraestrutura nos bairros favorece a propagação da covid-19. (Fonte:Photocarioca / Shutterstock)</figcaption></figure>
<!-- /wp:image -->

<!-- wp:paragraph -->
<p>O espaço público deve ser repensado para propiciar proteção maior contra o vírus, com ações como aumento de calçadas e abertura de parques. As mudanças devem ser realizadas especialmente em locais onde as casas são pequenas e grandes famílias compartilham o mesmo espaço.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Alteração de regras de ocupação do solo</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Daqui para frente, os regulamentos para ocupação do solo devem se preocupar em reduzir a densidade urbana, considerando os aspectos econômicos. Regras mais rígidas geralmente limitam a altura das construções e a quantidade de propriedades que podem ser desenvolvidas em um terreno.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>As cidades precisam ser organizadas em zonas menores de densidades diferenciadas, impulsionadas pela capacidade e demanda de infraestrutura, principalmente no transporte.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3><strong>Financiamento de infraestrutura</strong></h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>O financiamento da infraestrutura urbana deve fortalecer a possibilidade de o governo local e a comunidade melhorarem as condições de vida em favelas e bairros informais. A necessidade mais urgente é o acesso dos moradores à água potável e ao saneamento, que não só colabora com o controle da covid-19 como também ajuda a reduzir a incidência de outras doenças.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Fonte: Banco Mundial</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p><a href="https://estadaosummitmobilidade.com.br/?utm_source=nzn&amp;utm_medium=blog&amp;utm_campaign=summit">Curtiu o assunto? Clique aqui e saiba mais sobre como a mobilidade pode melhorar os espaços.</a></p>
<!-- /wp:paragraph -->";s:10:"post_title";s:60:"Cidades devem se transformar para sobreviver ao coronavírus";s:12:"post_excerpt";s:71:"Desigualdades sociais devem ser superadas no enfrentamento à pandemia
";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:59:"cidades-devem-se-transformar-para-sobreviver-ao-coronavirus";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2020-08-03 18:24:41";s:17:"post_modified_gmt";s:19:"2020-08-03 21:24:41";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:42:"http://summitmobilidade.nznvaas.io/?p=7185";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}